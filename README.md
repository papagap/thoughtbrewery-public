# thoughtbrewery

This repository contains all open-source software projects used for the website and demos of the thoughtbrewery.net domain. Currently the following projects are included:
* Datastreams
* EventplanningDemoServer
* FermentationDatabase
* NoiseSensorClient
* ThoughtbrewerySite

## Getting Started

Since the above projects are built and licensed separately, each of the projects contains separate README, LICENSE, and THIRD-PARTY-LICENSES files. Therefore, please find the respective information inside the project directories.

## Authors

* **Apostolos Papageorgiou** - [thoughtbrewery](https://thoughtbrewery.net)

## Licensing

Please refer to the directories of the individual projects for licensing information.
