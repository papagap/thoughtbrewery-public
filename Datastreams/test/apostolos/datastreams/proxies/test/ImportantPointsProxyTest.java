/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.datastreams.proxies.test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import apostolos.datastreams.Configuration;
import apostolos.datastreams.proxies.BaseProxy;
import apostolos.datastreams.proxies.ImportantPointsProxy;
import apostolos.datastreams.DatastreamItem;

/**
 * Test case of class {@link apostolos.datastreams.proxies.ImportantPointsProxy}
 * 
 * @author Apostolos Papageorgiou
 */
public class ImportantPointsProxyTest {

	/**
	 * Performs the initialization necessary for the test case execution by reading in the properties from the properties file
	 */
	@Before
	public void init() {
		Configuration.readPropertiesFromConfigFile();
	}
	
	/**
	 * JUint test of the mechanism with which the class under test finds adjacent points
	 */
	@Test
	public void testAdjacentPoints() {
		ArrayList<Integer> testList = new ArrayList<Integer>();
		testList.addAll(Arrays.asList(new Integer[]{1, 5, 88, 33, 4, 7, 9, 13, 16, 21})); // Play by adding and removing the number 10, but not removing any elements
		Collections.sort(testList);
		int tmp = Collections.binarySearch(testList, 10);
		if (testList.contains(10)) { // if the searched element is in the list...
			Assert.assertTrue(((tmp > 0) == true)); // ...then the result of the binary search gives its position in the sorted list
		} else { // if the searched element is not in the list...
			Assert.assertEquals(testList.get(-tmp-2), (Integer)9); // ...then -tmp-2 and -tmp-1 give the positions of the adjacent elements in the sorted list,
			Assert.assertEquals(testList.get(-tmp-1), (Integer)13); // between which the searched element WOULD BE (note: tmp is the result of the binary search)
		}
	}
	
	/**
	 * JUnit test of method {@link apostolos.datastreams.proxies.ImportantPointsProxy#orderItemsBasedOnDistanceToAdjacentImportantPoints(List)}
	 */
	@Test
	public void testOrderItemsBasedOnDistanceToAdjacentImportantPoints() {
		List<DatastreamItem> ds = new ArrayList<DatastreamItem>();
		String[] input = {"1", "0.5", "0.7", "0.5", "8.9", "5", "2", "12", "5", "2", "21", "2.9"};
		String dsName = "ipTestDS";
		DatastreamItem currItem = null;
		for (int i=0; i<input.length; i++) {
			Date date = new Date();
			String time = new Timestamp(date.getTime()).toString();
			currItem = new DatastreamItem(i, dsName, input[i], Timestamp.valueOf(time));
			ds.add(currItem);
		}
		ImportantPointsProxy proxy = new ImportantPointsProxy(dsName); // The threshold does not matter for this test, since the "process" method will not be tested; only the ordering of the items based on importance
		List<Integer> res = proxy.orderItemsBasedOnDistanceToAdjacentImportantPoints(ds);
		Assert.assertEquals(res.get(0), (Integer)0); // Check that the first item was selected to be the most important one
		Assert.assertEquals(res.get(1), (Integer)11); // Check that the last item was selected to be the second most important one
		Assert.assertEquals(res.get(2), (Integer)10); // Check that the item in position "10" (i.e., with value "21") was selected to be the third most important (because it is an outlier; refer to the algorithm reference in the documentation of the class under test)
		Assert.assertEquals(res.get(3), (Integer)9); // Check that the item in position "9" (i.e., with value "2") was selected to be the fourth most important (because it is another outlier; refer to the algorithm reference in the documentation of the class under test)
	}
	
	/**
	 * JUnit test of method {@link apostolos.datastreams.proxies.ImportantPointsProxy#process(String)}
	 */
	@Test
	public void testProcessWithFixedInputValues() {
		
		String dsName = "ipTestDS";
		
		BaseProxy proxy = new ImportantPointsProxy(dsName, 0.8);
		
		ArrayList<String> itemsThatShouldBeForwarded = new ArrayList<String>();
		// It was checked "on paper" that the importance of the following 4 items (as they arrive) should exceed the threshold 
		itemsThatShouldBeForwarded.add("1");
		itemsThatShouldBeForwarded.add("8.9");
		itemsThatShouldBeForwarded.add("12");
		itemsThatShouldBeForwarded.add("2");
		ArrayList<String> forwardedItems = new ArrayList<String>();
		
		String[] input = {"1", "0.5", "0.7", "0.5", "8.9", "5", "2", "12", "20", "2", "0.6", "2.9"};
		
		for (int i=0; i<input.length; i++) {
			String val = input[i];
			if (proxy.process(val)) {
				forwardedItems.add(val);
			}
		}

		// Check that the appropriate values have been forwarded
		Assert.assertEquals(forwardedItems, itemsThatShouldBeForwarded);
					
		// Check that all inputs have been added to the cache
		Assert.assertEquals(proxy.getCache().size(), input.length);
	}
	
	/*----------- Alternative code which performs the same testing but by using the proxy through a DatastreamThread -----------*/
	
	/*
	@Test
	public void testWithFixedInputValues() {
		DatastreamThread dst = new TestDatastreamThread();
		dst.run();
	}
	
	private class TestDatastreamThread extends DatastreamThread {
		
		public void run(){
			
			String dsName = "ipTestDS";
			
			setProxy(new ImportantPointsProxy(dsName, 0.8));
			
			ArrayList<String> itemsThatShouldBeForwarded = new ArrayList<String>();
			// It was checked "on paper" that the importance of the following 4 items (as they arrive) should exceed the threshold 
			itemsThatShouldBeForwarded.add("1");
			itemsThatShouldBeForwarded.add("8.9");
			itemsThatShouldBeForwarded.add("12");
			itemsThatShouldBeForwarded.add("2");
			ArrayList<String> forwardedItems = new ArrayList<String>();
			
			String[] input = {"1", "0.5", "0.7", "0.5", "8.9", "5", "2", "12", "20", "2", "0.6", "2.9"};
			
			for (int i=0; i<input.length; i++) {
				String val = input[i];
				if (proxy.process(val)) {
					forwardedItems.add(val);
				}
			}

			// Check that the appropriate values have been forwarded
			Assert.assertEquals(forwardedItems, itemsThatShouldBeForwarded);
						
			// Check that all inputs have been added to the cache
			Assert.assertEquals(proxy.getCache().size(), input.length);
			
			interrupt();
		}
		
	}
	*/

}
