/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.datastreams.proxies.test;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import apostolos.datastreams.Configuration;
import apostolos.datastreams.proxies.AggregationProxy;
import apostolos.datastreams.proxies.BaseProxy;

/**
 * Test case of class {@link apostolos.datastreams.proxies.AggregationProxy}
 * 
 * @author Apostolos Papageorgiou
 */
public class AggregationProxyTest {

	/**
	 * Performs the initialization necessary for the test case execution by reading in the properties from the properties file
	 */
	@Before
	public void init() {
		Configuration.readPropertiesFromConfigFile();
	}
	
	/**
	 * JUnit test of method {@link apostolos.datastreams.proxies.AggregationProxy#process(String)}
	 */
	@Test
	public void testProcessWithFixedInputValues() {
		
		String dsName = "aggTestDS";
		  
		BaseProxy proxy = new AggregationProxy(dsName, 5);
		
		String[] input = {"1", "0.5", "5", "1", "fg", "5", "1", "0.5", "5", "0.5", "1", "5"};
		ArrayList<String> itemsThatShouldBeForwarded = new ArrayList<String>();
		itemsThatShouldBeForwarded.add("2.5"); // (1 + 0.5 + 5 + 1 + 5) / 5 (note: "fg" is ignored) 
		itemsThatShouldBeForwarded.add("1.6"); // (1 + 0.5 + 5 + 1 + 0.5) / 5
		ArrayList<String> forwardedItems = new ArrayList<String>();
		
		for (int i=0; i<input.length; i++) {
			String val = input[i];
			String currAggrVal = ((AggregationProxy)proxy).getAggrValue();
			if (proxy.process(val)) {
				forwardedItems.add(Double.toString((Double.valueOf(currAggrVal)*4 + Double.valueOf(val)) / 5));
			}
		}
		
		// Check that the appropriate (aggregated) values have been forwarded
		Assert.assertEquals(itemsThatShouldBeForwarded, forwardedItems);
		
		// Check that all inputs have been added to the cache
		Assert.assertEquals(proxy.getCache().size(), input.length);
	}
	
	/*----------- Alternative code which performs the same testing but by using the proxy through a DatastreamThread -----------*/
	
	/*
	@Test
	public void testWithFixedInputValues() {
		DatastreamThread dst = new TestDatastreamThread();
		dst.run();
	}
	
	private class TestDatastreamThread extends DatastreamThread {
		
		public void run(){
			
			String dsName = "aggTestDS";
			  
			setProxy(new AggregationProxy(dsName, 5));
			
			String[] input = {"1", "0.5", "5", "1", "fg", "5", "1", "0.5", "5", "0.5", "1", "5"};
			ArrayList<String> itemsThatShouldBeForwarded = new ArrayList<String>();
			itemsThatShouldBeForwarded.add("2.5"); // (1 + 0.5 + 5 + 1 + 5) / 5 (note: "fg" is ignored) 
			itemsThatShouldBeForwarded.add("1.6"); // (1 + 0.5 + 5 + 1 + 0.5) / 5
			ArrayList<String> forwardedItems = new ArrayList<String>();
			
			for (int i=0; i<input.length; i++) {
				String val = input[i];
				String currAggrVal = ((AggregationProxy)proxy).getAggrValue();
				if (proxy.process(val)) {
					forwardedItems.add(Double.toString((Double.valueOf(currAggrVal)*4 + Double.valueOf(val)) / 5));
				}
			}
			
			// Check that the appropriate (aggregated) values have been forwarded
			Assert.assertEquals(itemsThatShouldBeForwarded, forwardedItems);
			
			// Check that all inputs have been added to the cache
			Assert.assertEquals(proxy.getCache().size(), input.length); 
			
			interrupt();
		
		}
		
	}
	*/

}
