/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.datastreams.proxies.test;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import apostolos.datastreams.Configuration;
import apostolos.datastreams.proxies.BaseProxy;
import apostolos.datastreams.proxies.SamplingProxy;

/**
 * Test case of class {@link apostolos.datastreams.proxies.SamplingProxy}
 * 
 * @author Apostolos Papageorgiou
 */
public class SamplingProxyTest {
	
	/**
	 * Performs the initialization necessary for the test case execution by reading in the properties from the properties file
	 */
	@Before
	public void init() {
		Configuration.readPropertiesFromConfigFile();
	}
	
	/**
	 * JUnit test of method {@link apostolos.datastreams.proxies.SamplingProxy#process(String)}
	 */
	@Test
	public void testProcessWithFixedInputValues() {
		
		String dsName = "samplingTestDS";
		  
		BaseProxy proxy = new SamplingProxy(dsName, 5);
		
		String[] input = {"1", "0.5", "5", "1", "5", "fg", "1", "0.5", "5", "1", "0.5", "5"};
		ArrayList<String> itemsThatShouldBeForwarded = new ArrayList<String>();
		itemsThatShouldBeForwarded.add("1"); 
		itemsThatShouldBeForwarded.add("fg");
		itemsThatShouldBeForwarded.add("0.5");
		ArrayList<String> forwardedItems = new ArrayList<String>();
		
		for (int i=0; i<input.length; i++) {
			String val = input[i];
			// Check that the sampling counter works correctly
			Assert.assertEquals((i+1)%((SamplingProxy)proxy).getInterval(), ((SamplingProxy)proxy).getCounter()%((SamplingProxy)proxy).getInterval());
			if (proxy.process(val)) {
				forwardedItems.add(val);
			}
		}
		
		// Check that the appropriate values have been forwarded
		Assert.assertEquals(itemsThatShouldBeForwarded, forwardedItems);
		
		// Check that all inputs have been added to the cache
		Assert.assertEquals(proxy.getCache().size(), input.length); 
	}
	
	/*----------- Alternative code which performs the same testing but by using the proxy through a DatastreamThread -----------*/
	
	/*
	@Test
	public void testWithFixedInputValues() {
		DatastreamThread dst = new TestDatastreamThread();
		dst.run();
	}
	
	private class TestDatastreamThread extends DatastreamThread {
		
		public void run(){
			
			String dsName = "samplingTestDS";
			  
			setProxy(new SamplingProxy(dsName, 5));
			
			String[] input = {"1", "0.5", "5", "1", "5", "fg", "1", "0.5", "5", "1", "0.5", "5"};
			ArrayList<String> itemsThatShouldBeForwarded = new ArrayList<String>();
			itemsThatShouldBeForwarded.add("1"); 
			itemsThatShouldBeForwarded.add("fg");
			itemsThatShouldBeForwarded.add("0.5");
			ArrayList<String> forwardedItems = new ArrayList<String>();
			
			for (int i=0; i<input.length; i++) {
				String val = input[i];
				// Check that the aggregation counter works correctly
				Assert.assertEquals((i+1)%((SamplingProxy)proxy).getInterval(), ((SamplingProxy)proxy).getCounter()%((SamplingProxy)proxy).getInterval());
				if (proxy.process(val)) {
					forwardedItems.add(val);
				}
			}
			
			// Check that the appropriate values have been forwarded
			Assert.assertEquals(itemsThatShouldBeForwarded, forwardedItems);
			
			// Check that all inputs have been added to the cache
			Assert.assertEquals(proxy.getCache().size(), input.length); 
			
			interrupt();
		
		}
		
	}
	*/

}
