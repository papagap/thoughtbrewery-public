#-------------------------------------------------------------------------------
# Copyright 2019 Apostolos Papageorgiou
# 
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License.  You may obtain a copy
# of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
# License for the specific language governing permissions and limitations under
# the License.
#-------------------------------------------------------------------------------
# Datastreams

This is a java library for managing data streams, i.e., series of items that are coming from a specific data source.
The library can be used in order to create different data proxies, which manage the data in different ways, and to switch among them.
For example, a sampling proxy can be used in order to select (and potentially forward) every n-th incoming value.

## API / Usage

Please find the complete javadoc documentation of the library in the doc folder.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

Please edit the java properties (resources/datastreams.properties file) to reflect your system settings and preferences, and execute "mvn clean install" in the root folder of this project in order to install the library in a local maven repository.

## Authors

* **Apostolos Papageorgiou** - [thoughtbrewery](https://thoughtbrewery.net)

## License

This project is licensed under the Apache 2.0 license. Please see the LICENSE file for more details.
