/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.datastreams;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.restlet.data.Form;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

import apostolos.datastreams.DatastreamItem;

/**
 * Util contains functions that are used by various proxies or other classes of the data streams library.
 * 
 * @author Apostolos Papageorgiou
 */
public class Util {
	
	private static Logger log = Logger.getLogger("Util");
	
	/**
	 * Writes a data item (name/value pair with a timestamp) into the (in-memory) cache of the proxy.  
	 * 
	 * @param cache the cache of the proxy that calls the method
	 * @param item the data item (i.e., {@link DatastreamItem}) to be written into the cache
	 * @return the status of the operation as a {@link Configuration#SUCCESS} or {@link Configuration#FAIL} code
	 */
	public static int writeToCache(List<DatastreamItem> cache, DatastreamItem item) {
		cache.add(item);
		log.debug("Writing to cache an item with name = " + item.getName() + " and value = " + item.getValue() + " (cache size = " + cache.size() + ")");
		return Configuration.SUCCESS;
	}
	
	/**
	 * Writes a data stream item (name/value pair with a timestamp) into a database, by calling the respective HTTP (REST) interface.
	 * The access details must have been provided as properties in the configuration.
	 * 
	 * @param item the data item (i.e., {@link DatastreamItem}) to be written into the Cloud database
	 * @return the status of the operation as a ({@link Configuration#SUCCESS}) or {@link Configuration#FAIL}) code
	 */
	public static int sendToCloud(DatastreamItem item) {
		log.debug("Sending to Cloud an item with name = " + item.getName() + " and value = " + item.getValue());
		
		ClientResource client = new ClientResource(Configuration.CLOUD_BASE_URL + "insertDatastreamItem");
		
		Form f = new Form();
		f.add("key", Configuration.CLOUD_API_KEY);
		f.add("datastream", Configuration.CLOUD_DB_TABLE);
		f.add("name", item.getName());
		f.add("value", item.getValue());
		f.add("timestamp", item.getTimestamp().toString());
		
		String response = "";
		try {
			response = client.post(f).getText();
			log.debug("Cloud server says: " + response);
		} catch (ResourceException re){
			log.error("Cloud call failed: " + re.getMessage());
			return Configuration.FAIL;
		} catch (IOException ioe) {
			log.error("Cloud call failed: " + ioe.getMessage());
			return Configuration.FAIL;
		}
		return Configuration.SUCCESS;
	}
	
	/**
	 * Retrieves the current time as a String compatible with the java sql Timestamp format. 
	 * 
	 * @return the current time as a String compatible with the java sql Timestamp format
	 */
	public static String getCurrTimeAsTimestampString() {
		Calendar cal = Calendar.getInstance();
		String ts = new Timestamp(cal.getTime().getTime()).toString();
		return ts;
	}

}
