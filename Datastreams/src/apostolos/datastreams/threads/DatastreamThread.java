/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.datastreams.threads;

import org.apache.log4j.Logger;

import apostolos.datastreams.proxies.BaseProxy;

/**
 * DatastreamThread shall be extended by every thread that will implement a data collection task and
 * it contains fields that are necessary to be set for each such thread, e.g., the proxy that will be used.
 * 
 * @author Apostolos Papageorgiou
 */
public class DatastreamThread extends Thread {

	protected Logger log = Logger.getLogger(this.getClass());
	protected BaseProxy proxy;
	
	/**
	 * DatastreamThread Constructor
	 */
	public DatastreamThread() {
		super();
		proxy = null;
	}
	
	/*-------------- GETTERS and SETTERS -------------*/

	/**
	 * @return the data stream proxy used by this thread
	 */
	public BaseProxy getProxy() {
		return proxy;
	}

	/**
	 * Assigns a data stream proxy to this thread.  
	 * 
	 * @param proxy the data stream proxy used by this thread
	 */
	public void setProxy(BaseProxy proxy) {
		this.proxy = proxy;
		
	}
	
}
