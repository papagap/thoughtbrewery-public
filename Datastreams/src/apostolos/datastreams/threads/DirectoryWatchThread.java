/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.datastreams.threads;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * DirectoryWatchThread shall be extended by every thread that will implement a concrete directory-watching task.
 * <p>
 * It contains methods for registering a directory and potentially its sub-directories.
 * Any creations, deletions, or modifications in this directory will be captured.
 * 
 * @author Apostolos Papageorgiou
 */
public class DirectoryWatchThread extends Thread {

	protected Logger log = Logger.getLogger(this.getClass());
	protected Path path;
	
	protected final WatchService watcher;
    protected final Map<WatchKey,Path> keys;
    protected final boolean recursive;
    private boolean trace = false;

    @SuppressWarnings("unchecked")
    protected static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>)event;
    }
	
	/**
	 * DirectoryWatchThread Constructor:
	 * 
	 * Creates a WatchService and registers the given directory.
	 * Initializes the passed path variable.
	 * 
	 * @param path the path to the directory that is to be watched
     * @param recursive if this is true then changes in the sub-directories under the provided path shall be watched as well
     * @throws IOException if the provided path is not valid or cannot be accessed
	 */
    public DirectoryWatchThread(Path path, boolean recursive) throws IOException {
    	log.info("Watcher Thread constructed for path: " + path);
    	this.watcher = FileSystems.getDefault().newWatchService();
        this.keys = new HashMap<WatchKey,Path>();
        this.recursive = recursive;
        this.path = path;

        if (recursive) {
            registerAll(path);
        } else {
            register(path);
        }
        
        this.trace = true;
    }
    
    /**
     * Registers the given directory with the WatchService
     * 
     * @param path the path watched by this {@link apostolos.datastreams.threads.DirectoryWatchThread}
     * @throws IOException if the provided path is not valid or cannot be accessed
     */
    protected void register(Path path) throws IOException {
        WatchKey key = path.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        if (trace) {
            Path prev = keys.get(key);
            if (prev == null) {
                System.out.format("register: %s\n", path);
            } else {
                if (!path.equals(prev)) {
                    System.out.format("update: %s -> %s\n", prev, path);
                }
            }
        }
        keys.put(key, path);
    }

    /**
     * Registers the given directory, as well all its sub-directories, with the WatchService.
     * 
     * @param start the path watched together with all its sub-directories by this {@link apostolos.datastreams.threads.DirectoryWatchThread}
     * @throws IOException if the provided path is not valid or cannot be accessed
     */
    protected void registerAll(Path start) throws IOException {
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path path, BasicFileAttributes attrs) throws IOException {
            	register(path);
                return FileVisitResult.CONTINUE;
            }
        });
    }

	/**
	 * Stops the Thread and performs some additional logging.
	 */
	public void stopIt() {
		log.info("Stopping the DirectoryWatchThread that was watching the directory: " + path);
		interrupt();
	}
	
	/*-------------- GETTERS and SETTERS -------------*/

	/**
	 * @return the path watched by this {@link apostolos.datastreams.threads.DirectoryWatchThread}
	 */
	public Path getPath() {
		return path;
	}

	/**
	 * @param path the path of the directory that shall be watched by this {@link apostolos.datastreams.threads.DirectoryWatchThread}
	 */
	public void setPath(Path path) {
		this.path = path;
	}

}
