/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.datastreams;

//import java.io.File;
//import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

//import org.apache.commons.configuration2.PropertiesConfiguration;
//import org.apache.commons.configuration2.builder.fluent.Configurations;
//import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Configuration is a static class used for maintaining system properties.
 * <p>
 * It reads the configuration from a java properties file, the path and name of which are also specified in this class.
 * It also provides Enumerations and useful methods for handling the properties.
 *
 * @author Apostolos Papageorgiou
 */
public class Configuration {

	private static Logger log = Logger.getLogger("Configuration");
	
	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	public static enum ProxyTypes {
		SAMPLING,
		AGGREGATION,
		CHANGE_DETECTION,
		IMPORTANT_POINTS
	}
	
	public static enum ChangeDetectionType {
		STRICT,
		THRESHOLD_BASED
	}
	
	private static final String CONFIG_FILE_NS = "apostolos.datastreams.";
	//private static final String CONFIG_FILE_DIR = "resources" + File.separator;
	private static final String CONFIG_FILE_NAME = "datastreams.properties";
	
	public static String CLOUD_DB_TABLE = "tsentry";
	public static String CLOUD_BASE_URL = "http://localhost:8182/";
	public static String CLOUD_API_KEY = "test-api-key";
	
	public static int DEFAULT_SAMPLING_INTERVAL = 1;
	public static int DEFAULT_AGGREGATION_WINDOW_SIZE = 5;
	public static String DEFAULT_CHANGE_DETECTION_TYPE = "strict";
	public static double DEFAULT_CHANGE_DETECTION_THRESHOLD = 0;
	public static double DEFAULT_IMPORTANT_POINTS_THRESHOLD = 0.8;
	public static int ANALYTICS_CACHE_SIZE = 100;
	
	/**
	 * Sets the values of the Configuration public class variables by reading the properties file.
	 * Note that the path and the name of the file must have been set correctly in the code.
	 * 
	 * @return status of reading in the properties file (true for success or false for failure)
	 */
	public static boolean readPropertiesFromConfigFile() {
		Properties properties = new Properties();
		try {
			
		    //properties.load(new FileInputStream(new File(CONFIG_FILE_DIR + CONFIG_FILE_NAME)));
			properties.load(Configuration.class.getResourceAsStream("/" + CONFIG_FILE_NAME));
			properties.load(Configuration.class.getResourceAsStream("/log4j.properties"));
			PropertyConfigurator.configure(properties);
		    
		    if (properties.containsKey(CONFIG_FILE_NS + "cloud_base_url")) {
		    	CLOUD_BASE_URL = properties.getProperty(CONFIG_FILE_NS + "cloud_base_url");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "cloud_api_key")) {
		    	CLOUD_API_KEY = properties.getProperty(CONFIG_FILE_NS + "cloud_api_key");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "cloud_db_table")) {
		    	CLOUD_DB_TABLE = properties.getProperty(CONFIG_FILE_NS + "cloud_db_table");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "default_sampling_interval")) {
		    	DEFAULT_SAMPLING_INTERVAL = Integer.parseInt(properties.getProperty(CONFIG_FILE_NS + "default_sampling_interval"));
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "default_aggregation_window_size")) {
		    	DEFAULT_AGGREGATION_WINDOW_SIZE = Integer.parseInt(properties.getProperty(CONFIG_FILE_NS + "default_aggregation_window_size"));
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "default_change_detection_type")) {
		    	DEFAULT_CHANGE_DETECTION_TYPE = properties.getProperty(CONFIG_FILE_NS + "default_change_detection_type");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "default_change_detection_threshold")) {
		    	DEFAULT_CHANGE_DETECTION_THRESHOLD = Double.parseDouble(properties.getProperty(CONFIG_FILE_NS + "default_change_detection_threshold"));
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "default_important_points_threshold")) {
		    	DEFAULT_IMPORTANT_POINTS_THRESHOLD = Double.parseDouble(properties.getProperty(CONFIG_FILE_NS + "default_important_points_threshold"));
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "analytics_cache_size")) {
		    	ANALYTICS_CACHE_SIZE = Integer.parseInt(properties.getProperty(CONFIG_FILE_NS + "analytics_cache_size"));
		    }
		    
		    return true;
		    
		} catch (IOException ioe) {
			log.error("Error reading configuration: " + ioe.getMessage());
			return false;
		}
	}

	/* Below is an alternative solution, which uses the Apache Commons Configuration library. It is not used here due to the big number of dependencies.
	public static boolean readPropertiesUsingCommonsConfiguration() {
		Configurations configs = new Configurations();
		try {
		    PropertiesConfiguration config = configs.properties(new File(CONFIG_FILE_DIR + CONFIG_FILE_NAME));
		    
		    CLOUD_BASE_URL = config.getString(CONFIG_FILE_NS + "cloud_base_url");
		    CLOUD_API_KEY = config.getString(CONFIG_FILE_NS + "cloud_api_key");
			CLOUD_DB_NAME = config.getString(CONFIG_FILE_NS + "cloud_db_name");
			CLOUD_DB_TABLE = config.getString(CONFIG_FILE_NS + "cloud_db_table");
			DEFAULT_SAMPLING_INTERVAL = config.getInt(CONFIG_FILE_NS + "default_sampling_interval");
			DEFAULT_AGGREGATION_WINDOW_SIZE = config.getInt(CONFIG_FILE_NS + "default_aggregation_window_size");
			DEFAULT_CHANGE_DETECTION_TYPE = config.getString(CONFIG_FILE_NS + "default_change_detection_type");
			DEFAULT_CHANGE_DETECTION_THRESHOLD = config.getDouble(CONFIG_FILE_NS + "default_change_detection_threshold");
			DEFAULT_IMPORTANT_POINTS_THRESHOLD = config.getDouble(CONFIG_FILE_NS + "default_important_points_threshold");
			ANALYTICS_CACHE_SIZE = config.getInt(CONFIG_FILE_NS + "analytics_cache_size");
			
			return true;
			
		} catch (ConfigurationException ce) {
		    log.error("Error reading configuration: " + ce.getMessage());
		    return false;
		}
	}*/
	
}
