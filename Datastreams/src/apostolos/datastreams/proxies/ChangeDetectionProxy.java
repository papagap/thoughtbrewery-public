/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.datastreams.proxies;

import java.sql.Timestamp;
import java.util.ArrayList;

import apostolos.datastreams.Configuration;
import apostolos.datastreams.Util;
import apostolos.datastreams.Configuration.ChangeDetectionType;
import apostolos.datastreams.Configuration.ProxyTypes;
import apostolos.datastreams.DatastreamItem;

/**
 * ChangeDetectionProxy is a data stream proxy (i.e., extends {@link apostolos.datastreams.proxies.BaseProxy})
 * which stores all the received values of a monitored data stream into the cache, but forwards
 * to the Cloud only values that are different than the previous item.
 * Depending on the used variation, this "difference" might be required to exceed a certain threshold. 
 * 
 * @author Apostolos Papageorgiou
 */
public class ChangeDetectionProxy extends BaseProxy {

	private ChangeDetectionType changeDetectionType;
	private double threshold;
	
	/**
	 * ChangeDetectionProxy Constructor that uses a default value for the variation of the Change Detection algorithm to be used.
	 * 
	 * @param name the name of the data stream that will be proxied by this proxy
	 */
	public ChangeDetectionProxy(String name) {
		this.proxyType = ProxyTypes.CHANGE_DETECTION;
		this.changeDetectionType = getChangeDetectionTypeFromString(Configuration.DEFAULT_CHANGE_DETECTION_TYPE);
		this.threshold = (double) Configuration.DEFAULT_CHANGE_DETECTION_THRESHOLD;
		datastreamName = name;
		cache = new ArrayList<DatastreamItem>();
	}
	
	/**
	 * ChangeDetectionProxy Constructor (overloaded) that takes the variation and the threshold of the Change Detection algorithm also as input parameters
	 * 
	 * @param name the name of the data stream that will be proxied by this proxy
	 * @param cdv the variation of the Change Detection algorithm to be used
	 * @param threshold the threshold of the Change Detection algorithm to be used (if the variation is threshold-based)
	 */
	public ChangeDetectionProxy(String name, String cdv, double threshold) {
		this.proxyType = ProxyTypes.CHANGE_DETECTION;
		this.changeDetectionType = getChangeDetectionTypeFromString(cdv);
		this.threshold = threshold;
		datastreamName = name;
		cache = new ArrayList<DatastreamItem>();
	}
	
	/* (non-Javadoc)
	 * @see apostolos.datastreams.proxies.BaseProxy#process(java.lang.String)
	 */
	@Override
	public boolean process(String value) {
		
		String ts = Util.getCurrTimeAsTimestampString();
		
		return process(value, ts);
	}

	/* (non-Javadoc)
	 * @see apostolos.datastreams.proxies.BaseProxy#process(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean process(String value, String timestamp) {
		
		DatastreamItem currItem = new DatastreamItem(cache.size(), datastreamName, value, Timestamp.valueOf(timestamp));
		DatastreamItem prevItem = null;
		
		Util.writeToCache(cache, currItem);
		
		if (cache.size() > 1) {
			prevItem = cache.get(cache.size()-2);
		} else {
			Util.sendToCloud(currItem);
			return true;
		}
		
		double diff = 0.0;

		if (changeDetectionType == ChangeDetectionType.THRESHOLD_BASED) {
			diff = calculateDiff(currItem, prevItem);
			if (diff > threshold) {
				Util.sendToCloud(currItem);
				return true;
			} else {
				return false;
			}
		} else { // variation == ChangeDetectionVariation.STRICT
			log.info("prev="+prevItem.getValue());
			log.info("curr="+currItem.getValue());
			if (!( (currItem.getValue()).equals(prevItem.getValue()) )) {
				Util.sendToCloud(currItem);
				return true;
			} else {
				return false;
			}
		}

	}
	
	/**
	 * Calculates for numeric values the degree of change of a {@link DatastreamItem} compared to another {@link DatastreamItem}
	 * 
	 * @param currItem A numeric data stream item
	 * @param prevItem Another numeric data stream item
	 * @return the degree to which the first argument is "different" compared to the second argument  
	 */
	private double calculateDiff(DatastreamItem currItem, DatastreamItem prevItem) {
		double diff = 0.0;
		
		try {
			diff = Math.abs( ((Double.valueOf(currItem.getValue()) - Double.valueOf(prevItem.getValue())) / Double.valueOf(prevItem.getValue())) );
		} catch (NumberFormatException nfe) {
			log.error("Threshold-based change detection can only be applied to numeric values, so no diff is returned: " + nfe.getMessage());
		}
		
		return diff;
	}
	
	/**
	 * Transforms a string representation of a change detection variation to the respective
	 * {@link ChangeDetectionVariation} value and returns the latter.
	 * It returns the variation {@link ChangeDetectionVariation#LOSSLESS}
	 * if the parameter does not correspond with any known variation
	 * 
	 * @param var the string representation of a change detection variation
	 * @return the change detection variation that corresponds with the string representation provided as input (or {@link ChangeDetectionVariation#LOSSLESS} if no other corresponding variation exists)
	 */
	private ChangeDetectionType getChangeDetectionTypeFromString(String var) {
		if (var.equals("strict")) {
			return ChangeDetectionType.STRICT;
		} else if (var.equals("threshold_based")) {
			return ChangeDetectionType.THRESHOLD_BASED;
		} else {
			log.error(var + " is an unknown change detection type. Using strict change detection instead.");
			return ChangeDetectionType.STRICT;
		}
	}
	
	/*-------------- GETTERS and SETTERS -------------*/

	/**
	 * @return the type of logic used for the Change Detection algorithm
	 */
	public ChangeDetectionType getChangeDetectionType() {
		return changeDetectionType;
	}

	/**
	 * @param changeDetectionType the type of logic used for the Change Detection algorithm
	 */
	public void setChangeDetectionType(ChangeDetectionType changeDetectionType) {
		this.changeDetectionType = changeDetectionType;
	}

	/**
	 * @return the threshold used for the Change Detection algorithm, if a threshold-based logic is used
	 */
	public double getThreshold() {
		return threshold;
	}

	/**
	 * @param threshold the threshold to be used for the Change Detection algorithm, if a threshold-based logic is used
	 */
	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

}
