/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.datastreams.proxies;

import java.awt.geom.Line2D;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import apostolos.datastreams.Configuration;
import apostolos.datastreams.Configuration.ProxyTypes;
import apostolos.datastreams.Util;
import apostolos.datastreams.DatastreamItem;

/**
 * ImportantPointsProxy is a data stream proxy (i.e., extends {@link apostolos.datastreams.proxies.BaseProxy})
 * which stores all the received values of a monitored data stream into the cache, but sends
 * to the Cloud only the values that are measured to be important based on a variation
 * of the mathematical method described in:
 * [F. Chung, T. Fu, R. Luk, and V. Ng. Flexible time series pattern matching based on Perceptually Important Points.
 * In International Joint Conference on Artificial Intelligence, Workshop on Learning from Temporal and Spatial Data, pages 1–7, 2001.]
 * 
 * @author Apostolos Papageorgiou
 */
public class ImportantPointsProxy extends BaseProxy {
	private double importanceThreshold;
	private List<DatastreamItem> analyticsCache;

	/**
	 * ImportantPointsProxy Constructor that uses the default importance threshold
	 * 
	 * @param name the name of the data stream that will be proxied by this proxy
	 */
	public ImportantPointsProxy(String name) {
		this.proxyType = ProxyTypes.IMPORTANT_POINTS;
		this.importanceThreshold = Configuration.DEFAULT_IMPORTANT_POINTS_THRESHOLD;
		datastreamName = name;
		cache = new ArrayList<DatastreamItem>();
		analyticsCache = new ArrayList<DatastreamItem>(); // This is smaller than the cache, i.e., contains only the latest items, and only the items in this cache are used for the calculation of the importance of an item
	}
	
	/**
	 * ImportantPointsProxy Constructor (overloaded) that takes the importance threshold also as an input parameter
	 * 
	 * @param name the name of the data stream that will be proxied by this proxy
	 * @param threshold the importance threshold to be used by this proxy
	 */
	public ImportantPointsProxy(String name, double threshold) {
		this.proxyType = ProxyTypes.IMPORTANT_POINTS;
		this.importanceThreshold = threshold;
		datastreamName = name;
		cache = new ArrayList<DatastreamItem>();
		analyticsCache = new ArrayList<DatastreamItem>();
	}
	
	/* (non-Javadoc)
	 * @see apostolos.datastreams.proxies.BaseProxy#process(java.lang.String)
	 */
	@Override
	public boolean process(String value) {
		
		String ts = Util.getCurrTimeAsTimestampString();
		
		return process(value, ts);
	}

	/* (non-Javadoc)
	 * @see apostolos.datastreams.proxies.BaseProxy#process(java.lang.String, java.lang.String)
	 */	
	@Override
	public boolean process(String value, String timestamp) {

		DatastreamItem currItem = new DatastreamItem(cache.size(), datastreamName, value, Timestamp.valueOf(timestamp));
		try {
			Double.valueOf(currItem.getValue());
		} catch (NumberFormatException nfe) {
			log.error("Dropping the item, because this proxy can only work with real numbers: " + nfe.getMessage());
			return false;
		}
		List<Integer> orderedCachedItems = new ArrayList<Integer>();
		
		Util.writeToCache(cache, currItem);
		analyticsCache.add(currItem);
		
		List<DatastreamItem> cacheProjection = new ArrayList<DatastreamItem>();
		
		ArrayList<Double> cacheProjectionValues = new ArrayList<Double>();
		for (DatastreamItem cacheItem : analyticsCache) {
			cacheProjection.add(cacheItem); 
			cacheProjectionValues.add(Double.valueOf(cacheItem.getValue()));
		}
		
		// In this variation of the algorithm, the (analytics) cache items are appended again at the end of the cache (creating a copy of the cache that resembles a predcition of a possible future), in order to compensate for the fact that no "future data" are available for the analysis.
		for (DatastreamItem cacheItem : analyticsCache) {
			cacheProjection.add(cacheItem);
			cacheProjectionValues.add(Double.valueOf(cacheItem.getValue()));
		}
		//cacheProjection.add(currItem);
		//cacheProjectionValues.add(Double.valueOf(currItem.getValue()));
		
		orderedCachedItems = orderItemsBasedOnDistanceToAdjacentImportantPoints(cacheProjection);
		double currItemImportance = normalizeItemImportance(orderedCachedItems, cacheProjection.size()-2); // cacheProjection.size()-2 is the position of the current item in cacheProjection
		
		if (currItemImportance > importanceThreshold  || cache.size() < 2) {
			// Important point!
			Util.sendToCloud(currItem);
			if (analyticsCache.size() == Configuration.ANALYTICS_CACHE_SIZE) {
				analyticsCache.remove(0); 
			}
			return true;
		} else {
			// Not important point.
			if (analyticsCache.size() == Configuration.ANALYTICS_CACHE_SIZE) {
				analyticsCache.remove(0); 
			}
			return false;
		}

	}
	
	/**
	 * Computes a list of positions of the items of a data stream,
	 * ranked in order of importance based on the maximum distance concept.
	 * 
	 * @param ds the examined data stream
	 * @return the positions of the important points of the data stream in order of importance
	 */
	public List<Integer> orderItemsBasedOnDistanceToAdjacentImportantPoints(List<DatastreamItem> ds) {
		List<Integer> order = new ArrayList<Integer>();
		// initialization of the iteratively growing list of important points, i.e., selection of the first and the last item as the two first points
		order.add(0);
		order.add(ds.size()-1);
		ArrayList<Integer> sortedOrder = new ArrayList<Integer>(order.size()); // we also need to keep a sorted version of "order" in order to find fast "adjacent" important points
		sortedOrder.add(0);
		sortedOrder.add(ds.size()-1);
		
		for (int j=1; j<ds.size()-1; j++) { // iterate as many times as the items that need to be ordered
		
			DatastreamItem currItem = null;
			double distance = 0.0;
			double maxDistance = 0.0;
			Line2D.Double currItemRelevantLine = null;
			int selection = j;
			
			for (int i=1; i<ds.size()-1; i++) { // iterate over all items ignoring the already selected ones in order to find the one with the maximum distance to its adjacent important points
				if (order.contains(i)) continue; // item has already been selected as an important point previously. Ignore it.
				currItem = ds.get(i);
				currItemRelevantLine = getLineOfAdjacentImportantPoints(ds, order, sortedOrder, i); // get the line to which the distance of the currently examined item should be checked
				if (currItemRelevantLine == null) continue; // This would happens only if the examined item had already been selected as an important point. However, this should never happen here, because in such a case this line of code shall not be reached.
				try {
					distance = currItemRelevantLine.ptLineDist(i, Double.valueOf(currItem.getValue()));
				} catch (NumberFormatException nfe) {
					log.error("The values of all items processed by an ImportantPointsProxy must be numeric values: " + nfe.getMessage());
					continue;
				}
				if (distance >= maxDistance) {
					maxDistance = distance;
					selection = i;
				}
			}
			order.add(selection); // add the item with the maximum distance to its adjacent important points to the already selected ones, thus making it the next important point
			int insertionIndex = Collections.binarySearch(sortedOrder, selection);
			sortedOrder.add(-insertionIndex-1, selection); // add it also at the right position in the sorted list of selected important points
		}
		
		return order;
	}
	
	/**
	 * Given a data stream item, a list with the positions of the items of the datastream managed by this proxy
	 * which have already been selected as important points,
	 * and the position of the currently examined item,
	 * this method finds and returns the line that connects the two important points
	 * that are adjacent to the currently examined item
	 * 
	 * @param ts a data stream
	 * @param order a list with the positions of the items of the data stream which have already been selected as important points
	 * @param sortedOrder a sorted version of the above list ("order")
	 * @param itemPos the position of the currently examined item in the data stream
	 * @return the line that connects the two important points that are adjacent to the currently examined item
	 */
	public Line2D.Double getLineOfAdjacentImportantPoints(List<DatastreamItem> ts, List<Integer> order, List<Integer> sortedOrder, int itemPos) {
		
		int leftBound = order.get(0); // the leftmost point of the data stream
		int rightBound = order.get(1); // the rightmost point of the data stream
		int itemPosInsertIndex = Collections.binarySearch(sortedOrder, itemPos);
		if (itemPosInsertIndex < 0) {
			leftBound = sortedOrder.get(-itemPosInsertIndex-2);
			rightBound = sortedOrder.get(-itemPosInsertIndex-1);
		} else { // if binarySearch returns a positive number, this means that the examined item was found in the list, i.e., has already been previously selected as an important point. Just a check, though this should never happen here.
			log.error("This item has already been selected and should not be considered.");
			return null;
		}
		
		try {
			return new Line2D.Double(leftBound, Double.valueOf(ts.get(leftBound).getValue()), rightBound, Double.valueOf(ts.get(rightBound).getValue()));
		} catch (NumberFormatException nfe) {
			log.error("The values of all items processed by an ImportantPointsProxy must be numeric values: " + nfe.getMessage());
			return null;
		}
	}
	
	/**
	 * Computes a value of importance (between 0.0 and 1.0) of an item,
	 * analogous to its position inside an ordered list.
	 * For example, in list {5, 3, 12, 1, 2}, item "3" is in the 2nd position
	 * out of 5, and thus has a normalized importance equal to 1-(1/5)= 0.8 
	 * 
	 * @param list an ordered list of integers (in descending order of importance)
	 * @param item an integer of the ordered list
	 * @return the normalized importance (between 0.0 and 1.0) of the integer parameter depending on its position inside the list
	 */
	public double normalizeItemImportance(List<Integer> list, int item) {
		int itemIndex = list.indexOf(item);
		return (1.0 - ((double) (itemIndex-1)/(list.size()-2))); // ignoring the first and the last items which are going to be the two first important points anyway.
	}
	
	/*-------------- GETTERS and SETTERS -------------*/

	/**
	 * @return the importance threshold used for forwarding (or not) the PIPs (Perceptually Important Points)
	 */
	public double getImportanceThreshold() {
		return importanceThreshold;
	}

	/**
	 * @param importanceThreshold the importance threshold to be used for forwarding (or not) the PIPs (Perceptually Important Points)
	 */
	public void setImportanceThreshold(double importanceThreshold) {
		this.importanceThreshold = importanceThreshold;
	}

}
