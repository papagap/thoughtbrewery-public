/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.datastreams.proxies;

import java.util.List;

import org.apache.log4j.Logger;

import apostolos.datastreams.Configuration.ProxyTypes;
import apostolos.datastreams.DatastreamItem;

/**
 * BaseProxy is an abstract class that is implemented by all datastream proxies.
 * <p>
 * Its existence is necessary in order to achieve Inversion of Control,
 * which will be implemented via Dependency Injection,
 * because there are objects and methods that are used or registered
 * with a data proxy without prior knowledge of the specific type of this proxy.
 * For example, {@link apostolos.datastreams.threads.DatastreamThread} is associated with a BaseProxy object,
 * which might be an instance of any proxy, i.e., of any of the classes that implement BaseProxy.
 * In that case, the objects that will "inject the dependency" (i.e., decide the actual proxy type) will
 * be within the implementation of classes that extend {@link apostolos.datastreams.threads.DatastreamThread}
 * for a specific purpose. Either the Factory design pattern or a similar mechanism will have to be
 * used there in order to implement the Dependency Injection.  
 * 
 * @author Apostolos Papageorgiou
 */
public abstract class BaseProxy {

	protected Logger log = Logger.getLogger(this.getClass());
	
	protected String datastreamName;
	protected ProxyTypes proxyType;
	protected List<DatastreamItem> cache;
	
	/**
	 * Is implemented differently by each class that extends this abstract class,
	 * in order to process a data stream item according to the desired type of proxying.
	 * 
	 * @param value a received data stream item
	 * @return true if the value has been also sent to the Cloud, false if it has been only stored in the (in-memory) cache
	 */
	public abstract boolean process(String value);

	/**
	 * Like {@link #process(String)}, but overloaded for the case that the timestamp
	 * is also provided by the data collection thread (and is not to be captured by the library).
	 * 
	 * @param value a received data stream item
	 * @param timestamp the timestamp of the received value
	 * @return true if the value has been also sent to the Cloud, false if it has been only stored in the (in-memory) cache
	 */
	public abstract boolean process(String value, String timestamp);
	
	/**
	 * Clears the (in-memory) cache, as well as the cachePositionsOfForwardedItems list, which refers refers to the (in-memory) cache.
	 */
	public void resetCache() {
		cache.clear();
	}

	/*-------------- GETTERS and SETTERS -------------*/

	/**
	 * @return the name of the data stream processed by this proxy
	 */
	public String getDatastreamName() {
		return datastreamName;
	}

	/**
	 * @param name the name of the data stream processed by this proxy
	 */
	public void setDatastreamName(String name) {
		this.datastreamName = name;
	}

	/**
	 * @return the cache with the locally stored data stream items
	 */
	public List<DatastreamItem> getCache() {
		return cache;
	}

	/**
	 * @return the type of proxying performed by this proxy
	 */
	public ProxyTypes getProxyType() {
		return proxyType;
	}

	/**
	 * @param proxyType the type of proxying to be performed by this proxy
	 */
	public void setProxyType(ProxyTypes proxyType) {
		this.proxyType = proxyType;
	}

}
