/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.datastreams.proxies;

import java.sql.Timestamp;
import java.util.ArrayList;

import apostolos.datastreams.Configuration;
import apostolos.datastreams.Configuration.ProxyTypes;
import apostolos.datastreams.Util;
import apostolos.datastreams.DatastreamItem;

/**
 * SamplingHandler is a data stream proxy (i.e., extends {@link apostolos.datastreams.proxies.BaseProxy})
 * which stores all the received values of a monitored data stream into the cache, but forwards only one out of every n
 * values to the Cloud, depending on the sampling interval n, which is provided during instantiation or as a default value.
 * A sampling interval equal to 0 means "never forward".
 * 
 * @author Apostolos Papageorgiou
 */
public class SamplingProxy extends BaseProxy {
	
	private int interval;
	private int counter;

	/**
	 * SamplingProxy Constructor that uses a default value for the interval
	 * 
	 * @param name the name of the data stream that will be proxied by this proxy
	 */
	public SamplingProxy(String name) {
		this.proxyType = ProxyTypes.SAMPLING;
		this.interval = Configuration.DEFAULT_SAMPLING_INTERVAL;
		this.counter = 1;
		datastreamName = name;
		cache = new ArrayList<DatastreamItem>();
	}
	
	/**
	 * SamplingProxy Constructor (overloaded) that takes the interval attribute also as an input parameter
	 * 
	 * @param name the name of the data stream that will be proxied by this proxy
	 * @param intvl the interval to be used for the sampling
	 */
	public SamplingProxy(String name, int intvl) {
		this.proxyType = ProxyTypes.SAMPLING;
		this.interval = intvl;
		this.counter = 1;
		this.datastreamName = name;
		this.cache = new ArrayList<DatastreamItem>();
	}

	/* (non-Javadoc)
	 * @see apostolos.datastreams.proxies.BaseProxy#process(java.lang.String)
	 */
	@Override
	public boolean process(String value) {
		
		String ts = Util.getCurrTimeAsTimestampString();
		
		return process(value, ts);
	}
	
	/* (non-Javadoc)
	 * @see apostolos.datastreams.proxies.BaseProxy#process(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean process(String value, String timestamp) {

		DatastreamItem currItem = new DatastreamItem(cache.size(), datastreamName, value, Timestamp.valueOf(timestamp));
		Util.writeToCache(cache, currItem);
		if (interval == 0) {
			return false;
		} else if (counter % interval == 1 || interval == 1) {
			Util.sendToCloud(currItem);
			counter++;
			return true;
		} else if (counter == interval) {
			counter = 1;
			return false;
		} else {
			counter++;
			return false;
		}
	}
	
	/*-------------- GETTERS and SETTERS -------------*/

	/**
	 * @return the interval to be used for sampling by this proxy
	 */
	public int getInterval() {
		return interval;
	}

	/**
	 * @param interval the interval to be used for sampling by this proxy
	 */
	public void setInterval(int interval) {
		this.interval = interval;
	}
	
	/**
	 * @return the counter that shows the relative order of the next value that will be received (to assist the sampling)
	 */
	public int getCounter() {
		return counter;
	}
	
}
