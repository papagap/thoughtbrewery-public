/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.datastreams.proxies;

import java.sql.Timestamp;
import java.util.ArrayList;

import apostolos.datastreams.Configuration;
import apostolos.datastreams.Configuration.ProxyTypes;
import apostolos.datastreams.Util;
import apostolos.datastreams.DatastreamItem;

/**
 * AggregationProxy is a data stream proxy (i.e., extends {@link apostolos.datastreams.proxies.BaseProxy})
 * which stores all the received values of a monitored data stream into the cache, but forwards
 * to the Cloud only a mean value for every N data items of the original data set
 * (ignoring -but caching- items with values that are not real numbers).
 * N is variable and it is called the "window size" of the aggregation.
 * 
 * @author Apostolos Papageorgiou
 */
public class AggregationProxy extends BaseProxy {
	
	private int windowSize;
	private int counter;
	private String aggrValue;

	/**
	 * AggregationProxy Constructor that uses a default value for the window size
	 * 
	 * @param name the name of the data stream that will be proxied by this proxy
	 */
	public AggregationProxy(String name) {
		this.proxyType = ProxyTypes.AGGREGATION;
		this.windowSize = Configuration.DEFAULT_AGGREGATION_WINDOW_SIZE;
		this.counter = 1;
		this.aggrValue = null;
		datastreamName = name;
		cache = new ArrayList<DatastreamItem>();
	}
	
	/**
	 * AggregationProxy Constructor (overloaded) that takes the window size attribute also as an input parameter
	 * 
	 * @param name the name of the data stream that will be proxied by this proxy
	 * @param windowSize the window size to be used for the aggregation (i.e., the number of items to be averaged/summarized before being sent to the Cloud)
	 */
	public AggregationProxy(String name, int windowSize) {
		this.proxyType = ProxyTypes.AGGREGATION;
		this.windowSize = windowSize;
		this.counter = 1;
		this.aggrValue = null;
		datastreamName = name;
		cache = new ArrayList<DatastreamItem>();
	}
	
	/* (non-Javadoc)
	 * @see apostolos.datastreams.proxies.BaseProxy#process(java.lang.String)
	 */
	@Override
	public boolean process(String value) {
		
		String ts = Util.getCurrTimeAsTimestampString();
		
		return process(value, ts);
	}

	/* (non-Javadoc)
	 * @see apostolos.datastreams.proxies.BaseProxy#process(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean process(String value, String timestamp) {
		
		DatastreamItem currItem = new DatastreamItem(cache.size(), datastreamName, value, Timestamp.valueOf(timestamp));
		Util.writeToCache(cache, currItem);
		if (aggrValue == null) {
			aggrValue = value;
		} else {
			try {
				double tmpVal = Double.valueOf(aggrValue);
				double currVal = Double.valueOf(value);
				tmpVal = (tmpVal * (counter-1) + currVal) / counter;
				aggrValue = Double.toString(tmpVal);
			} catch (NumberFormatException nfe) {
				log.error("The current value will be ignored for the aggregation because it was not a real number: " + nfe.getMessage());
				return false;
			}
		}
		DatastreamItem aggrItem = new DatastreamItem(cache.size(), datastreamName, aggrValue, Timestamp.valueOf(timestamp));
		if (counter == windowSize) {
			System.out.println("Forwarding aggrValue " + aggrItem.getValue());
			Util.sendToCloud(aggrItem);
			counter = 1;
			aggrValue = null;
			return true;
		} else {
			System.out.println("Not forwarding aggrValue " + aggrItem.getValue());
			counter++;
			return false;
		}
	}
	
	/*-------------- GETTERS and SETTERS -------------*/
	
	/**
	 * @return the window size that is used for the aggregation
	 */
	public int getWindowSize() {
		return windowSize;
	}

	/**
	 * @param windowSize the window size to be used for the aggregation
	 */
	public void setWindowSize(int windowSize) {
		this.windowSize = windowSize;
	}
	
	/**
	 * @return the counter that shows the relative order of the next value that will be received, in order to assist the aggregation
	 */
	public int getCounter() {
		return counter;
	}

	/**
	 * @return the current aggregated value, which will only be forwarded when the number of elements that have been used to compute it has reached the window size
	 */
	public String getAggrValue() {
		return aggrValue;
	}

}
