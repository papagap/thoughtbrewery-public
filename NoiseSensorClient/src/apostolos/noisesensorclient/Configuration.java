/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.noisesensorclient;

//import java.io.File;
//import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Configuration is a static class used for maintaining system properties.
 * <p>
 * It reads the configuration from a java properties file, the path and name of which are also specified in this class.
 *
 * @author Apostolos Papageorgiou
 */
public class Configuration {

	private static Logger log = Logger.getLogger("Configuration");
	
	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	private static final String CONFIG_FILE_NS = "apostolos.noisesensorclient.";
	//private static final String CONFIG_FILE_DIR = "resources" + File.separator;
	private static final String CONFIG_FILE_NAME = "noisesensorclient.properties";
	
	public static int POLLING_INTERVAL = 5000;
	
	public static String REQUESTED_PROXY_DIR = "/dir/of/file/that/contains/requested/proxy";
	public static String REQUESTED_PROXY_FILENAME = "filename.extension";
	
	public static String NOISE_WEB_SERVICE_BASE_URL = "https://api.smartcitizen.me/v0/devices/";
	public static ArrayList<String> NOISE_SENSOR_IDS = new ArrayList<String>();
	
	/**
	 * Sets the values of the Configuration public class variables by reading the properties file.
	 * 
	 * @return status of reading in the properties file (true for success or false for failure)
	 */
	public static boolean readPropertiesFromConfigFile() {
		Properties properties = new Properties();
		try {
			
		    //properties.load(new FileInputStream(new File(CONFIG_FILE_DIR + CONFIG_FILE_NAME)));
			properties.load(Configuration.class.getResourceAsStream("/" + CONFIG_FILE_NAME));
			properties.load(Configuration.class.getResourceAsStream("/log4j.properties"));
			PropertyConfigurator.configure(properties);
		    
		    if (properties.containsKey(CONFIG_FILE_NS + "polling_interval")) {
		    	POLLING_INTERVAL = Integer.parseInt(properties.getProperty(CONFIG_FILE_NS + "polling_interval"));
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "requested_proxy_dir")) {
		    	REQUESTED_PROXY_DIR = properties.getProperty(CONFIG_FILE_NS + "requested_proxy_dir");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "requested_proxy_filename")) {
		    	REQUESTED_PROXY_FILENAME = properties.getProperty(CONFIG_FILE_NS + "requested_proxy_filename");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "noise_web_service_base_url")) {
		    	NOISE_WEB_SERVICE_BASE_URL = properties.getProperty(CONFIG_FILE_NS + "noise_web_service_base_url");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "noise_sensor_ids")) {
		    	String noiseSensorIdsString = properties.getProperty(CONFIG_FILE_NS + "noise_sensor_ids");
		    	NOISE_SENSOR_IDS = new ArrayList<String>(Arrays.asList(noiseSensorIdsString.split(","))); 
		    }
		    return true;
		    
		} catch (IOException ioe) {
			log.error("Error reading configuration: " + ioe.getMessage());
			return false;
		}
	}

	
}
