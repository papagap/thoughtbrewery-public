/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.noisesensorclient;

import java.io.IOException;
import java.nio.file.Paths;

import org.apache.log4j.Logger;

/**
 * NoiseSensorClientRunner is an executable (Main) class that starts a noise sensor data stream monitoring thread as well as
 * a directory-watching thread for enabling the switching of the used data stream proxy type at runtime. 
 * 
 * @author Apostolos Papageorgiou
 */
public class NoiseSensorClientRunner {

	public static void main(String[] args) {

		Logger log = Logger.getLogger("Noise Sensor Client Main");
		
		Configuration.readPropertiesFromConfigFile();
		apostolos.datastreams.Configuration.readPropertiesFromConfigFile();
		
		NoiseDatastreamThread st = new NoiseDatastreamThread();
		st.start();
		
		RequestedProxyDirectoryWatchThread wt = null;
		try {
			wt = new RequestedProxyDirectoryWatchThread(Paths.get(Configuration.REQUESTED_PROXY_DIR), false, st);
		} catch (IOException ioe) {
			log.error("The directory watch thread could not be instantiated: " + ioe.getMessage());
		}
	
		if (wt != null) {
			wt.start();
		} else {
			log.error("The directory watch thread could not be started.");
		}

	}

}
