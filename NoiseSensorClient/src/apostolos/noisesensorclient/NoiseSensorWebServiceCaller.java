/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.noisesensorclient;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * NoiseSensorWebServiceCaller provides a method to call a publicly available Web Service
 * that retrieves -among others- noise sensor levels.
 * The target sensors are given as inputs by providing their id, which must be known and
 * listed in the properties file.
 * Note that this class has some generic value but it is specifically implemented for the used
 * Web Service and should be modified in order to consume different Web Services.
 * 
 * @author Apostolos Papageorgiou
 */
public class NoiseSensorWebServiceCaller {
	
	private static Logger log = Logger.getLogger("Util");
	
	public static double getNoiseLevel(String sensorId) {
		log.debug("Retrieving the noise level (in decibels) of sensor with id " + sensorId + " from " + Configuration.NOISE_WEB_SERVICE_BASE_URL);

		String response = "";
		double value = 0.0;
		try {
			ClientResource clientResource = new ClientResource(Configuration.NOISE_WEB_SERVICE_BASE_URL + sensorId); 
			response = clientResource.get().getText();
			log.debug("Received response: " + response);
			JsonObject responseJson = new JsonParser().parse(response).getAsJsonObject();
			value = responseJson.get("data").getAsJsonObject().get("sensors").getAsJsonArray().get(4).getAsJsonObject().get("raw_value").getAsDouble();
		} catch (ResourceException re){
			log.error("Web service call failed: " + re.getMessage());
		} catch (IOException ioe) {
			log.error("Web service response parsing failed: " + ioe.getMessage());
		}
		
		return value;
	}

}
