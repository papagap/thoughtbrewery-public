/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.noisesensorclient;

import apostolos.datastreams.proxies.SamplingProxy;
import apostolos.datastreams.threads.DatastreamThread;

/**
 * NoiseDatastreamThread uses the methods of the {@link apostolos.noisesensorclient.NoiseSensorWebServiceCaller} in order to
 * periodically fetch noise sensor values and pass them to its proxy for processing.
 * The polling interval and the queried sensors are provided as properties, while the proxy class variable (inherited by
 * {@link apostolos.datastreams.threads.DatastreamThread} can be dynamically changed (without interrupting this thread).
 * 
 * @author Apostolos Papageorgiou
 */
public class NoiseDatastreamThread extends DatastreamThread {

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run(){
		
		String datastreamName = "NoiseLevel";
		
		setProxy(new SamplingProxy(datastreamName, 5));
		
		while (true) {
			String val = String.valueOf(NoiseSensorWebServiceCaller.getNoiseLevel(Configuration.NOISE_SENSOR_IDS.get(0)));
			
			proxy.process(val);
			
			val = String.valueOf(NoiseSensorWebServiceCaller.getNoiseLevel(Configuration.NOISE_SENSOR_IDS.get(1)));
			
			proxy.process(val);
			
			try {
				Thread.sleep(Configuration.POLLING_INTERVAL); 
			} catch (InterruptedException ie) {
				log.error("The data collector was interrupted: " + ie.getMessage());
			}
		}
	}
}
