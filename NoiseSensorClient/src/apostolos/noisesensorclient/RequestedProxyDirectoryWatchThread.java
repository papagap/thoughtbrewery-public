/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.noisesensorclient;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;

import apostolos.datastreams.proxies.AggregationProxy;
import apostolos.datastreams.proxies.ChangeDetectionProxy;
import apostolos.datastreams.proxies.ImportantPointsProxy;
import apostolos.datastreams.proxies.SamplingProxy;
import apostolos.datastreams.threads.DatastreamThread;
import apostolos.datastreams.threads.DirectoryWatchThread;

/**
 * RequestedProxyDirectoryWatchThread is a directory-watching thread (i.e., extends {@link apostolos.datastreams.threads.DirectoryWatchThread}),
 * which monitors changes in a file in order to identify if a different type of data stream proxy has been reuqested.
 * The name of the monitored file is provided as a property, and as soon as the file is changed in order to indicate that a new proxy type
 * "X" is requested for a specific data stream processing thread (which is also passed to the constructor of this class), then
 * the proxy used by the data stream processing thread is automatically set to be "X" (without interrupting the data stream processing thread).  
 * 
 * @author Apostolos Papageorgiou
 */
public class RequestedProxyDirectoryWatchThread extends DirectoryWatchThread {

	private DatastreamThread dsThread;

	/**
	 * RequestedProxyDirectoryWatchThread Constructor:
	 * 
	 * Uses the constructor of its superclass ({@link apostolos.datastreams.threads.DirectoryWatchThread}) to create a (non-)recursive watch
	 * service for a given path (directory + filename). Additionally, it initiates the class variable that will hold the data stream processing thread, for which
	 * a proxy type change might be requested via changes in the monitored file. 
	 * 
	 * @param path the path of the directory to be watched for changes in the desired proxy type
	 * @param recursive if true then also sub-directories are watched for changes
	 * @param dsThread the thread that manages the data stream for which the current thread manages the requested proxy types 
	 * @throws IOException if the provided path is not valid or cannot be accessed
	 */
	public RequestedProxyDirectoryWatchThread(Path path, boolean recursive, DatastreamThread dsThread) throws IOException {
		super(path, recursive);
		this.dsThread = dsThread;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run() {
        while (true) {

            WatchKey key;
            try {
                key = watcher.take();
            } catch (InterruptedException x) {
                return;
            }

            Path path = keys.get(key);
            if (path == null) {
                log.error("WatchKey not recognized!");
                continue;
            }

            for (WatchEvent<?> event: key.pollEvents()) {
                WatchEvent.Kind<?> kind = event.kind();

                if (kind == OVERFLOW) {
                    continue;
                }

                // Note that the context for the directory entry event is the file name of the entry
                WatchEvent<Path> ev = cast(event);
                Path name = ev.context();
                Path child = path.resolve(name);
                log.info(event.kind().name() + ": " + child);

                // if the thread is watching recursively and a directory is created, then register the newly created directory and its sub-directories
                if (recursive && (kind == ENTRY_CREATE)) {
                    try {
                        if (Files.isDirectory(child, NOFOLLOW_LINKS)) {
                            registerAll(child);
                        }
                    } catch (IOException ioe) {
                        log.error(ioe.getMessage());
                    }
                }
                
                // Note that multiple file modification events can be fired for the same modification for some file systems and/or file types.
                if (child.toString().equals(Configuration.REQUESTED_PROXY_DIR + File.separator + Configuration.REQUESTED_PROXY_FILENAME)) {
                	String requestedProxyType = "reqProxyType";
                	try {
						BufferedReader br = new BufferedReader(new FileReader(Configuration.REQUESTED_PROXY_DIR + File.separator + Configuration.REQUESTED_PROXY_FILENAME));
						requestedProxyType = br.readLine();
						br.close();
					} catch (FileNotFoundException fnfe) {
						log.error("The file with the requested proxy type was not found: " + fnfe.getMessage());
						return;
					} catch (IOException ioe) {
						log.error("The file with the requested proxy type could not be correctly read: " + ioe.getMessage());
					}
                	log.info("New proxy type requested: " + requestedProxyType + "!" + "\n");
                	switchProxy(requestedProxyType);
                }
            }

            // Reset the key and remove it from the set if the directory is no longer accessible
            boolean valid = key.reset();
            if (!valid) {
                keys.remove(key);

                if (keys.isEmpty()) {
                    break;
                }
            }
        }
	}
	
	/**
	 * Switches the type of the proxy used by the data stream processing thread associated with this class
	 * according to the requestedProxyType argument, which is a string-encoded proxy type.
	 * This is a kind of Factory Method, which creates the appropriate type of proxy object and
	 * -instead of returning it as a typical Factory Method would do-
	 * uses it in order to set the proxy of a running DatastreamThream,
	 * namely the NoiseDatastreamThread that the current class is associated with.
	 * Thus, it is here that the Dependency Injection of the proxy type of a datastream thread is actually implemented.
	 * 
	 * @param requestedProxyType a string encoding of the requested proxy type
	 */
	private void switchProxy(String requestedProxyType) {
		if (dsThread==null || dsThread.getProxy()==null) {
			log.error("The proxy cannot be switched because either the current thread or the current proxy were null.");
    		return;
    	}
		switch(requestedProxyType) {		
			case "aggregation":
				if (dsThread.getProxy().getClass().equals(AggregationProxy.class)) {
					log.info("This proxy type is currently already in use anyway, so the request will be ignored.");
					return;
				} else {
					dsThread.setProxy(new AggregationProxy(dsThread.getProxy().getDatastreamName()));
				}
				break;
				
			case "change_detection":
				if (dsThread.getProxy().getClass().equals(ChangeDetectionProxy.class)) {
					log.info("This proxy type is currently already in use anyway, so the request will be ignored.");
					return;
				} else {
					dsThread.setProxy(new ChangeDetectionProxy(dsThread.getProxy().getDatastreamName()));
				}
				break;
				
			case "important_points":
				if (dsThread.getProxy().getClass().equals(ImportantPointsProxy.class)) {
					log.info("This proxy type is currently already in use anyway, so the request will be ignored.");
					return;
				} else {
					dsThread.setProxy(new ImportantPointsProxy(dsThread.getProxy().getDatastreamName(), apostolos.datastreams.Configuration.DEFAULT_IMPORTANT_POINTS_THRESHOLD));
				}
				break;
	    
			case "sampling":
				if (dsThread.getProxy().getClass().equals(SamplingProxy.class)) {
					log.info("This proxy type is currently already in use anyway, so the request will be ignored.");
					return;
				}else {
					dsThread.setProxy(new SamplingProxy(dsThread.getProxy().getDatastreamName()));
				}
				break;
	
			default: 
				log.error("The proxy caannot be switched because the requested proxy type was unknown.");
				break;
		}
	}

}
