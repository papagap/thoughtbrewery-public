#-------------------------------------------------------------------------------
# Copyright 2019 Apostolos Papageorgiou
# 
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License.  You may obtain a copy
# of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
# License for the specific language governing permissions and limitations under
# the License.
#-------------------------------------------------------------------------------
# NoiseSensorClient

This is a java application (restlet) that collects data from a public source and stores it in a database according to a selected proxying logic. The purpose of the project within the thoughtbrewery domain is to use the Datastreams library in order to manage the data coming from a public source in various ways.

## Usage

Run the client as an executable java application with the "java -jar" command (see below how to build it) in order to collect and manage data from the used public source.

Visit thoughtbrewery.net in order to see a demo application that uses this software and visualizes the data that it manages.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

Make sure you have first installed the Datastreams project in the local maven repository.
Edit the java properties (resources/noisesensorclient.properties file) to reflect your system settings and preferences, and execute "mvn clean package" in the root folder of this project in order to create an executable jar file.

## Authors

* **Apostolos Papageorgiou** - [thoughtbrewery](https://thoughtbrewery.net)

## License

This project is licensed under the Apache 2.0 license. Please see the LICENSE file for more details.
