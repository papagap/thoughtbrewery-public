// map-related "global" variables
var map;
var vectorSource;
var selectInteraction;

// status-related "global" variables
var loadingMsg = "Loading...";


$(window).on('load', function() {
	if (Login.isUserLoggedIn()) {
		Login.displayIn();
	}
	startMap();
	getEventsList();
});

function startMap() {
	var cw = $('#dinnerMap').width();
	$('#dinnerMap').css({'height':3*cw/4+'px'});
    
	vectorSource = new ol.source.Vector({});
	
	var tileLayer = new ol.layer.Tile({
		source: new ol.source.OSM()
	});
	
	var vectorLayer = new ol.layer.Vector({
		source: vectorSource
	});
	
    map = new ol.Map({
    	layers: [tileLayer, vectorLayer],
        target:'dinnerMap',                                                                  
        renderer:'canvas',
        moveTolerance: 5,
        view: new ol.View({
        	projection: 'EPSG:3857',
        })                                                                         
    });
	                                 
	center = new ol.proj.transform([8.709543, 49.414224],                                  
	                               'EPSG:4326', // essentially LonLat                      
	                               map.getView().getProjection());
	map.getView().setCenter(center);                                                  
	map.getView().setZoom(5);
	
	selectInteraction = new ol.interaction.Select();
	
	selectInteraction.getFeatures().on("add", function (e) {
	     var feature = e.element;
	     var coord = feature.getGeometry().getCoordinates();
	     popup.show(coord, feature.get("name") + "<br><br>INFO: " + feature.get("info") + "<br>OWNER: " + feature.get("email") + "<br>PLACE: " + feature.get("place") + "<br>ADDRESS: " + feature.get("address") + "<br>TYPE: " + feature.get("type") + "<br>REGISTRATION TIME: " + feature.get("regTime") + "<br>EVENT TIME: " + feature.get("time"));
	     $('#event-list').val(feature.get("name"));
	     selectInteraction.getFeatures().clear();
	});
	
	map.addInteraction(selectInteraction);
	
	var popup = new ol.Overlay.Popup();
	map.addOverlay(popup);
	
	/*
	// Alternative way of implementing a handler for when the user "clicks on a feature" (instead of using the selectInteraction "on add" event).
	// In this case, if we wanted the function to be triggered also when programmatically selecting a feature (or somewhere else), we would have to
	// decouple it from the code below, putting it into an independent function, which would be called below and anywhere else. 
	map.on("click", function(e) {
	    map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
	        popup.show(e.coordinate, feature.get("name") + " / " + feature.get("info"));
	        $('#event-list').val(feature.get("name"));
	    })
	});
	*/
}

function createMapFeatureForEvent(coord, name, email, info, place, address, type, regTime, time) {
	var iconFeature = new ol.Feature({
		geometry: new ol.geom.Point(ol.proj.transform([coord[0], coord[1]], 'EPSG:4326', 'EPSG:3857')),
		name: name,
		email: email,
		info: info,
		place: place,
		address: address,
		type: type,
		regTime: regTime,
		time: time
	});
	
	iconFeature.setId(name);

	var iconStyle = new ol.style.Style({
		image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
			anchor: [0.5, 10],
		    anchorXUnits: 'fraction',
		    anchorYUnits: 'pixels',
		    opacity: 0.75,
		    src: 'assets/css/fancy-images/food.png'
		}))
	});

	iconFeature.setStyle(iconStyle);
	
	return iconFeature;
}

function deleteEvent(form) {
	
	var list = document.getElementById("event-list");
	var selectedEventName = list.options[list.selectedIndex].value;
	
	console.log("Delete event: " + selectedEventName);
	
	$('#status-text').val(loadingMsg);
	$('#status-text').css("color",'black');
	
	Util.post("deleteEvent", $(form).serialize(), function(response){
		console.log("RECEIVED: " + response);
		var msg = "Your event has been successfully deleted!";
		$('#status-text').val(msg);
		$('#status-text').css("color",'green');
		getEventsList();
	});
	
	return false;
}

function getGuestList() {
	
	var form = document.getElementById("dp-form");
	
	var list = document.getElementById("event-list");
	var selectedEventName = list.options[list.selectedIndex].value;
	
	console.log("Get Guest List for event: " + selectedEventName);
	
	$('#status-text').val(loadingMsg);
	$('#status-text').css("color",'black');
	
	Util.post("listEventRequests", $(form).serialize(), function(response){
		console.log("RECEIVED: " + response); // Note that response is of type "text" (and not, for example, json "object") due to the implementation of Util.post. Therefore, there is no need to use JSON.stringify(response).
		var guestList = jQuery.parseJSON(response);
		if(typeof guestList =='object') // It is JSON
		{
			populateGuestList(guestList);
		}
		else
		{
			// This should never happen because the response should always be valid JSON even if no candidates exist.
			// Currently the program simply shows nothing on the screen if this unexpected error occurs.
		}
	});
	
	return false;
}

function getEventsList() {
	
	console.log("Get List of all events in the DB.");
	
	Util.get("listEvents?filter=sessionemail", function(response){
		console.log("RECEIVED: " + response); // Note that response is of type "text" (and not, for example, json "object") due to the implementation of Util.get. Therefore, there is no need to use JSON.stringify(response).
		var eventList = jQuery.parseJSON(response);
		if(typeof eventList =='object') // It is JSON
		{
			populateDropDownAndMap(eventList);
		}
		else
		{
			// No events have been returned. Currently the program does nothing,
			// i.e., throws no warning, in this case. The list and the map will be simply empty.
		}
	});
	
	return false;
}

function populateDropDownAndMap(eventList) {

	var list = document.getElementById("event-list");
	var listLength = list.options.length;
	for (var i=0; i < listLength; i++) {
	  list.remove(0);
	}
	vectorSource.clear();
	
	addOption("event-list", "--- SELECT EVENT ---", true);
	
	for(var i = 0; i < eventList.length; i++) {
	    var currEvent = eventList[i];
	    var currEventBrief = currEvent.name;
	    
	    if (currEvent.email === Cookie.read("email")) {
	    	// Add event to drop-down list
		    addOption("event-list", currEventBrief, false);
		    
		    // Add event to map
		    var currEventCoord = [currEvent.lon, currEvent.lat];
		    var featureForCurrEvent = createMapFeatureForEvent(currEventCoord, currEvent.name, currEvent.email, currEvent.info, currEvent.place, currEvent.address, currEvent.type, currEvent.regTime, currEvent.time);
			vectorSource.addFeature(featureForCurrEvent);
	    }
	    
	}
}

function populateGuestList(guestList) {
	
	var msg = "";
	
	for(var i = 0; i < guestList.length; i++) {
	    var currGuest = guestList[i];
	    msg += (i+1) + ". candidate: " + currGuest.email + " says:\r\n" + currGuest.message + "\r\n\r\n";
	}
	
	if (msg == "") {
		msg = "No candidates have registered for this event";
	}
	
	$('#status-text').val(msg);
	$('#status-text').css("color",'green');
}

function addOption(listID, value, selected) {
	  var list = document.getElementById(listID),
	  opt = document.createElement("option");
	  opt.value = value;
	  opt.innerHTML = value;
	  opt.selected = selected;
	  list.appendChild(opt);
}

function eventSelected() {
	var list = document.getElementById("event-list");
	var selectedEventName = list.options[list.selectedIndex].value;
	
	var mapFeatureOfSelectedItem = vectorSource.getFeatureById(selectedEventName);
	if (mapFeatureOfSelectedItem !== null) {
		selectInteraction.getFeatures().clear();
		selectInteraction.getFeatures().push(mapFeatureOfSelectedItem);
		map.getView().setCenter(mapFeatureOfSelectedItem.getGeometry().getCoordinates());
	}
}