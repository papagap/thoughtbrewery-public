// map-related "global" variables
var map;

// status-related "global" variables
var loadingMsg = "Loading...";


$(window).on('load', function() {
	if (Login.isUserLoggedIn()) {
		Login.displayIn();
	}
	startMap();
});

function startMap() {
	var cw = $('#eventMap').width();
	$('#eventMap').css({'height':3*cw/4+'px'});
    
	var vectorSource = new ol.source.Vector({});
	
	var tileLayer = new ol.layer.Tile({
		source: new ol.source.OSM()
	});
	
	var vectorLayer = new ol.layer.Vector({
		source: vectorSource
	});
	
    map = new ol.Map({
    	layers: [tileLayer, vectorLayer],
        target:'eventMap',                                                                  
        renderer:'canvas',
        moveTolerance: 5,
        view: new ol.View({
        	projection: 'EPSG:3857',
        })                                                                         
    });
	                                 
	center = new ol.proj.transform([8.709543, 49.414224],                                  
	                               'EPSG:4326', // essentially LonLat                      
	                               map.getView().getProjection());
	map.getView().setCenter(center);                                                  
	map.getView().setZoom(5);
	
	var clickedLon = 8.709543;
	var clickedLat = 49.414224;
	$('#dp-lon').val(clickedLon);
	$('#dp-lat').val(clickedLat);
	
	var clickedCoord = [clickedLon, clickedLat];
	var clickFeature = createClickFeatureForCoordinates(clickedCoord);
	vectorSource.addFeature(clickFeature);
	
	map.on('singleclick', function (event) {
		
		// convert coordinate to EPSG-4326
		var coord = ol.proj.transform(event.coordinate, 'EPSG:3857', 'EPSG:4326');
		
		var clickFeature = createClickFeatureForCoordinates(coord);
		vectorSource.clear();
		vectorSource.addFeature(clickFeature);
		
		clickedLon = coord[0];
    	clickedLat = coord[1];
    	$('#dp-lon').val(clickedLon);
    	$('#dp-lat').val(clickedLat);
		
    	console.log("lon="+clickedLon+" lat="+clickedLat);
	});
}

function createClickFeatureForCoordinates(coord) {
	var clickFeature = new ol.Feature({
		geometry: new ol.geom.Point(ol.proj.transform([coord[0], coord[1]], 'EPSG:4326', 'EPSG:3857'))
	});

	var iconStyle = new ol.style.Style({
		image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
			anchor: [0.5, 10],
		    anchorXUnits: 'fraction',
		    anchorYUnits: 'pixels',
		    opacity: 0.75,
		    src: 'assets/css/fancy-images/food.png'
		}))
	});

	clickFeature.setStyle(iconStyle);
	
	return clickFeature;
}

function submitEvent(form) {
	
	console.log("Submit Event for e-mail: ");
	console.log(form.elements["email"].value);
	
	$('#status-text').val(loadingMsg);
	$('#status-text').css("color",'black');
	
	Util.post("registerEvent", $(form).serialize(), function(response){
		console.log("RECEIVED: " + response);
		var msg = "Your event has been successfully registered!";
		$('#status-text').val(msg);
		$('#status-text').css("color",'green');
	});
	
	return false;
}

Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
});
