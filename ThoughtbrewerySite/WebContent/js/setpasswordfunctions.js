// password-related global variables
var minPassLength = 8;
var maxPassLength = 40;

// status-related "global" variables
var loadingMsg = "Loading...";


$(window).on('load', function() {
	if (Login.isUserLoggedIn()) {
		Login.displayIn();
	}
});

function setPassphrase(form) {
	
	if (form.elements["pass"].value.length < minPassLength) {
		var passShortMsg = "Your password needs to be at least " + minPassLength + " characters long!"
		$('#status-text').val(passShortMsg);
		$('#status-text').css("color",'red');
		return false;
	} else if (form.elements["pass"].value.length > maxPassLength) {
		var passLongMsg = "Your password cannot be longer than " + maxPassLength + " characters!"
		$('#status-text').val(passLongMsg);
		$('#status-text').css("color",'red');
		return false;
	} else if (form.elements["pass"].value !== form.elements["pass2"].value) {
		var passMissMsg = "The passwords you provided do not match!"
		$('#status-text').val(passMissMsg);
		$('#status-text').css("color",'red');
		return false;
	}
	
	console.log("Setting passphrase for e-mail: " + form.elements["email"].value);
	
	$('#status-text').val(loadingMsg);
	$('#status-text').css("color",'black');
	
	Util.post("setPassphrase", $(form).serialize(), function(response){
		console.log("RECEIVED: " + response);
		var msg = "Your password has been successfully changed!";
		$('#status-text').val(msg);
		$('#status-text').css("color",'green');
	});
}