var Util = {
		
	post: function requestPost(uri, postData, callback) {
		
		var hostURI = "https://thoughtbrewery.net/eds/";
		//var hostURI = "https://localhost:8443/eds/";
		//var hostURI = "http://localhost:8185/";

		console.log("REQUESTED (POST): " + hostURI + uri);
		
		$.ajax({
		  	  type: 'POST',
		  	  url: hostURI + uri,
		  	  data: postData,
		  	  contentType: 'application/x-www-form-urlencoded', //default, anyway (This is for the content of the REQUEST!)
		  	  dataType: 'text', // (This is for the content of the RESPONSE! For example, It could be 'json', so that non-json responses would throw error)
		  	  success: callback,
			  error: function(jqXHR, textStatus, errorThrown) {
				  console.log("REST (POST) call failed with message: " + jqXHR.responseText);
				  if (jqXHR.status == 500 || jqXHR.status == 401 || jqXHR.status == 403 || jqXHR.status == 400) { // TODO: Check all possible errors are captured
						var msg = "The request FAILED with the following message:\r\n" + jqXHR.responseText;
						$('#status-text').val(msg);
						$('#status-text').css("color",'red');
				  } else {
						var msg = "Hmmm, there seems to be a CONNECTION PROBLEM between this machine and our server.\r\nPlease try again later or report the error to the website owner.";
						$('#status-text').val(msg);
						$('#status-text').css("color",'red');
				  }
			  }
		  	});
		
	},

	get: function requestGet(uri, callback) {
		
		var hostURI = "https://thoughtbrewery.net/eds/";
		//var hostURI = "https://localhost:8443/eds/";
		//var hostURI = "http://localhost:8185/";

		console.log("REQUESTED (GET): " + hostURI + uri);
		
		$.ajax({
		  	  type: 'GET',
		  	  url: hostURI + uri,
		  	  contentType: 'application/x-www-form-urlencoded', //default, anyway (This is for the content of the REQUEST!)
		  	  dataType: 'text', // (This is for the content of the RESPONSE! For example, It could be 'json', so that non-json responses would throw error)
		  	  success: callback,
			  error: function(jqXHR, textStatus, errorThrown) {
				  console.log("REST (GET) call failed with message: " + jqXHR.responseText);
				  if (jqXHR.status == 500 || jqXHR.status == 401 || jqXHR.status == 403 || jqXHR.status == 400) { // TODO: Check all possible errors are captured
						var msg = "The request FAILED with the following message:\r\n" + jqXHR.responseText;
						$('#status-text').val(msg);
						$('#status-text').css("color",'red');
				  } else {
						var msg = "Hmmm, there seems to be a CONNECTION PROBLEM between this machine and our server.\r\nPlease try again later or report the error to the website owner.";
						$('#status-text').val(msg);
						$('#status-text').css("color",'red');
				  }
			  }
		  	});
		
	}
	
};