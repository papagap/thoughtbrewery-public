// status-related "global" variables
var loadingMsg = "Loading...";


$(window).on('load', function() {
	if (Login.isUserLoggedIn()) {
		Login.displayIn();
	}
});

function submitUserInfo(form) {
	
	if (document.getElementById("agreement-box").checked === false) {
		var noAgrMsg = "You have to tick the box to agree with the terms and policies before submitting anything!"
		$('#status-text').val(noAgrMsg);
		$('#status-text').css("color",'red');
		return false;
	}
	
	console.log("Submit user info for e-mail: ");
	console.log(form.elements["email"].value);
	
	$('#status-text').val(loadingMsg);
	$('#status-text').css("color",'black');
	
	Util.post("registerUser", $(form).serialize(), function(response){ // TODO: update messages
		console.log("RECEIVED: " + response);
		var msg = "An e-mail with bla blah has been sent to " + form.elements["email"].value;
		$('#status-text').val(msg);
		$('#status-text').css("color",'green');
	});
	
	// TODO: Check: If the server needs to treat differently SUCCESS codes other than 200, they can be handled by adding the two arguments statusText and jqXHR to the callback function and doing something like the following:
	/*if (jqXHR.status == 200) {
		var msg = "An e-mail with blah blah has been sent to " + email;
		$('#status-text').val(msg);
		$('#status-text').css("color",'green');
	} else if (jqXHR.status == 201) {
		var msg = "An e-mail with blah blah has been sent to " + email;
		$('#status-text').val(msg);
		$('#status-text').css("color",'green');
	} else {
		var msg = "Unexpected response status: " + jqXHR.status + ".\r\nPlease try again later or report the error to the website owner.";
		$('#status-text').val(msg);
		$('#status-text').css("color",'red');
	}*/
	
	return false;
}