var Login = {
	
		displayIn: function displayLoggedIn() {
			document.getElementById("registerbutton").style.display = "none";
			document.getElementById("loginbutton").style.display = "none";
			document.getElementById("greeting").innerHTML = '<a href="#">Hi '+ Cookie.read("alias") +'</a>';
			document.getElementById("greeting").style.display = "inline-block";
			document.getElementById("logoutbutton").style.display = "inline-block";
			document.getElementById("navPanel").innerHTML = '<nav>' + $('#nav').navList() + '</nav>';
		},
		
		displayOut: function displayLoggedOut() {
			document.getElementById("registerbutton").style.display = "inline-block";
			document.getElementById("loginbutton").style.display = "inline-block";
			document.getElementById("greeting").style.display = "none";
			document.getElementById("logoutbutton").style.display = "none";
			document.getElementById("navPanel").innerHTML = '<nav>' + $('#nav').navList() + '</nav>';
		},
		
		isUserLoggedIn: function isUserLoggedIn() {
			if (Cookie.read("session") !== null && Cookie.read("email") !== null && Cookie.read("alias") !== null) {
				return true;
			} else {
				return false;
			}
		},
		
		logout: function logout() {
			Cookie.erase("session");
			Cookie.erase("alias");
			Cookie.erase("email");
			Login.displayOut();
			$('#status-text').val("You have been successfully logged out!");
			$('#status-text').css("color",'green');
		}
}