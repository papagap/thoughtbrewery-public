// password-related global variables
var minPassLength = 8;
var maxPassLength = 40;

// status-related "global" variables
var loadingMsg = "Loading...";


$(window).on('load', function() {
	if (Login.isUserLoggedIn()) {
		Login.displayIn();
	}
});

function login(form) {
	
	if (form.elements["pass"].value.length < minPassLength) {
		var passShortMsg = "Your password needs to be at least " + minPassLength + " characters long!"
		$('#status-text').val(passShortMsg);
		$('#status-text').css("color",'red');
		return false;
	} else if (form.elements["pass"].value.length > maxPassLength) {
		var passLongMsg = "Your password cannot be longer than " + maxPassLength + " characters!"
		$('#status-text').val(passLongMsg);
		$('#status-text').css("color",'red');
		return false;
	}
	
	console.log("Logging in with e-mail: " + form.elements["email"].value);
	
	$('#status-text').val(loadingMsg);
	$('#status-text').css("color",'black');
	
	Util.post("login", $(form).serialize(), function(response){
		
		var session_id = response.substring(0, response.indexOf(';'));
		var alias = response.substring(response.indexOf(';')+1);
		var email = form.elements["email"].value;
		Cookie.create("session", session_id);
		Cookie.create("alias", alias);
		Cookie.create("email", email);
		Login.displayIn();
		
		var msg = "You are successfully logged in! You can now create and join events.";
		$('#status-text').val(msg);
		$('#status-text').css("color",'green');
		 
	});
}