// datastream-related global variables
var lastEntryId = [-1, -1, -1]; // To support up to three dynamically updated graphs

// status-related "global" variables
var loadingMsg = "Loading...";


function submitPreferredProxy(form) {
	
	console.log("Submit preferred proxy: ");
	console.log(form.elements["proxy"].value);
	
	$('#status-text').val(loadingMsg);
	$('#status-text').css("color",'black');
	
	Util.post("setPreferredProxy", $(form).serialize(), function(response){ // TODO: update?
		console.log("RECEIVED: " + response);
		var msg = response;
		$('#status-text').val(msg);
		$('#status-text').css("color",'green');
	});
	
	return false;
}

function drawGraph(updInt, cont, graphtitle) {
    $(document).ready(function() {
        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });
    
        var dbTableShownInChart = " ";
        var graphId = -1;
        if (cont == 'graphContainer') {
        	dbTableShownInChart = "tsentry";
        	garphId = 1;
        }
        
        var chart;
        
        $('#'+cont+'').highcharts({
            chart: {
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function() {
                        // set up the updating of the chart each X seconds
                        var series = this.series[0];
                        /*setInterval(function() {
                            var x = (new Date()).getTime(), // current time
                                y = Math.random();
                            series.addPoint([x, y], true, true);
                        }, updInt);*/
                        setInterval(function(){updateIdAndValueOfLastEntry(dbTableShownInChart, graphId, series);}, updInt);
                    }
                }
            },
            title: {
                //text: 'Live random data every ' + updInt/1000 + ' seconds'
                text: graphtitle
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: 'Value'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) +'<br/>'+
                        Highcharts.numberFormat(this.y, 2);
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                name: 'Initialization data',
                data: (function() {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;
    
                    for (i = -19; i <= 0; i++) {
                        data.push({
                            x: time + i * updInt,
                            y: 0
                            //y: Math.random()
                        });
                    }
                    return data;
                })()
            }]
        });
    });
    
}

function updateIdAndValueOfLastEntry(table, graphId, series) {
	Util.get("getLastId?table=" + table, function(response) {
		console.log("Server says: " + response);
		var receivedId = Number(response);
		if (receivedId != lastEntryId[graphId]) {
			Util.get("getValueById?table=" + table + "&id=" + receivedId, function(response){
				console.log("Server says: " + response);
				var lastEntryValue = Number(response);
				lastEntryId[graphId] = receivedId;
				var x = (new Date()).getTime();
				series.addPoint([x, lastEntryValue], true, true);
			});
		}
	});
}