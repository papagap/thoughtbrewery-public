#-------------------------------------------------------------------------------
# Copyright 2019 Apostolos Papageorgiou
# 
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License.  You may obtain a copy
# of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
# License for the specific language governing permissions and limitations under
# the License.
#-------------------------------------------------------------------------------
# EventplanningDemoServer

This is a server application (restlet) that offers the API required by a client in order to implement an event planning application. The purpose of the project within the thoughtbrewery domain is to be used by the javascript pages of the thoughtbrewery site.

## API / Usage

There is currently no published API documentation for the RPC-style http interface exposed by this application. However, information about the API usage can be gained by looking into the list of tests in the curl-tests.sh script under test/apostolos/eventplanning/rest/resources/test. For the implementation itself, there is full javadoc documentation in the doc folder.

Visit thoughtbrewery.net in order to try out the demo application that uses this server.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

Edit the java properties (src/apostolos/eventplanning/ep.properties file) to reflect your system settings and preferences, and execute "mvn clean package" in the root folder of this project in order to create a war file, which can be then deployed on a web container. To-date, the project has been tested with tomcat 8 and 9.

## Authors

* **Apostolos Papageorgiou** - [thoughtbrewery](https://thoughtbrewery.net)

## License

This project is licensed under the Apache 2.0 license. Please see the LICENSE file for more details.
