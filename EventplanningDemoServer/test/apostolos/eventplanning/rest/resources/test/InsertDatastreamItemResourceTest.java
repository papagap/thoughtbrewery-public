/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.resources.test;

import java.io.IOException;
import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.restlet.data.Form;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

import apostolos.eventplanning.Configuration;
import apostolos.eventplanning.Util;

public class InsertDatastreamItemResourceTest {
	
	@Before
	public void init() {
		Util.startStandaloneServerWithRestletAttached(8185);
	}

	@Test
	public void testProcessGet() {
		Random rand = new Random();
		int numOfInvocations = 20;
		//Client client = new Client(Protocol.HTTP); // Using alternative class (lower-level) to ClientResource
		ClientResource client = new ClientResource("http://localhost:8185/insertDatastreamItem");
		//Request request = null; // Needed if using Client instead of ClientResource
		//Response response = null; // Needed if using Client instead of ClientResource
		String val = null;
		String response = "";
		
		for (int i=0; i<numOfInvocations; i++) {
			val = String.valueOf(rand.nextFloat());
			
			Form f = new Form();
			f.add("key", Configuration.DATASTREAM_API_KEY);
			f.add("datastream", "tsentry");
			f.add("name", "noise-level");
			f.add("value", val);
			
			//request = new Request(Method.GET, "http://localhost:8185/insertDatastreamItem?table=tsentry&name=junitinsertion&value=" + val); // Needed if using Client instead of ClientResource
			//response = client.handle(request); // Needed if using Client instead of ClientResource
			try {
				response = client.post(f).getText(); // Representation.getText() empties the InputStream, so we need to store the text in a variable
			} catch (ResourceException re) {
				re.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
			
			Assert.assertTrue(client.getStatus().getCode() == 200);
			Assert.assertEquals(response, "Success!");
		}
	}

}
