/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.resources.test;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.restlet.Client;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Method;
import org.restlet.data.Protocol;

import apostolos.eventplanning.Util;

public class LastIdResourceTest {
	
	@Before
	public void init() {
		Util.startStandaloneServerWithRestletAttached(8185);
	}

	@Test
	public void testProcessGet() {
		Client client = new Client(Protocol.HTTP);
		int numOfInvocations = 5;
		Request request = null;
		Response response = null;
		String res = "";
		
		for (int i=0; i<numOfInvocations; i++) {
			request = new Request(Method.GET, "http://localhost:8185/getLastId?table=tsentry");
			response = client.handle(request);
	
			Assert.assertTrue(response.getStatus().getCode() == 200);
			Assert.assertTrue(response.isEntityAvailable());
	
			// Representation.getText() empties the InputStream, so we need to store the text in a variable
			try {
				res = response.getEntity().getText();
			} catch (IOException ioe) {
				System.out.println(ioe.getMessage());
			}
			Assert.assertTrue(isInteger(res));
			
			request = new Request(Method.GET, "http://localhost:8185/getLastId?table=nonexistingtable");
			response = client.handle(request);
	
			Assert.assertTrue(response.getStatus().getCode() == 422);
			Assert.assertTrue(response.isEntityAvailable());
	
			// Representation.getText() empties the InputStream, so we need to store the text in a variable
			try {
				res = response.getEntity().getText();
			} catch (IOException ioe) {
				System.out.println(ioe.getMessage());
			}
			Assert.assertTrue(!Util.isInteger(res));
		}
	}
	
	private static boolean isInteger(String s) {
	    return isInteger(s,10);
	}

	private static boolean isInteger(String s, int radix) {
	    if(s.isEmpty()) return false;
	    for(int i = 0; i < s.length(); i++) {
	        if(i == 0 && s.charAt(i) == '-') {
	            if(s.length() == 1) return false;
	            else continue;
	        }
	        if(Character.digit(s.charAt(i),radix) < 0) return false;
	    }
	    return true;
	}

}
