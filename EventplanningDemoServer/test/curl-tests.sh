eds_url=http://thoughtbrewery.net/eds/

testHttp() {
	verb=$1    
	url=$2
	endpoint=$3
	data=$4
	exp_code=$5
	exp_res=$6
	
	if [ $verb == "post" ]; then
	  curl_command='curl -w "^%{http_code}" -s --data "$data" $url$endpoint'
	  cc="curl -w \"^%{http_code}\" -s --data \"$data\" $url$endpoint"
	elif [ $verb == "get" ]; then
	  curl_command='curl -w "^%{http_code}" -s $url$endpoint?$data'
	  cc="curl -w \"^%%{http_code}\" -s $url$endpoint?$data"
	else
	  echo FAIL: unknown http verb: $verb
	  return -1
	fi
	
	resWithStatus=$(eval $curl_command)
	exit_code=$?
	IFS='^' read -r -a resArray <<< "$resWithStatus"
	response="${resArray[0]}"
	status="${resArray[1]}"
	
	echo "  executed curl command = ${cc}"
	printf "  curl exit code = ${exit_code}\n"
	printf "  http status = ${status}\n"
	printf "  http response = ${response}\n"
	
	if [ 0 -ne $exit_code ]; then
	  echo FAIL: curl returned failure code $exit_code
	  return -1
	fi
	
	if [[ "${exp_res}" == "" && "${exp_code}" == "" ]]; then
	  echo SUCCESS: call completed and there were no expectations for the response and the code
	elif [[ "${response}" == "${exp_res}" && "${status}" == "${exp_code}" ]]; then
	  echo SUCCESS: response and code were as expected
	elif [[ "${exp_res}" == "" && "${status}" == "${exp_code}" ]]; then
	  echo SUCCESS: code was as expected and there were no expectations for the response
	elif [[ "${response}" == "${exp_res}" && "${exp_code}" == "" ]]; then
	  echo SUCCESS: response was as expected and there were no expectations for the code
	elif [[ "${exp_res}" == "" && "${status}" != "${exp_code}" ]]; then
	  echo FAIL: code was \"$status\" but expected \"${exp_code}\"
	elif [[ "${response}" != "${exp_res}" && "${exp_code}" == "" ]]; then
	  echo FAIL: response was \"$response\" but expected \"${exp_res}\"
	elif [[ "${response}" == "${exp_res}" && "${status}" != "${exp_code}" ]]; then
	  echo FAIL: code was \"$status\" but expected \"${exp_code}\"
	elif [[ "${response}" != "${exp_res}" && "${status}" == "${exp_code}" ]]; then
	  echo FAIL: response was \"$response\" but expected \"${exp_res}\"
	else
	  echo FAIL: response was \"$response\" \(expected \"${exp_res}\"\) and code was \"$status\" \(expected \"${exp_code}\"\)
	fi
}


#while true

#do

# The following user registration and password-setting tests work only in test versions of the server, where the token is given in the registerUser response.
# This is, of course, not the case in the deployment version, which sends the token only via e-mail.
# Therefore, if the test users are not in the database already, please run the insert_test_users.sql script in FermentationDatabase before running the rest of the tests here.

#printf "Registering user without alias\n"
#testHttp post $eds_url registerUser "email=apostolos.ch.papageorgiou%2B1@gmail.com&name=Apostolos&surname=Papageorgiou&telephone=644&address=APA 36&sex=male&age=99" 400 "Parameter alias is missing or empty."

#printf "\nRegistering user with empty email\n"
#testHttp post $eds_url registerUser "alias=user1&email=&name=Apostolos&surname=Papageorgiou&telephone=644&address=APA 36&sex=male&age=99" 400 "Parameter email is missing or empty."

#printf "\nRegistering user1 correctly\n"
#testHttp post $eds_url registerUser "alias=user1&email=apostolos.ch.papageorgiou%2B1@gmail.com&name=Apostolos&surname=Papageorgiou&telephone=644&address=APA 36&sex=male&age=99" 201 ""
#token=$response

#printf "\nRegistering user with existing unconfirmed alias and new e-mail\n"
#testHttp post $eds_url registerUser "alias=user1&email=whoever@gmail.com" 400 "This alias already exists. Please use a different one."

#printf "\nSetting pass of user1 with wrong token\n"
#testHttp post $eds_url setPassphrase "email=apostolos.ch.papageorgiou%2B1@gmail.com&token=1234&pass=pass1111" 401 "Token mismatch"

#printf "\nSetting pass of user that does not exist\n"
#testHttp post $eds_url setPassphrase "email=whoever%2B1@gmail.com&token=${token}&pass=pass1111" 401 "Token mismatch"

#printf "\nRegistering user with existing confirmed alias and new e-mail\n"
#testHttp post $eds_url registerUser "alias=user1&email=whoever@gmail.com" 400 "This alias already exists. Please use a different one."

#printf "\nSetting pass of user1\n"
#testHttp post $eds_url setPassphrase "email=apostolos.ch.papageorgiou%2B1@gmail.com&token=${token}&pass=pass1111" 200 Success!

#printf "\nRegistering user2 correctly but with missing optional inputs\n"
#testHttp post $eds_url registerUser "alias=user2&email=apostolos.ch.papageorgiou%2B2@gmail.com&name=Onoma&address=Pfg 9&sex=whatever" 201 ""
#token=$response
#printf "\nSetting pass of user2\n"
#testHttp post $eds_url setPassphrase "email=apostolos.ch.papageorgiou%2B2@gmail.com&token=${token}&pass=pass2222" 200 Success!

printf "\nRegistering event with correct inputs\n"
testHttp post $eds_url registerEvent "email=apostolos.ch.papageorgiou%2B1@gmail.com&pass=pass1111&eventName=myEvent1&info=A cool event&place=home&address=APA 36&lat=37.383511&lon=23.257138&type=invitation&date=2018-12-31&time=00:00" 200 Success!

printf "\nRe-registering the same event without having deleted it\n"
testHttp post $eds_url registerEvent "email=apostolos.ch.papageorgiou%2B1@gmail.com&pass=pass1111&eventName=myEvent1&info=A cool event&place=home&address=APA 36&lat=33&lon=8&type=invitation&date=2018-12-31&time=00:00" 400 "An event with this name already exists"

printf "\nDeleting the event of someone else\n"
testHttp post $eds_url deleteEvent "email=apostolos.ch.papageorgiou%2B2@gmail.com&pass=pass2222&eventName=myEvent1" 403 "Not the event owner"

printf "\nDeleting the event with wrong password\n"
testHttp post $eds_url deleteEvent "email=apostolos.ch.papageorgiou%2B1@gmail.com&pass=pass2222&eventName=myEvent1" 401 "Passphrase mismatch"

printf "\nDeleting the event correctly\n"
testHttp post $eds_url deleteEvent "email=apostolos.ch.papageorgiou%2B1@gmail.com&pass=pass1111&eventName=myEvent1" 200 Success!

printf "\nRe-registering the same event after having deleted it\n"
testHttp post $eds_url registerEvent "email=apostolos.ch.papageorgiou%2B1@gmail.com&pass=pass1111&eventName=myEvent1&info=A cool event&place=home&address=mpisti 2&lat=37.383511&lon=23.257138&type=invitation&date=2018-12-31&time=00:00" 200 Success!

printf "\nRegistering event with correct inputs but strange name\n"
testHttp post $eds_url registerEvent "email=apostolos.ch.papageorgiou%2B1@gmail.com&pass=pass1111&eventName=event  . �ko&info=A cool event with strange s@mbol$``end&place=wonderland&address=Street 1&lat=42.383511&lon=9.257138&type=invitation&date=2018-12-31&time=00:00" 200 Success!

#printf "\nRegistering event with correct inputs but strange name 2\n"
#testHttp post $eds_url registerEvent "email=apostolos.ch.papageorgiou%2B1@gmail.com&pass=pass1111&eventName=event . �ko&info=A cool event with strange s@mbol$``end&place=wonderland&address=Street 1&lat=42.383511&lon=11.257138&type=invitation&date=2018-12-31&time=00:00" 200 Success!

printf "\nRegistering event with correct inputs but strange name 3\n"
testHttp post $eds_url registerEvent "email=apostolos.ch.papageorgiou%2B1@gmail.com&pass=pass1111&eventName=event  . ko&info=A cool event with strange s@mbol$``end&place=wonderland&address=Street 1&lat=42.383511&lon=13.257138&type=invitation&date=2018-12-31&time=00:00" 200 Success!

#printf "\nRegistering event with correct inputs but strange name 3\n"
#testHttp post $eds_url registerEvent "email=apostolos.ch.papageorgiou%2B1@gmail.com&pass=pass1111&eventName=event�ko&info=A cool event with strange s@mbol$``end&place=wonderland&address=Street 1&lat=42.383511&lon=15.257138&type=invitation&date=2018-12-31&time=00:00" 200 Success!

printf "\nRegistering another event with wrong password\n"
testHttp post $eds_url registerEvent "email=apostolos.ch.papageorgiou%2B1@gmail.com&pass=pass2222&eventName=myEvent2&info=Another cool event&place=outside&address=Pl.Sol&lat=33.2&lon=8.2&type=foodsharing&date=2018-12-31&time=03:15" 401 "Passphrase mismatch"

printf "\nRegistering another event with correct inputs\n"
testHttp post $eds_url registerEvent "email=apostolos.ch.papageorgiou%2B1@gmail.com&pass=pass1111&eventName=myEvent2&info=Another cool event&place=outside&address=Schuknecht&lat=49.883567&lon=8.655279&type=foodsharing&date=2018-12-31&time=03:15" 200 Success!

printf "\nGetting list of events\n"
testHttp get $eds_url listEvents "" 200 ""

printf "\nRequesting participation to non-existing event\n"
testHttp post $eds_url requestParticipation "email=apostolos.ch.papageorgiou%2B2@gmail.com&pass=pass2222&eventName=noEvent&message=I would be joining" 400 "No event with this name was found."

printf "\nRequesting participation to an event\n"
testHttp post $eds_url requestParticipation "email=apostolos.ch.papageorgiou%2B2@gmail.com&pass=pass2222&eventName=myEvent1&message=I am joining" 200 Success!

printf "\nListing requests for an event of someone else\n"
testHttp post $eds_url listEventRequests "email=apostolos.ch.papageorgiou%2B2@gmail.com&pass=pass2222&eventName=myEvent1" 403 "Not the event owner"

printf "\nListing requests for an event with wrong pass\n"
testHttp post $eds_url listEventRequests "email=apostolos.ch.papageorgiou%2B1@gmail.com&pass=pass2222&eventName=myEvent1" 401 "Passphrase mismatch"

printf "\nListing requests for an event\n"
testHttp post $eds_url listEventRequests "email=apostolos.ch.papageorgiou%2B1@gmail.com&pass=pass1111&eventName=myEvent1" 200 ""

printf "\nListing requests for another event\n"
testHttp post $eds_url listEventRequests "email=apostolos.ch.papageorgiou%2B1@gmail.com&pass=pass1111&eventName=myEvent2" 200 ""

printf "\nInserting data stream items with wrong API key\n"
testHttp post $eds_url insertDatastreamItem "key=wrong-key&datastream=tsentry&name=whateverItemName&value=1.1" 401 "Wrong API key!"

printf "\nInserting data stream items without name\n"
testHttp post $eds_url insertDatastreamItem "key=wrong-key&datastream=tsentry&value=1.1" 400 "Parameter name is missing or empty."

printf "\nInserting data stream item correctly\n"
testHttp post $eds_url insertDatastreamItem "key=test-api-key&datastream=tsentry&name=whateverItemName&value=1.1" 200 "Success!"

printf "\nInserting another data stream item correctly with the same name\n"
testHttp post $eds_url insertDatastreamItem "key=test-api-key&datastream=tsentry&name=whateverItemName&value=2.2" 200 "Success!"

printf "\nInserting a third data stream item correctly with another name\n"
testHttp post $eds_url insertDatastreamItem "key=test-api-key&datastream=tsentry&name=myItem&value=3.3" 200 "Success!"

printf "\nRetrieving last id of table tsentry\n"
testHttp get $eds_url getLastId "table=tsentry" 200 ""
lastid=$response

printf "\nChecking the value of item with wrong id\n"
testHttp get $eds_url getValueById "table=tsentry&id=5a5a" 204 ""

printf "\nChecking the value of item with correct id\n"
testHttp get $eds_url getValueById "table=tsentry&id=${lastid}" 200 "3.3"

#sleep 2

#done