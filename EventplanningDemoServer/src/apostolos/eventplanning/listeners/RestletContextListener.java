/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.listeners;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RestletContextListener implements ServletContextListener {
	
	protected static Logger log = LoggerFactory.getLogger("EventPlanning");
	
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        log.info("Starting up the Eventplanning server servlet!");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        log.info("Shutting down the Eventplanning server servlet!");
        
        // If any background tasks are using a database, they have to be closed here, together with any database connection pools.
        // The following code deregisters JDBC drivers registered in the ClassLoader of the context of this servlet.
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            if (driver.getClass().getClassLoader() == cl) {
                try {
                    log.info("Deregistering JDBC driver " + driver);
                    DriverManager.deregisterDriver(driver);
                } catch (SQLException sqle) {
                    log.error("Error deregistering JDBC driver " + driver  + ": " + sqle.getMessage());
                }
            } else {
                log.debug("JDBC driver " + driver + " was found but will not be deregistered because it was not registered in the context of this servlet.");
            }
        }
    }

}
