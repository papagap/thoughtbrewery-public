/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.resources;

import java.io.File;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;

import apostolos.eventplanning.Configuration;
import apostolos.eventplanning.Util;
import apostolos.eventplanning.rest.messages.RestRequest;
import apostolos.eventplanning.rest.messages.RestResponse;

/**
 * SetPreferredProxyResource is a Restlet server resource that is used to indicate which type of data management proxy should be used for storing (or not) data stream items into the database.
 * 
 * @author Apostolos Papageorgiou
 */
public class SetPreferredProxyResource extends BaseResource {

	private String pass;
	private String proxyType;

	/**
	 * This is the entry point of a (POST) request to this resource (see annotation).
	 * It leads through the parsing and the handling of the request, as well as the preparation of the result, which will be sent as the POST response.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return the representation of the outgoing POST response
	 */
	@Post
	public Representation accept(Representation entity) {
		
		configureCorsHeaders(getRequest(), getResponse());
		
		RestRequest request = createRestRequestForIncomingMessage();
		request.logRestMessage();
		
		Representation result = null;
		
		if (parseSetPreferredProxyPostRequest(entity)) {
			handleSetPreferredProxyPostRequest();
		} else {
			; // If the parsing of the request fails then no further processing is required, while the status and the response message have already been appropriately set.
		}
		
		RestResponse response = createResponse(request);
		response.logRestMessage();
		
		result = new StringRepresentation(responseString, MediaType.TEXT_PLAIN);

		return result;
	}

	/**
	 * Parses the incoming message to check if all required fields are present, preparing appropriate error responses otherwise.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return true if all required fields are present and correctly parsed, false otherwise.
	 */
	private boolean parseSetPreferredProxyPostRequest(Representation entity) {
		if (entity != null) {
			log.debug("Processing POST request with content type: " + entity.getMediaType().toString());
			Form form = new Form(entity);
	        
			pass = form.getFirstValue("pass");
			if (pass == null || pass.equals("")) {
				responseString = "Parameter pass is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
			proxyType = form.getFirstValue("proxy");
			if (proxyType == null || proxyType.equals("")) {
				responseString = "Parameter proxy is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
	        
	        return true;
		} else {
			log.warn("Processing POST request with NULL content");
			responseString = "The server received a request without data.";
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return false;
		}
	}
	
	/**
	 * Checks the passphrase and triggers the change of the registered preferred proxy type according to the provided input.
	 */
	private void handleSetPreferredProxyPostRequest() {
		String passcheckResult;
        passcheckResult = checkPassphrase(Configuration.DATASTREAM_ADMIN_USER, pass);
        
        if (passcheckResult.equals(Configuration.SUCCESS_MSG)) {
        	responseString = changePreferredProxyType(proxyType);
        	if (!responseString.equals(Configuration.SUCCESS_MSG)) {
        		setStatus(Status.SERVER_ERROR_INTERNAL);
	        }
        } else if (passcheckResult.equals(Configuration.AUTH_ERROR_MSG)) {
        	setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
        	responseString = "Passphrase mismatch. Maybe you are just not logged in?";
        } else {
        	responseString = passcheckResult;
        	setStatus(Status.SERVER_ERROR_INTERNAL);
        }
	}
	
	/**
	 * Changes the registered preferred proxy type according to the provided input.
	 * 
	 * @param preferredProxyType
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the preferred proxy type setting has been changed successfully, or an appropriate error message otherwise
	 */
	private String changePreferredProxyType(String preferredProxyType) {
		return Util.writeContentToFile(Configuration.REQUESTED_PROXY_DIR + File.separator + Configuration.REQUESTED_PROXY_FILENAME, preferredProxyType);
	}
}
