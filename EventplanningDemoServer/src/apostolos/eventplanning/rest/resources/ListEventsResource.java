/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.resources;

import java.util.ArrayList;

import org.restlet.resource.Get;

import com.google.gson.Gson;

import apostolos.eventplanning.datamanagement.DatasetDAO;
import apostolos.eventplanning.datamanagement.DatasetDBHandler;
import apostolos.eventplanning.datamanagement.Event;
import apostolos.eventplanning.rest.messages.RestRequest;
import apostolos.eventplanning.rest.messages.RestResponse;

/**
 * ListEventsResource is a Restlet server resource that is used to retrieve the list of all events from the event management system.
 * 
 * @author Apostolos Papageorgiou
 */
public class ListEventsResource extends BaseResource {
	
	private String filter;

	/**
	 * This is the entry point of a (GET) request to this resource (see annotation).
	 * It leads through the parsing and the handling of the request, as well as the preparation of the result, which will be sent as the GET response.
	 * 
	 * @param requestStr the incoming GET request string
	 * @return the GET response string
	 */
	@Get
	public String processGet(String requestStr) {
		
		configureCorsHeaders(getRequest(), getResponse());
		
		RestRequest request = createRestRequestForIncomingMessage();
		request.logRestMessage();
		
		if (parseListEventsGetRequest(request)) {
			handleListEventsGetRequest();
		} else {
			; // If the parsing of the request fails then no further processing is required, while the status and the response message have already been appropriately set.
		}
		
		RestResponse response = createResponse(request);
		response.logRestMessage();

		return responseString;
	}
	
	/**
	 * Parses the incoming message to check if all required fields are present, preparing appropriate error responses otherwise.
	 * 
	 * @param r the object representing the incoming GET request
	 * @return true if all required fields are present and correctly parsed, false otherwise.
	 */
	protected boolean parseListEventsGetRequest(RestRequest r) {
		
		filter = r.getParams().get("filter");
		
		if (filter == null) {
			filter = "none";
		}
		
		return true;
	}

	/**
	 * Wraps the query for retrieving the list of events.
	 */
	private void handleListEventsGetRequest() {

		responseString = getListOfEvents();
	}
	
	/**
	 * Triggers the query for retrieving the list of events by using a {@link apostolos.eventplanning.datamanagement.DatasetDAO} object.
	 * 
	 * @return the list of events as a String with a JSON format if such a valid list was successfully retrieved, or an appropriate error message otherwise
	 */
	private String getListOfEvents() {
		DatasetDAO dataHandler = new DatasetDBHandler();
		ArrayList<Event> eventsList = null;
		String res; 
		
		try {
			eventsList = dataHandler.getAllEvents();
			if (filter.equals("sessionemail")) { // Note that the filter cannot be null here
				ArrayList<Event> myEvents = new ArrayList<Event>();
				for (Event e : eventsList) {
					if (e.getEmail().equals(email)) { // Selecting only the events of the current user
						myEvents.add(e);
					}
				}
				eventsList = myEvents;
			} else {
				for (Event e : eventsList) {
					e.setEmail("_hidden_"); // Obscuring information about the event owner
				}
			}
			res = convertToJsonString(eventsList);
		} catch (IllegalArgumentException iae) {
			log.error(iae.getMessage());
			res = iae.getMessage();
		}
		
		return res;
	}
	
	/**
	 * Converts an ArrayList of events to a JSON String by using the Gson library.
	 * 
	 * @param eventsList the list of events to be converted to a JSON String 
	 * @return the list of events as a JSON String
	 */
	private String convertToJsonString(ArrayList<Event> eventsList) {
		Gson gson = new Gson();
		String response = gson.toJson(eventsList);
		return response;
	}

}
