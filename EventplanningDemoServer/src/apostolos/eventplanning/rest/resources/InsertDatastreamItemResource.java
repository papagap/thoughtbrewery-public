/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.resources;

import java.sql.Timestamp;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;

import apostolos.eventplanning.Configuration;
import apostolos.eventplanning.Util;
import apostolos.eventplanning.datamanagement.DatasetDAO;
import apostolos.eventplanning.datamanagement.DatasetDBHandler;
import apostolos.eventplanning.datamanagement.DatastreamItem;
import apostolos.eventplanning.rest.messages.RestRequest;
import apostolos.eventplanning.rest.messages.RestResponse;

/**
 * InsertDatastreamItemResource is a Restlet server resource that is used to insert a data stream item entry into the database.
 * 
 * @author Apostolos Papageorgiou
 */
public class InsertDatastreamItemResource extends BaseResource {

	private String datastreamName;
	private String name;
	private String value;
	private String timestamp;
	private String apiKey;

	/**
	 * This is the entry point of a (POST) request to this resource (see annotation).
	 * It leads through the parsing and the handling of the request, as well as the preparation of the result, which will be sent as the POST response.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return the representation of the outgoing POST response
	 */
	@Post
	public Representation accept(Representation entity) {
		
		configureCorsHeaders(getRequest(), getResponse());
		
		RestRequest request = createRestRequestForIncomingMessage();
		request.logRestMessage();
		
		Representation result = null;
		
		if (parseInsertDatastreamItemPostRequest(entity)) {
			handleInsertDatastreamItemPostRequest();
		} else {
			; // If the parsing of the request fails then no further processing is required, while the status and the response message have already been appropriately set.
		}
		
		RestResponse response = createResponse(request);
		response.logRestMessage();
		
		result = new StringRepresentation(responseString, MediaType.TEXT_PLAIN);

		return result;
	}

	/**
	 * Parses the incoming message to check if all required fields are present, preparing appropriate error responses otherwise.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return true if all required fields are present and correctly parsed, false otherwise.
	 */
	private boolean parseInsertDatastreamItemPostRequest(Representation entity) {
		if (entity != null) {
			log.debug("Processing POST request with content type: " + entity.getMediaType().toString());
			Form form = new Form(entity);
	        
			apiKey = form.getFirstValue("key");
			if (apiKey == null || apiKey.equals("")) {
				responseString = "Parameter key is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
			datastreamName = form.getFirstValue("datastream");
			if (datastreamName == null || datastreamName.equals("")) {
				responseString = "Parameter datastream is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
			name = form.getFirstValue("name");
			if (name == null || name.equals("")) {
				responseString = "Parameter name is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
	        value = form.getFirstValue("value");
	        if (value == null || value.equals("")) {
				responseString = "Parameter value is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
	        timestamp = form.getFirstValue("timestamp");
	        
	        return true;
		} else {
			log.warn("Processing POST request with NULL content");
			responseString = "The server received a request without data.";
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return false;
		}
	}
	
	/**
	 * Checks the API key and triggers the insertion of the data stream item if everything is fine, setting appropriate error status and messages otherwise.
	 */
	private void handleInsertDatastreamItemPostRequest() {
        
        if (apiKey.equals(Configuration.DATASTREAM_API_KEY)) {
        	responseString = insertDatastreamItem(name, value, timestamp, datastreamName);
        	if (!responseString.equals(Configuration.SUCCESS_MSG)) {
        		setStatus(Status.SERVER_ERROR_INTERNAL);
	        }
        } else {
        	setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
        	responseString = "Wrong API key!";
        }
	}
	
	/**
	 * Triggers the insertion of a data stream item with the specified name, value, and timestamp into the specified table
	 * of the database by using a {@link apostolos.eventplanning.datamanagement.DatasetDAO} object.
	 * 
	 * @param name the name of the data stream item to be inserted
	 * @param value the value of the data stream item to be inserted
	 * @param timestamp the timestamp of the data stream item to be inserted
	 * @param table the database table into which the item shall be inserted 
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the item was successfully inserted, or an appropriate error message otherwise
	 */
	private String insertDatastreamItem(String name, String value, String timestamp, String table) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		
		Timestamp time = null;
		if (timestamp == null) {
			time = Util.getCurrTimeAsTimestamp();
		} else {
			time = Timestamp.valueOf(timestamp);
		}
		
		DatastreamItem ds = new DatastreamItem(-1, name, value, time);
		
		return dataHandler.insertDatastreamItem(table, ds);
		
	}
	
}
