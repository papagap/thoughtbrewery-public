/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.resources;

import org.restlet.data.Status;
import org.restlet.resource.Get;

import apostolos.eventplanning.datamanagement.DatasetDAO;
import apostolos.eventplanning.datamanagement.DatasetDBHandler;
import apostolos.eventplanning.rest.messages.RestRequest;
import apostolos.eventplanning.rest.messages.RestResponse;

/**
 * ValueByIdResource is a Restlet server resource that is used to retrieve the value of an entry with a specified id from a specified database table.
 * Note that it works for all tables that have a column called "id" and another one called "value".
 * 
 * @author Apostolos Papageorgiou
 */
public class ValueByIdResource extends BaseResource {

	private String table;
	private String id;

	/**
	 * This is the entry point of a (GET) request to this resource (see annotation).
	 * It leads through the parsing and the handling of the request, as well as the preparation of the result, which will be sent as the GET response.
	 * 
	 * @param requestStr the incoming GET request string
	 * @return the GET response string
	 */
	@Get
	public String processGet(String requestStr) {
		
		configureCorsHeaders(getRequest(), getResponse());
		
		RestRequest request = createRestRequestForIncomingMessage();
		request.logRestMessage();
		
		if (parseValueByIdGetRequest(request)) {
			handleValueByIdGetRequest();
		} else {
			; // If the parsing of the request fails then no further processing is required, while the status and the response message have already been appropriately set.
		}
		
		RestResponse response = createResponse(request);
		response.logRestMessage();

		return responseString;
	}
	
	/**
	 * Parses the incoming message to check if all required fields are present, preparing appropriate error responses otherwise.
	 * 
	 * @param r the object representing the incoming GET request
	 * @return true if all required fields are present and correctly parsed, false otherwise.
	 */
	protected boolean parseValueByIdGetRequest(RestRequest r) {
		
		table = r.getParams().get("table");
		if (table == null || table.equals("")) {
			responseString = "Parameter table is missing or empty.";
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return false;
		}
		id = r.getParams().get("id");
		if (id == null || id.equals("")) {
			responseString = "Parameter id is missing or empty.";
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return false;
		}
		
		return true;
	}
	
	/**
	 * Wraps the value-by-id query
	 */
	private void handleValueByIdGetRequest() {

		responseString = makeValueByIdQuery(table, id);
	}

	/**
	 * Triggers the query for retrieving the value of the entry with the specified id from the specified table
	 * by using a {@link apostolos.eventplanning.datamanagement.DatasetDAO} object.
	 * 
	 * @param table the name of table in which the provided id and its value are being searched
	 * @param id the id for which the corresponding value shall be retrieved
	 * @return the searched value if the query was successful, or an appropriate error message otherwise
	 */
	private String makeValueByIdQuery(String table, String id) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		String res;
		
		try {
			res = dataHandler.getValueById(table, id);
		} catch (IllegalArgumentException iae) {
          	log.error(iae.getMessage());
          	res = iae.getMessage();
        }
		
		return res;
	}
	
}
