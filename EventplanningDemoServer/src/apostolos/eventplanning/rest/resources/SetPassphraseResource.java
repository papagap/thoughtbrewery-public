/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.resources;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;

import apostolos.eventplanning.Configuration;
import apostolos.eventplanning.Util;
import apostolos.eventplanning.datamanagement.Candidate;
import apostolos.eventplanning.datamanagement.DatasetDAO;
import apostolos.eventplanning.datamanagement.DatasetDBHandler;
import apostolos.eventplanning.rest.messages.RestRequest;
import apostolos.eventplanning.rest.messages.RestResponse;

/**
 * SetPassphraseResource is a Restlet server resource that is used to set the passphrase for a user.
 * 
 * @author Apostolos Papageorgiou
 */
public class SetPassphraseResource extends BaseResource {
	
	private String email;
	private String token;
	private String pass;

	/**
	 * This is the entry point of a (POST) request to this resource (see annotation).
	 * It leads through the parsing and the handling of the request, as well as the preparation of the result, which will be sent as the POST response.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return the representation of the outgoing POST response
	 */
	@Post
	public Representation accept(Representation entity) {
		
		configureCorsHeaders(getRequest(), getResponse());
		
		RestRequest request = createRestRequestForIncomingMessage();
		request.logRestMessage();
		
		Representation result = null;
		
		if (parseRequestPassphrasePostRequest(entity)) {
			handleRequestPassphrasePostRequest();
		} else {
			; // If the parsing of the request fails then no further processing is required, while the status and the response message have already been appropriately set.
		}
		
		RestResponse response = createResponse(request);
		response.logRestMessage();
		
		result = new StringRepresentation(responseString, MediaType.TEXT_PLAIN);

		return result;
	}
	
	/**
	 * Parses the incoming message to check if all required fields are present, preparing appropriate error responses otherwise.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return true if all required fields are present and correctly parsed, false otherwise.
	 */
	private boolean parseRequestPassphrasePostRequest(Representation entity) {
		if (entity != null) {
			log.debug("Processing POST request with content type: " + entity.getMediaType().toString());
			Form form = new Form(entity);
	        
			email = form.getFirstValue("email");
			if (email == null || email.equals("")) {
				responseString = "Parameter email is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
			
			token = form.getFirstValue("token");
			if (token == null || token.equals("")) {
				responseString = "Parameter token is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
			
			pass = form.getFirstValue("pass");
			if (pass == null || pass.equals("")) {
				responseString = "Parameter pass is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
	        
	        return true;
		} else {
			log.warn("Processing POST request with NULL content");
			responseString = "The server received a request without data.";
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return false;
		}
	}
	
	/**
	 * Triggers the generation of a hash based on the provided password
	 * and wraps the query for storing it into the database,
	 * setting appropriate error status and messages upon failure.
	 * Upon success it creates a permanent entry for the (confirmed) user
	 * and deletes the temporary entry of the (unconfirmed) user (i.e. candidate).
	 */
	private void handleRequestPassphrasePostRequest() {
		String tokencheckResult;
        tokencheckResult = checkToken(email, token);
        
        if (tokencheckResult.equals(Configuration.SUCCESS_MSG)) {
        	DatasetDAO dataHandler = new DatasetDBHandler();
    		Candidate confirmedCandidate = dataHandler.getCandidateByEmail(email);
        	String hash = Util.generatePwdHash(pass);
        	confirmedCandidate.setPassphrase(hash);
        	responseString = storePerson(confirmedCandidate);
        	if (!responseString.equals(Configuration.SUCCESS_MSG)) {
        		setStatus(Status.SERVER_ERROR_INTERNAL);
	        } else {
	        	responseString = deleteCandidate(email);
	            if (!responseString.equals(Configuration.SUCCESS_MSG)) {
	            	responseString = "Candidate deletion failed. " + responseString;
	            	setStatus(Status.SERVER_ERROR_INTERNAL);
	            }
	        }
        } else if (tokencheckResult.equals(Configuration.AUTH_ERROR_MSG)) {
        	setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
        	responseString = "Token mismatch";
        } else {
        	responseString = tokencheckResult;
        	setStatus(Status.SERVER_ERROR_INTERNAL);
        }
	}
	
	/**
	 * Triggers the insertion of a confirmed user with all the details contained in the {@link apostolos.eventplanning.datamanagement.Candidate} object
	 * provided as argument by using a {@link apostolos.eventplanning.datamanagement.DatasetDAO} object.
	 * 
	 * @param candidate the Candidate object containing all the details of the confirmed user to be inserted
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the confirmed user was successfully inserted, or an appropriate error message otherwise
	 */
	private String storePerson(Candidate candidate) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		
		try {
			String res = dataHandler.insertPerson(candidate);
			return res;
		} catch (IllegalArgumentException iae) {
          	log.error(iae.getMessage());
          	return iae.getMessage();
        }
	}
	
	/**
	 * Checks the validity of a (one-time) token for a certain e-mail address by using a {@link apostolos.eventplanning.datamanagement.DatasetDAO} object to query the database.
	 * 
	 * @param email the e-mail for which the (one-time) token is being checked
	 * @param token the token to be checked for validity
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the token is correct, or an appropriate error message otherwise
	 */
	protected String checkToken(String email, String token) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		
		try {
			String res =  dataHandler.checkToken(email, token);
			return res;
		} catch (IllegalArgumentException iae) {
          	log.error(iae.getMessage());
          	return iae.getMessage();
        }
	}
	
	/**
	 * Triggers the deletion of an unconfirmed user with all the provided e-mail address
	 * by using a {@link apostolos.eventplanning.datamanagement.DatasetDAO} object.
	 * 
	 * @param email the e-mail address of the (unconfirmed) user to be deleted
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the (unconfirmed) user entry is successfully deleted, or an appropriate error message otherwise
	 */
	private String deleteCandidate(String email) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		
		try {
			String res = dataHandler.deleteCandidate(email);
			return res;
		} catch (IllegalArgumentException iae) {
          	log.error(iae.getMessage());
          	return iae.getMessage();
        }
	}
	
}
