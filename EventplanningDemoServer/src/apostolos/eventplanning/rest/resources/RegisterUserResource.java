/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.resources;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;

import apostolos.eventplanning.Configuration;
import apostolos.eventplanning.Util;
import apostolos.eventplanning.datamanagement.Candidate;
import apostolos.eventplanning.datamanagement.DatasetDAO;
import apostolos.eventplanning.datamanagement.DatasetDBHandler;
import apostolos.eventplanning.rest.messages.RestRequest;
import apostolos.eventplanning.rest.messages.RestResponse;

/**
 * RegisterUserResource is a Restlet server resource that is used to register a user ("candidate") the event management system.
 * The user registration is finalized and made permanent only after the user has set the password (and thus confirmed the account).
 * The usage of credentials inside the request body has been preferred to alternative solutions (e.g., BASIC authentication inside the HTTP headers, OAuth2 server)
 * because it provides the same level of security, while it allows for fine-grained control of resources per user inside the server code.
 * 
 * @author Apostolos Papageorgiou
 */
public class RegisterUserResource extends BaseResource {
	
	private Candidate candidate;

	/**
	 * This is the entry point of a (POST) request to this resource (see annotation).
	 * It leads through the parsing and the handling of the request, as well as the preparation of the result, which will be sent as the POST response.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return the representation of the outgoing POST response
	 */
	@Post
	public Representation accept(Representation entity) {
		
		configureCorsHeaders(getRequest(), getResponse());
		
		RestRequest request = createRestRequestForIncomingMessage();
		request.logRestMessage();
		
		Representation result = null;
		
		if (parseRegisterUserPostRequest(entity)) {
			handleRegisterUserPostRequest();
		} else {
			; // If the parsing of the request fails then no further processing is required, while the status and the response message have already been appropriately set.
		}
		
		RestResponse response = createResponse(request);
		response.logRestMessage();
		
		result = new StringRepresentation(responseString, MediaType.TEXT_PLAIN);

		return result;
	}
	
	/**
	 * Parses the incoming message to check if all required fields are present, preparing appropriate error responses otherwise.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return true if all required fields are present and correctly parsed, false otherwise.
	 */
	private boolean parseRegisterUserPostRequest(Representation entity) {
		if (entity != null) {
			log.debug("Processing POST request with content type: " + entity.getMediaType().toString());
			Form form = new Form(entity);
	        
			String alias = form.getFirstValue("alias");
			if (alias == null || alias.equals("")) {
				responseString = "Parameter alias is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
	        String email = form.getFirstValue("email");
	        if (email == null || email.equals("")) {
				responseString = "Parameter email is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
	        String token = Util.generateToken(Configuration.TOKEN_LENGTH);
	        String name = form.getFirstValue("name");
	        String surname = form.getFirstValue("surname");
	        String tel = form.getFirstValue("telephone");
	        String address = form.getFirstValue("address");
	        String sex = form.getFirstValue("sex");
	        if (sex != null && !sex.equalsIgnoreCase("male") && !sex.equalsIgnoreCase("female")) {
	        	sex = "unknown";
	        }
	        int age = -1;
	        try {
	        	age = Integer.valueOf(form.getFirstValue("age"));
	        } catch(NumberFormatException nfe) {
	        	log.warn("Wrong input for candidate's age: " + nfe.getMessage());
	        }
	        
	        candidate = new Candidate(-1, alias, email, token, name, surname, tel, address, sex, age, Util.getCurrTimeAsTimestamp());
	        
	        return true;
		} else {
			log.warn("Processing POST request with NULL content");
			responseString = "The server received a request without data.";
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return false;
		}
	}
	
	/**
	 * Checks the validity (uniqueness) of the provided alias and triggers the insertion of the user if everything is fine, setting appropriate error status and messages otherwise.
	 * Note that in case a user with this e-mail already exists, then this entry will just be updated (upon confirmation and password setting actions), otherwise it is created.
	 * Respective information is provided in the response.
	 */
	private void handleRegisterUserPostRequest() {
		String emailCheckResult = checkEmail(candidate.getEmail());
		String aliasCheckResult = checkAlias(candidate.getAlias(), candidate.getEmail());
        
        if (aliasCheckResult == null) {
        	responseString = "This alias already exists. Please use a different one.";
        	setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
        	return;
        }
        
        if (!aliasCheckResult.equals(Configuration.SUCCESS_MSG)) {
        	responseString = aliasCheckResult;
        	setStatus(Status.SERVER_ERROR_INTERNAL);
        	return;
        }
        
        if (emailCheckResult == null) { // an entry with this e-mail existed so the request is handled as a "request for update"
        	responseString = storeCandidate(candidate);
        	if (!responseString.equals(Configuration.SUCCESS_MSG)) {
        		setStatus(Status.SERVER_ERROR_INTERNAL);
        	} else {
        		String mailBody = Util.getUpdatedRegistrationMailBody(candidate);
    			Util.sendMail(candidate.getEmail(), Configuration.REGISTRATION_MAIL_SUBJECT, mailBody);
    			// responseString = candidate.getPassphrase(); Only for test versions where the token is returned to the http client and not only sent via e-mail 
        	}
        } else if (emailCheckResult.equals(Configuration.SUCCESS_MSG)) { // an entry with this e-mail did not exist so it will be created
        	responseString = storeCandidate(candidate);
        	if (!responseString.equals(Configuration.SUCCESS_MSG)) {
        		setStatus(Status.SERVER_ERROR_INTERNAL);
        	} else {
        		String mailBody = Util.getNewRegistrationMailBody(candidate);
    			Util.sendMail(candidate.getEmail(), Configuration.REGISTRATION_MAIL_SUBJECT, mailBody);
    			// responseString = candidate.getPassphrase(); Only for test versions where the token is returned to the http client and not only sent via e-mail
    			setStatus(Status.SUCCESS_CREATED);
        	}
        } else {
        	responseString = emailCheckResult;
        	setStatus(Status.SERVER_ERROR_INTERNAL);
        }
	}
	
	/**
	 * Checks if a provided e-mail is already in use.
	 * 
	 * @param email the e-mail of the user that shall be registered
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} did not previously exist in the database, null if it did exist, or an appropriate error message upon a different kind of failure
	 */
	private String checkEmail(String email) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		
		try {
			String res =  dataHandler.checkEmailAvailability(email);
			return res;
		} catch (IllegalArgumentException iae) {
          	log.error(iae.getMessage());
          	return iae.getMessage();
        }
	}
	
	/**
	 * Checks if a provided alias is available or it is already in use.
	 * 
	 * @param alias the alias with which the user shall be registered
	 * @param email the e-mail of the user that shall be registered
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the alias is available for usage, null if not, or an appropriate error message upon a different kind of failure
	 */
	private String checkAlias(String alias, String email) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		
		try {
			String res =  dataHandler.checkAliasAvailability(alias, email);
			return res;
		} catch (IllegalArgumentException iae) {
          	log.error(iae.getMessage());
          	return iae.getMessage();
        }
	}

	/**
	 * Triggers the insertion of a user with all the details contained in the {@link apostolos.eventplanning.datamanagement.Candidate} object
	 * provided as argument by using a {@link apostolos.eventplanning.datamanagement.DatasetDAO} object.
	 * 
	 * @param candidate the Candidate object containing all the details of the user to be inserted
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the user was successfully inserted, or an appropriate error message otherwise
	 */
	private String storeCandidate(Candidate candidate) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		
		try {
			String res = dataHandler.insertCandidate(candidate);
			
			return res;
		} catch (IllegalArgumentException iae) {
          	log.error(iae.getMessage());
          	return iae.getMessage();
        }
	}
	
}
