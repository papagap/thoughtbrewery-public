/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.resources;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;

import apostolos.eventplanning.Configuration;
import apostolos.eventplanning.Util;
import apostolos.eventplanning.datamanagement.DatasetDAO;
import apostolos.eventplanning.datamanagement.DatasetDBHandler;
import apostolos.eventplanning.datamanagement.Session;
import apostolos.eventplanning.rest.messages.RestRequest;
import apostolos.eventplanning.rest.messages.RestResponse;

/**
 * LoginResource is a Restlet server resource that is used to initiate sessions for a specific user in the event management system.
 * 
 * @author Apostolos Papageorgiou
 */
public class LoginResource extends BaseResource {
	
	private String pass;

	/**
	 * This is the entry point of a (POST) request to this resource (see annotation).
	 * It leads through the parsing and the handling of the request, as well as the preparation of the result, which will be sent as the POST response.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return the representation of the outgoing POST response
	 */
	@Post
	public Representation accept(Representation entity) {
		
		configureCorsHeaders(getRequest(), getResponse());
		
		RestRequest request = createRestRequestForIncomingMessage();
		request.logRestMessage();
		
		Representation result = null;
		
		if (parseLoginPostRequest(entity)) {
			handleLoginPostRequest();
		} else {
			; // If the parsing of the request fails then no further processing is required, while the status and the response message have already been appropriately set.
		}
		
		RestResponse response = createResponse(request);
		response.logRestMessage();
		
		result = new StringRepresentation(responseString, MediaType.TEXT_PLAIN);

		return result;
	}
	
	/**
	 * Parses the incoming message to check if all required fields are present, preparing appropriate error responses otherwise.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return true if all required fields are present and correctly parsed, false otherwise.
	 */
	private boolean parseLoginPostRequest(Representation entity) {
		if (entity != null) {
			log.debug("Processing POST request with content type: " + entity.getMediaType().toString());
			Form form = new Form(entity);
	        
			pass = form.getFirstValue("pass");
			if (pass == null || pass.equals("")) {
				responseString = "Parameter pass is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
			email = form.getFirstValue("email");
			if (email == null || email.equals("")) {
				responseString = "Parameter email is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
	        
	        return true;
		} else {
			log.warn("Processing POST request with NULL content");
			responseString = "The server received a request without data.";
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return false;
		}
	}
	
	/**
	 * Checks the passphrase and triggers the insertion of the event if everything is fine, setting appropriate error status and messages otherwise.
	 */
	private void handleLoginPostRequest() {
		String passcheckResult;
        passcheckResult = checkPassphrase(email, pass);
        
        if (passcheckResult.equals(Configuration.SUCCESS_MSG)) {
        	// Generate the session id and its hash and then send the session id back to the user while storing its hash in the database
        	String sessionId = Util.generateToken(Configuration.SESSION_ID_LENGTH);
        	String sessionIdHash = Util.generatePwdHash(sessionId);
        	Session session = new Session(sessionIdHash, email, Util.getCurrTimeAsTimestamp());
        	responseString = storeSession(session);
        	if (!responseString.equals(Configuration.SUCCESS_MSG)) {
    			if (responseString.equals(Configuration.DUPLICATE_ERROR_MSG)) {
    				responseString = "A rare session id conflict happened, please try to login again.";
    				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
    			} else {
    				setStatus(Status.SERVER_ERROR_INTERNAL);
    			}
        	} else {
        		responseString = sessionId;
        		DatasetDAO dataHandler = new DatasetDBHandler();
            	String alias = "";
            	try {
            		alias = dataHandler.getPersonByEmail(email).getAlias();
            		responseString += ";" + alias; // Send colon-separated session id and alias to be set as cookies. Note that the session id cannot contain a colon so it will be easy to split in the client.
            	} catch (NullPointerException npe) {
            		responseString = "A rare error happened while retrieving the alias, please try to login again.";
            		setStatus(Status.SERVER_ERROR_INTERNAL);
            	}
        	}
        } else if (passcheckResult.equals(Configuration.AUTH_ERROR_MSG)) {
        	setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
        	responseString = "Passphrase mismatch.";
        } else {
        	responseString = passcheckResult;
        	setStatus(Status.SERVER_ERROR_INTERNAL);
        }
	}
	
	/**
	 * Triggers the insertion of a session with all the details contained in the {@link apostolos.eventplanning.datamanagement.Session}
	 * provided as argument by using a {@link apostolos.eventplanning.datamanagement.DatasetDAO} object.
	 * 
	 * @param session the session object containing all the details of the session to be inserted
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the session was successfully inserted, or an appropriate error message otherwise
	 */
	private String storeSession(Session session) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		
		try {
			String res = dataHandler.insertSession(session);
			return res;
		} catch (IllegalArgumentException iae) {
          	log.error(iae.getMessage());
          	return iae.getMessage();
        }
	}

}
