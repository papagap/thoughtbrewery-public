/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.resources;

import java.sql.Timestamp;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;

import apostolos.eventplanning.Configuration;
import apostolos.eventplanning.Util;
import apostolos.eventplanning.datamanagement.DatasetDAO;
import apostolos.eventplanning.datamanagement.DatasetDBHandler;
import apostolos.eventplanning.datamanagement.Event;
import apostolos.eventplanning.rest.messages.RestRequest;
import apostolos.eventplanning.rest.messages.RestResponse;

/**
 * RegisterEventResource is a Restlet server resource that is used to insert an event in the event management system.
 * 
 * @author Apostolos Papageorgiou
 */
public class RegisterEventResource extends BaseResource {
	
	private Event event;
	private String pass;

	/**
	 * This is the entry point of a (POST) request to this resource (see annotation).
	 * It leads through the parsing and the handling of the request, as well as the preparation of the result, which will be sent as the POST response.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return the representation of the outgoing POST response
	 */
	@Post
	public Representation accept(Representation entity) {
		
		configureCorsHeaders(getRequest(), getResponse());
		
		RestRequest request = createRestRequestForIncomingMessage();
		request.logRestMessage();
		
		Representation result = null;
		
		if (parseRegisterEventPostRequest(entity)) {
			handleRegisterEventPostRequest();
		} else {
			; // If the parsing of the request fails then no further processing is required, while the status and the response message have already been appropriately set.
		}
		
		RestResponse response = createResponse(request);
		response.logRestMessage();
		
		result = new StringRepresentation(responseString, MediaType.TEXT_PLAIN);

		return result;
	}
	
	/**
	 * Parses the incoming message to check if all required fields are present, preparing appropriate error responses otherwise.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return true if all required fields are present and correctly parsed, false otherwise.
	 */
	private boolean parseRegisterEventPostRequest(Representation entity) {
		if (entity != null) {
			log.debug("Processing POST request with content type: " + entity.getMediaType().toString());
			Form form = new Form(entity);
	        
			pass = form.getFirstValue("pass");
			String formEmail = form.getFirstValue("email");
			if (formEmail != null && !formEmail.equals("")) {
	        	email = formEmail;
	        }
	        String name = form.getFirstValue("eventName");
	        if (name == null || name.equals("")) {
				responseString = "Parameter eventName is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
	        String info = form.getFirstValue("info");
	        String place = form.getFirstValue("place");
	        String address = form.getFirstValue("address");
	        float lat;
	        float lon;
	        try {
	        	lat = Float.valueOf(form.getFirstValue("lat"));
	        	lon = Float.valueOf(form.getFirstValue("lon"));
	        } catch (NumberFormatException nfe) {
	        	log.warn("Wrong coordinates format: " + nfe.getMessage());
	        	responseString = "Wrong coordinates format!";
	        	setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
	        	return false;
	        } catch (NullPointerException npe) {
	        	log.warn("Wrong coordinates format: " + npe.getMessage());
	        	responseString = "Wrong coordinates format!";
	        	setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
	        	return false;
	        }
	        String type = form.getFirstValue("type");
	        if (type != null && !type.equalsIgnoreCase("invitation") && !type.equalsIgnoreCase("foodsharing")) {
	        	type = "unknown";
	        }
	        String timeStr = form.getFirstValue("date") + " " + form.getFirstValue("time") + ":00";
	        Timestamp time = null;
	        try {
	        	time = Timestamp.valueOf(timeStr);
	        } catch (IllegalArgumentException iae) {
	        	responseString = "Wrong event time format!";
	        	setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
	        	return false; // Note that although the event date/time might not be a mandatory field in the data storage system, e.g., database, thus allowing in principle for event entries without a date/time, this REST interface currently returns error upon wrong or null date/time, because it is meant to be used primarily from web sites with enabled date/time-widgets which should be used to submit valid date/time.
	        }
	        Timestamp regTime = Util.getCurrTimeAsTimestamp();
	        event = new Event(-1, name, email, info, place, address, lat, lon, type, regTime, time);
	        
	        return true;
		} else {
			log.warn("Processing POST request with NULL content");
			responseString = "The server received a request without data.";
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return false;
		}
	}
	
	/**
	 * Checks the passphrase and triggers the insertion of the event if everything is fine, setting appropriate error status and messages otherwise.
	 */
	private void handleRegisterEventPostRequest() {
		String passcheckResult;
        passcheckResult = checkPassOrSession(event.getEmail(), pass);
        
        if (passcheckResult.equals(Configuration.SUCCESS_MSG)) {
        	responseString = storeEvent(event);
        	if (!responseString.equals(Configuration.SUCCESS_MSG)) {
        		if (responseString.equals(Configuration.DUPLICATE_ERROR_MSG)) {
        			responseString = "An event with this name already exists";
        			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
        		} else {
        			setStatus(Status.SERVER_ERROR_INTERNAL);
        		}
	        }
        } else if (passcheckResult.equals(Configuration.AUTH_ERROR_MSG)) {
        	setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
        	responseString = "Passphrase mismatch. Maybe you are just not logged in?";
        } else {
        	responseString = passcheckResult;
        	setStatus(Status.SERVER_ERROR_INTERNAL);
        }
	}
	
	/**
	 * Triggers the insertion of an event with all the details contained in the {@link apostolos.eventplanning.datamanagement.Event}
	 * provided as argument by using a {@link apostolos.eventplanning.datamanagement.DatasetDAO} object.
	 * 
	 * @param event the event object containing all the details of the event to be inserted
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the event was successfully inserted, or an appropriate error message otherwise
	 */
	private String storeEvent(Event event) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		
		try {
			String res = dataHandler.insertEvent(event);
			return res;
		} catch (IllegalArgumentException iae) {
          	log.error(iae.getMessage());
          	return iae.getMessage();
        }
	}

}
