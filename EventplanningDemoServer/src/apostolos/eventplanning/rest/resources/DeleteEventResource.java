/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.resources;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;

import apostolos.eventplanning.Configuration;
import apostolos.eventplanning.datamanagement.DatasetDAO;
import apostolos.eventplanning.datamanagement.DatasetDBHandler;
import apostolos.eventplanning.rest.messages.RestRequest;
import apostolos.eventplanning.rest.messages.RestResponse;

/**
 * DeleteEventResource is a Restlet server resource that is used to delete an event from the event management system.
 * 
 * @author Apostolos Papageorgiou
 */
public class DeleteEventResource extends BaseResource {
	
	private String event;
    private String pass;

	/**
	 * This is the entry point of a (POST) request to this resource (see annotation).
	 * It leads through the parsing and the handling of the request, as well as the preparation of the result, which will be sent as the POST response.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return the representation of the outgoing POST response
	 */
	@Post
	public Representation accept(Representation entity) {
		
		configureCorsHeaders(getRequest(), getResponse());
		
		RestRequest request = createRestRequestForIncomingMessage();
		request.logRestMessage();
		
		Representation result = null;
		
		if (parseDeleteEventPostRequest(entity)) {
			handleDeleteEventPostRequest();
		} else {
			; // If the parsing of the request fails then no further processing is required, while the status and the response message have already been appropriately set.
		}
		
		RestResponse response = createResponse(request);
		response.logRestMessage();
		
		result = new StringRepresentation(responseString, MediaType.TEXT_PLAIN);

		return result;
	}
	
	/**
	 * Parses the incoming message to check if all required fields are present, preparing appropriate error responses otherwise.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return true if all required fields are present and correctly parsed, false otherwise.
	 */
	private boolean parseDeleteEventPostRequest(Representation entity) {
		if (entity != null) {
			log.debug("Processing POST request with content type: " + entity.getMediaType().toString());
			Form form = new Form(entity);
	        
			event = form.getFirstValue("eventName");
			if (event == null || event.equals("")) {
				responseString = "Parameter eventName is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
	        String formEmail = form.getFirstValue("email");
	        if (formEmail != null && !formEmail.equals("")) {
	        	email = formEmail;
	        }
	        pass = form.getFirstValue("pass");
	        
	        return true;
		} else {
			log.warn("Processing POST request with NULL content");
			responseString = "The server received a request without data.";
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return false;
		}
	}
	
	/**
	 * Checks the passphrase and the event ownership and triggers the deletion of the event if everything is fine, setting appropriate error status and messages otherwise.
	 */
	private void handleDeleteEventPostRequest() {
		String passcheckResult;
        String ownershipCheckResult;
        passcheckResult = checkPassOrSession(email, pass);
        ownershipCheckResult = checkOwnership(event, email);
        
        if (passcheckResult.equals(Configuration.SUCCESS_MSG) && ownershipCheckResult.equals(Configuration.SUCCESS_MSG)) {
        	responseString = deleteEvent(event);
        	if (!responseString.equals(Configuration.SUCCESS_MSG)) {
        		setStatus(Status.SERVER_ERROR_INTERNAL);
	        }
        } else if (passcheckResult.equals(Configuration.SUCCESS_MSG) && !ownershipCheckResult.equals(Configuration.SUCCESS_MSG)) {
        	setStatus(Status.CLIENT_ERROR_FORBIDDEN);
        	responseString = "Not the event owner. Maybe you are just not logged in?";
        } else if (passcheckResult.equals(Configuration.AUTH_ERROR_MSG)) {
        	setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
        	responseString = "Passphrase mismatch. Maybe you are just not logged in?";
        } else {
        	responseString = passcheckResult;
        	setStatus(Status.SERVER_ERROR_INTERNAL);
        }
	}
	
	/**
	 * Checks if the user with the specified e-mail if the owner of the specified event.
	 * 
	 * @param event the event for which ownership shall be checked
	 * @param email the e-mail of the user whose ownership of an event is being checked.
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the user is the owner of the event, or an appropriate error message otherwise
	 */
	private String checkOwnership(String event, String email) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		
		try {
			String res =  dataHandler.checkEventOwnership(event, email);
			return res;
		} catch (IllegalArgumentException iae) {
          	log.error(iae.getMessage());
          	return iae.getMessage();
        }
	}
	
	/**
	 * Triggers the deletion of the event with the specified name from the database by using a {@link apostolos.eventplanning.datamanagement.DatasetDAO} object.
	 * 
	 * @param eventName the name of the event to be deleted
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the event was successfully deleted, or an appropriate error message otherwise
	 */
	private String deleteEvent(String eventName) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		
		try {
			String res = dataHandler.deleteEvent(eventName);
			return res;
		} catch (IllegalArgumentException iae) {
          	log.error(iae.getMessage());
          	return iae.getMessage();
        }
	}

}
