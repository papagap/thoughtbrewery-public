/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.resources;

import java.util.HashMap;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Cookie;
import org.restlet.data.Header;
import org.restlet.resource.ServerResource;
import org.restlet.util.Series;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import apostolos.eventplanning.Configuration;
import apostolos.eventplanning.datamanagement.DatasetDAO;
import apostolos.eventplanning.datamanagement.DatasetDBHandler;
import apostolos.eventplanning.rest.messages.RestMessageType;
import apostolos.eventplanning.rest.messages.RestRequest;
import apostolos.eventplanning.rest.messages.RestResponse;

/**
 * BaseResource shall be extended by every Restlet server resource of the event management system,
 * because it contains generic methods useful (and uniform) for all server resources,
 * e.g., for configuring the CORS headers of REST requests and responses, checking provided passphrases,
 * or instantiating {@link apostolos.eventplanning.rest.messages.RestRequest} and {@link apostolos.eventplanning.rest.messages.RestResponse} objects for the current context.
 * 
 * @author Apostolos Papageorgiou
 */
public class BaseResource extends ServerResource {

	protected static Logger log = LoggerFactory.getLogger("EventPlanning");

	protected String sessionId;
	protected String email;
	protected String responseString;
	
	/**
	 * Checks if the IP of the origin server (i.e., the server who instructed the client via javascript to invoke this service), is among those allowed to trigger this invocation via javascript code.
	 * If yes (or if the system is configured to allow ALL origins anyway), then it adds that ip address to the response header as an allowed origin.
	 * The CORS strategy can also be configured on application-level (i.e. for all resources) using the CorsService object of the restlet library,
	 * but the current solution works i) in a more fine-granular way and ii) also for "null origin" (e.g. from local file system), which might not be achievable with the CorsService only.
	 * 
	 * @param request the server request that is checked for compliance with the (configured) CORS policy
	 * @param response the server response whose headers shall be set according to the request headers and the (configured) CORS policy
	 */
	public void configureCorsHeaders(Request request, Response response) {
		 
		@SuppressWarnings("unchecked")
		Series<Header> requestHeaders = (Series<Header>) request.getAttributes().get("org.restlet.http.headers");
		@SuppressWarnings("unchecked")
		Series<Header> responseHeaders = (Series<Header>) response.getAttributes().get("org.restlet.http.headers");
		
		String origin = requestHeaders.getFirstValue("Origin", true);
		
		if (responseHeaders == null) {
		    responseHeaders = new Series<Header>(Header.class);
		    response.getAttributes().put("org.restlet.http.headers", responseHeaders);
		} 
		if (Configuration.ALLOWED_ORIGINS.contains(origin) || Configuration.ALLOWED_ORIGINS.contains("*")) {
			response.setAccessControlAllowOrigin(origin);
		}
    }
	
	/**
	 * Creates a response object for a specific request based on the request object and the (dynamically set) response message.
	 * 
	 * @param request the REST request for which a response is being created 
	 * @return the object that represents the response that will be sent
	 */
	protected RestResponse createResponse(RestRequest request) {
		RestResponse res = new RestResponse(request.getType());
		res.setClientAddress(request.getClientAddress());
		res.setResponseMsg(responseString);
		return res;
	}
	
	/**
	 * Creates a REST request object for an incoming message.
	 * 
	 * @return the object that represents the incoming message as a REST request
	 */
	protected RestRequest createRestRequestForIncomingMessage() {
		RestMessageType requestType = RestMessageType.Unknown;
		try {
			requestType = RestMessageType.valueOf(getRequest().getResourceRef().getLastSegment());
		} catch(IllegalArgumentException iae) { // The type of request is not recognized
			log.error("The REST request type was unknown."); // This should never happen if the Restlet router is working properly
			return null;
		} catch(NullPointerException npe) { // The type of request is null
			log.error("The REST request type was unknown."); // This should never happen if the Restlet router is working properly
			return null;
		}
		RestRequest req = new RestRequest(requestType);
		req.setClientAddress(getClientInfo().getUpstreamAddress());
		req.setParams((HashMap<String, String>) getQuery().getValuesMap());
		
		Series<Cookie> cookies = getRequest().getCookies();
        sessionId = cookies.getFirstValue(Configuration.SESSION_COOKIE_NAME);
        email = cookies.getFirstValue(Configuration.EMAIL_COOKIE_NAME);
        log.debug(cookies.toString());
        log.debug("email: " + email);
        log.debug("sessionId: " + sessionId);
		
		return req;
	}
	
	/**
	 * Checks the validity of a passphrase for a certain e-mail address by using a {@link apostolos.eventplanning.datamanagement.DatasetDAO} object to query the database.
	 * 
	 * @param email the e-mail for which the passphrase is being checked
	 * @param pass the passphrase to be checked for validity
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the passphrase is correct, or an appropriate error message otherwise
	 */
	protected String checkPassphrase(String email, String pass) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		
		try {
			String res =  dataHandler.checkPassphrase(email, pass);
			return res;
		} catch (IllegalArgumentException iae) {
          	log.error(iae.getMessage());
          	return iae.getMessage();
        }
	}
	
	/**
	 * Checks the validity of a session id for a certain e-mail address by using a {@link apostolos.eventplanning.datamanagement.DatasetDAO} object to query the database.
	 * 
	 * @param email the e-mail for which the session id is being checked
	 * @param session the session id to be checked for validity
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the session id is valid, or an appropriate error message otherwise
	 */
	protected String checkSession(String email, String session) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		
		try {
			String res =  dataHandler.checkSession(email, session);
			return res;
		} catch (IllegalArgumentException iae) {
          	log.error(iae.getMessage());
          	return iae.getMessage();
        }
	}
	
	/**
	 * Checks if either the provided pass or the provided session id are currently valid for the user with the specified e-mail address. 
	 * 
	 * @param email the e-mail for which the provided passphrase will be checked if no active session already exists
	 * @param pass the passphrase to be checked for validity if no session exists
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if either an active session exists or the passphrase and e-mail combination is valid, or an appropriate error message otherwise
	 */
	protected String checkPassOrSession(String email, String pass) {
		if (checkSession(email, sessionId).equals(Configuration.SUCCESS_MSG)) {
			return Configuration.SUCCESS_MSG;
		} else {
			return checkPassphrase(email, pass);
		}
	}
	
}
