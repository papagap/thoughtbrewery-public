/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.resources;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;

import apostolos.eventplanning.Configuration;
import apostolos.eventplanning.Util;
import apostolos.eventplanning.datamanagement.DatasetDAO;
import apostolos.eventplanning.datamanagement.DatasetDBHandler;
import apostolos.eventplanning.datamanagement.Event;
import apostolos.eventplanning.datamanagement.EventRequest;
import apostolos.eventplanning.rest.messages.RestRequest;
import apostolos.eventplanning.rest.messages.RestResponse;

/**
 * RequestParticipationResource is a Restlet server resource that is used to insert an event request in the event management system.
 * 
 * @author Apostolos Papageorgiou
 */
public class RequestParticipationResource extends BaseResource {
	
	private EventRequest eventRequest;
	private String pass; 

	/**
	 * This is the entry point of a (POST) request to this resource (see annotation).
	 * It leads through the parsing and the handling of the request, as well as the preparation of the result, which will be sent as the POST response.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return the representation of the outgoing POST response
	 */
	@Post
	public Representation accept(Representation entity) {
		
		configureCorsHeaders(getRequest(), getResponse());
		
		RestRequest request = createRestRequestForIncomingMessage();
		request.logRestMessage();
		
		Representation result = null;
		
		if (parseRequestParticipationPostRequest(entity)) {
			handleRequestParticipationPostRequest();
		} else {
			; // If the parsing of the request fails then no further processing is required, while the status and the response message have already been appropriately set
		}
		
		RestResponse response = createResponse(request);
		response.logRestMessage();
		
		result = new StringRepresentation(responseString, MediaType.TEXT_PLAIN);

		return result;
	}
	
	/**
	 * Parses the incoming message to check if all required fields are present, preparing appropriate error responses otherwise.
	 * 
	 * @param entity the representation of the incoming POST request
	 * @return true if all required fields are present and correctly parsed, false otherwise.
	 */
	private boolean parseRequestParticipationPostRequest(Representation entity) {
		if (entity != null) {
			log.debug("Processing POST request with content type: " + entity.getMediaType().toString());
			Form form = new Form(entity);
	        
			pass = form.getFirstValue("pass");
			String formEmail = form.getFirstValue("email");
			if (formEmail != null && !formEmail.equals("")) {
	        	email = formEmail;
	        }
	        String event = form.getFirstValue("eventName");
	        if (event == null || event.equals("")) {
				responseString = "Parameter eventName is missing or empty.";
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return false;
			}
	        String message = form.getFirstValue("message");
	        
	        eventRequest = new EventRequest(-1, email, event, message, Util.getCurrTimeAsTimestamp());
	        
	        return true;
		} else {
			log.warn("Processing POST request with NULL content");
			responseString = "The server received a request without data.";
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return false;
		}
	}
	
	/**
	 * Checks the passphrase and triggers the insertion of the event (participation) request if everything is fine, setting appropriate error status and messages otherwise.
	 */
	private void handleRequestParticipationPostRequest() {
		String passcheckResult;
        passcheckResult = checkPassOrSession(eventRequest.getEmail(), pass);
        
        if (passcheckResult.equals(Configuration.SUCCESS_MSG)) {
        	
        	DatasetDAO dataHandler = new DatasetDBHandler();
        	Event event = dataHandler.getEventByName(eventRequest.getEvent());
        	if (event == null) {
        		responseString = "No event with this name was found.";
            	setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
        	} else {
        		responseString = storeEventRequest(eventRequest);
            	if (!responseString.equals(Configuration.SUCCESS_MSG)) {
            		setStatus(Status.SERVER_ERROR_INTERNAL);
    	        } else {
    	    		// Send e-mail to event owner and event requester and stop the involvement of the platform here.
    	    		Util.sendMail(event.getEmail(), Configuration.REQUEST_MAIL_SUBJECT, Util.getRequestMailBody(eventRequest));
    	    		Util.sendMail(eventRequest.getEmail(), Configuration.REQUEST_MAIL_SUBJECT, Util.getRequestMailBody(eventRequest));
    	        }
        	}
        } else if (passcheckResult.equals(Configuration.AUTH_ERROR_MSG)) {
        	setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
        	responseString = "Passphrase mismatch. Maybe you are just not logged in?";
        } else {
        	responseString = passcheckResult;
        	setStatus(Status.SERVER_ERROR_INTERNAL);
        }
	}
	
	/**
	 * Triggers the insertion of an event request with all the details contained in the {@link apostolos.eventplanning.datamanagement.EventRequest}
	 * provided as argument by using a {@link apostolos.eventplanning.datamanagement.DatasetDAO} object.
	 * 
	 * @param eventRequest the object containing all the details of the event (participation) request to be inserted
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the event request was successfully inserted, or an appropriate error message otherwise
	 */
	private String storeEventRequest(EventRequest eventRequest) {
		DatasetDAO dataHandler = new DatasetDBHandler();
		
		try {
			String res = dataHandler.insertEventRequest(eventRequest);
			return res;
		} catch (IllegalArgumentException iae) {
          	log.error(iae.getMessage());
          	return iae.getMessage();
        }
	}

}
