/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * RestMessage is an abstract class that can be extended by classes that represent concrete REST messages, e.g., concrete requests or responses
 * 
 * @author Apostolos Papageorgiou
 */
public abstract class RestMessage {
	
	protected static Logger log = LoggerFactory.getLogger("EventPlanning");

	private RestMessageType type = RestMessageType.Unknown;
	private String clientAddress;
	
	public RestMessage() {
		this.type = RestMessageType.Unknown;
	}
	
	public RestMessage(RestMessageType type) {
		this.type = type;
	}

	/**
	 * @return the type of this REST message, i.e., the name of the (REST API) function that this message is related to  
	 */
	public RestMessageType getType() {
		return type;
	}

	/**
	 * @param type a type to be set as the type of this REST message, i.e., the name of the (REST API) function that this message is related to
	 */
	public void setType(RestMessageType type) {
		this.type = type;
	}

	/**
	 * @return the IP address of the client that either sent or shall receive this message
	 */
	public String getClientAddress() {
		return clientAddress;
	}

	/**
	 * @param clientAddress the IP address to be set as the IP address of the client that either sent or shall receive this message
	 */
	public void setClientAddress(String clientAddress) {
		this.clientAddress = clientAddress;
	}
	
	/**
	 * To be implemented by each concrete REST message (i.e., each class that extends this class),
	 * in order to specify how to log the reception (or the sending) of a REST message. 
	 */
	public abstract void logRestMessage();
	
}
