/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.messages;

/**
 * RestMessageType is an enumeration of all possible types of REST messages in the event management system,
 * i.e., the names of all functions of the REST API of the system.
 * 
 * @author Apostolos Papageorgiou
 */
public enum RestMessageType {
	insertDatastreamItem,
	getLastId,
	getValueById,
	setPassphrase,
	registerUser,
	listEvents,
	listEventRequests,
	requestParticipation,
	registerEvent,
	deleteEvent,
	setPreferredProxy,
	login,
	Unknown
}
