/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.messages;

/**
 * RestResponse is a {@link apostolos.eventplanning.rest.messages.RestMessage} that represents a REST response.
 * 
 * @author Apostolos Papageorgiou
 */
public class RestResponse extends RestMessage {
		
	public String responseMsg;
	
	public RestResponse(RestMessageType type) {
		super(type);
	}

	/**
	 * @return the text of the REST response
	 */
	public String getResponseMsg() {
		return responseMsg;
	}

	/**
	 * @param responseMsg the text that shall be included in the REST response
	 */
	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}
	
	/* (non-Javadoc)
	 * @see apostolos.eventplanning.rest.messages.RestMessage#logRestMessage()
	 */
	public void logRestMessage() {
		log.info("The following HTTP Response for {} shall be sent to {}:", getType(), getClientAddress());
		log.info(responseMsg);
	}
	
}
