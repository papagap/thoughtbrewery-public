/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.rest.messages;

import java.util.HashMap;

/**
 * RestRequest is a {@link apostolos.eventplanning.rest.messages.RestMessage} that represents a REST request.
 * 
 * @author Apostolos Papageorgiou
 */
public class RestRequest extends RestMessage {
	
	private HashMap<String, String> params;
	
	public RestRequest() {
		super();
		params = new HashMap<String, String>();
	}
	
	public RestRequest(RestMessageType type) {
		super(type);
		params = new HashMap<String, String>();
	}
	
	/**
	 * @return the parameters of the REST request
	 */
	public HashMap<String, String> getParams() {
		return params;
	}

	/**
	 * @param params the map of srings that shall be set as the parameters of the REST request
	 */
	public void setParams(HashMap<String, String> params) {
		this.params = params;
	}

	/* (non-Javadoc)
	 * @see apostolos.eventplanning.rest.messages.RestMessage#logRestMessage()
	 */
	public void logRestMessage() {
		log.info("HTTP Request {} was received from {}.", getType(), getClientAddress());
		log.debug("Request URL Parameters: " + params);
	}
	
}
