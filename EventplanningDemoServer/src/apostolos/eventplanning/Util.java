/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Properties;
import java.util.Random;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;
import org.restlet.Component;
import org.restlet.Server;
import org.restlet.data.Protocol;

import apostolos.eventplanning.datamanagement.Candidate;
import apostolos.eventplanning.datamanagement.EventRequest;

/**
 * Util defines general-purpose static functions that might be useful in different parts of the program,
 * e.g., for generating random passphrases or sending e-mails.
 * 
 * @author Apostolos Papageorgiou
 */
public class Util {
	
	private static Logger log = Logger.getLogger("Util");
	
	private static final String symbols;

	static {
		StringBuilder tmp = new StringBuilder();
	    for (char ch = '0'; ch <= '9'; ++ch) {
	    	tmp.append(ch);
	    }
	    for (char ch = 'a'; ch <= 'z'; ++ch) {
	    	tmp.append(ch);
	    }
	    for (char ch = 'A'; ch <= 'Z'; ++ch) {
	    	tmp.append(ch);
	    }
	    symbols = tmp.toString();
	}
	
	/**
	 * Configures and fires a standalone web server with the {@link apostolos.eventplanning.EventPlanningRestlet} attached,
	 * in order to ease "full stack"-testing without having to use tomcat and deploy war files on it.
	 * 
	 * @param port the port at which the server shall be started on the local machine
	 */
	public static void startStandaloneServerWithRestletAttached(int port) {
		Component component = new Component();
	
		Server server = component.getServers().add(Protocol.HTTP, port);
		server.getContext().getParameters().add("maxTotalConnections", "2000");
		server.getContext().getParameters().add("maxThreads", "2048");

		component.getDefaultHost().attach("", new EventPlanningRestlet());
	
		try {
			component.start();
			log.info("HTTP Server started on port " + port);
		} catch (Exception e) {
			log.error("Error starting server: " + e.getMessage());
		}

	}
	
	/**
	 * Generates a random passphrase (of a fixed length, as set in the properties)
	 * which consists currently exclusively of letters and digits.
	 * 
	 * @return a random String that consists of letters and digits
	 */
	public static String generatePassphrase() {
		Random rand = new Random();
		char[] text = new char[Configuration.PASSWORD_LENGTH];
	    for (int i = 0; i < Configuration.PASSWORD_LENGTH; i++)
	    {
	        text[i] = symbols.charAt(rand.nextInt(symbols.length()));
	    }
	    return new String(text);
	}
	
	/**
	 * Generates a random token of the specified length,
	 * which consists currently exclusively of letters and digits.
	 * 
	 * @param length the length of the token to be generated, in number of characters
	 * @return a random String that consists of letters and digits
	 */
	public static String generateToken(int length) {
		Random rand = new Random();
		char[] text = new char[length];
	    for (int i = 0; i < length; i++)
	    {
	        text[i] = symbols.charAt(rand.nextInt(symbols.length()));
	    }
	    return new String(text);
	}
	
	/**
	 * Generates a hash given a password.
	 * The bcrypt algorithm is used.
	 * Note passwords should never be stored, but rather only their hashes.
	 * 
	 * @param pass the password for which a hash shall be generated
	 * @return the generated hash as a String
	 */
	public static String generatePwdHash(String pass) {
		String salt = BCrypt.gensalt(12); // A workload equal to 12 is a safe default that is used also in the example of the jBCrypt home page
		String hash = BCrypt.hashpw(pass, salt);
		return hash;
	}
	
	/**
	 * Compares a computed hash from a provided plain text password (first argument) with a stored hash (second argument).
	 * The bcrypt algorithm is used.
	 * 
	 * @param pass The plain text password
	 * @param hash The (typically previously stored) password hash
	 * @return true if the password matches the hash, false otherwise
	 */
	public static boolean checkPwd(String pass, String hash) {
		if (hash == null || !hash.startsWith("$2a$")) {
			return false;
		} else {
			return BCrypt.checkpw(pass, hash); 
		}
	}
	
	/**
	 * Sends an e-mail using a gmail account to the
	 * specified address, containing the provided subject and text.
	 * The sender e-mail address and password are configurable properties and the method has been tested to work with Gmail sender addresses.
	 * 
	 * @param toAddress the address of the e-mail recipient
	 * @param subject the subject of the e-mail to be sent
	 * @param text the body of the e-mail to be sent
	 */
	public static void sendGMail(String toAddress, String subject, String text) {

		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(Configuration.MAIL_FROM_ADDRESS, Configuration.MAIL_PASS);
			}
		  });

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Configuration.MAIL_FROM_ADDRESS));
			message.setRecipients(Message.RecipientType.TO,	InternetAddress.parse(toAddress));
			message.setSubject(subject);
			message.setText(text);

			Transport.send(message);

			log.info("E-mail has been sent to: " + toAddress);

		} catch (MessagingException me) {
			log.error(me.getMessage());
		}
	}
	
	/**
	 * Sends an e-mail using a locally running mail server to the
	 * specified address, containing the provided subject and text.
	 * The sender e-mail address and password are configurable properties.
	 * 
	 * @param toAddress the address of the e-mail recipient
	 * @param subject the subject of the e-mail to be sent
	 * @param text the body of the e-mail to be sent
	 */
	public static void sendMail(String toAddress, String subject, String text) {

		Properties props = new Properties();
		props.put("mail.smtp.host", "localhost");
		Session s = Session.getInstance(props,null);

		MimeMessage message = new MimeMessage(s);

		try {
			InternetAddress from = new InternetAddress(Configuration.MAIL_FROM_ADDRESS);
			InternetAddress to = new InternetAddress(toAddress);
			message.setFrom(from);
			message.addRecipient(Message.RecipientType.TO, to);
			message.setSubject(subject);
			message.setText(text);
			Transport.send(message);
		} catch (AddressException ae) {
			log.error("Error with address used for e-mailing: " + ae.getMessage());
		} catch (MessagingException me) {
			log.error("Error preparing or sending e-mail message: " + me.getMessage());
		}
	}
	
	/**
	 * Returns the text that should be sent via e-mail to a candidate upon registering in the system. 
	 * 
	 * @param candidate the object containing the details of the candidate that is performing the registration
	 * @return the text of the e-mail body that should be sent to the registered candidate  
	 */
	public static String getNewRegistrationMailBody(Candidate candidate) {
		String res = "Hey user " + candidate.getEmail() + "!";
		res += "\n";
		res += "A new account has been requested for you at thoughtbrewery.net.";
		res += "\n";
		res += "If this was done by you, you can visit the following link to confirm the account creation and set your password:";
		res += "\n";
		res += "https://thoughtbrewery.net/setpassword.html";
		res += "\n";
		res += "Please use your e-mail address and the token " + candidate.getPassphrase();
		res += "\n";
		res += "\n";
		res += "Greetings,";
		res += "\n";
		res += "Your Thoughtbrewery system";
		
		return res;
	}
	
	/**
	 * Returns the text that should be sent via e-mail to a candidate upon updating their registration.
	 * 
	 * @param candidate the object containing the details of the candidate that is updating their registration
	 * @return the text of the e-mail body that should be sent to the candidate
	 */
	public static String getUpdatedRegistrationMailBody(Candidate candidate) {
		String res = "Hey user " + candidate.getEmail() + "!";
		res += "\n";
		res += "A request for updating your account at thoughtbrewery.net has been submitted.";
		res += "\n";
		res += "If this was done by you, you can visit the following link to confirm the updates and reset your password:";
		res += "\n";
		res += "https://thoughtbrewery.net/setpassword.html";
		res += "\n";
		res += "Please use your e-mail address and the token " + candidate.getPassphrase();
		res += "\n";
		res += "\n";
		res += "Greetings,";
		res += "\n";
		res += "Your Thoughtbrewery system";
		
		return res;
	}
	
	/**
	 * Returns the text that should be sent via e-mail to a candidate upon registering in the system. 
	 * 
	 * @param request the object containing the details of the event request that has been registered in the system
	 * @return the text of the e-mail body that should be sent to the event owner as well as to the candidate that registers for participating to the event
	 */
	public static String getRequestMailBody(EventRequest request) {
		String res = "Hello!";
		res += "\n";
		res += "A request for participation to an event with the following data has been submitted:";
		res += "\n";
		res += "\n";
		res += "Event name: ";
		res += request.getEvent();
		res += "\n";
		res += "Requester: ";
		res += request.getEmail();
		res += "\n";
		res += "Message: ";
		res += request.getMessage();
		res += "\n";
		res += "\n";
		res += "If you are the owner of this event and you are interested in this request, please contact the requester via e-mail.";
		res += "\n";
		res += "If you are the requester, the event owner might contact you with regard to participating in this event.";
		res += "\n";
		res += "\n";
		res += "Greetings,";
		res += "\n";
		res += "Your Thoughtbrewery system";
		
		return res;
	}
	
	/**
	 * Check if a String value represents an integer number.
	 * 
	 * @param s the String to be checked for representing (or not) an integer number
	 * @return true if the provided String represents an integer number, false otherwise
	 */
	public static boolean isInteger(String s) {
		int len = s.length();
		if (s == null || len == 0) {
	        return false;
	    }
	    int pos = 0;
	    if (s.charAt(0) == '-') {
	        if (len != 1) {
	            pos = 1;
	        } else {
	        	return false;
	        }
	    }
	    while (pos < len) {
	        char c = s.charAt(pos);
	        if (c < '0' || c > '9') {
	            return false;
	        }
	        pos++;
	    }
	    return true;
	}
	
	public static String writeContentToFile(String pathname, String content) {
		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(pathname);
			bw = new BufferedWriter(fw);
			bw.write(content);
			return Configuration.SUCCESS_MSG;
		} catch(IOException ioe) {
			log.error(ioe.getMessage());
			return Configuration.UNKNOWN_ERROR_MSG;
		} finally {
			try {
				if (bw != null) {
					bw.close();
				}
				if (fw != null) {
					fw.close();
				}
			} catch (IOException ioe) {
				log.error(ioe.getMessage());
			}

		}
	}
	
	/**
	 * Retrieves the current time as a String compatible with the java sql Timestamp format. 
	 * 
	 * @return the current time as a String compatible with the java sql Timestamp format
	 */
	public static String getCurrTimeAsTimestampString() {
		Calendar cal = Calendar.getInstance();
		String ts = new Timestamp(cal.getTime().getTime()).toString();
		return ts;
	}
	
	/**
	 * Retrieves the current time as a java sql Timestamp. 
	 * 
	 * @return the current time as a java sql Timestamp
	 */
	public static Timestamp getCurrTimeAsTimestamp() {
		Calendar cal = Calendar.getInstance();
		return new Timestamp(cal.getTime().getTime());
	}

}
