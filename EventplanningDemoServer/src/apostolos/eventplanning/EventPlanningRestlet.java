/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

import apostolos.eventplanning.rest.resources.DeleteEventResource;
import apostolos.eventplanning.rest.resources.RegisterEventResource;
import apostolos.eventplanning.rest.resources.InsertDatastreamItemResource;
import apostolos.eventplanning.rest.resources.LastIdResource;
import apostolos.eventplanning.rest.resources.ListEventRequestsResource;
import apostolos.eventplanning.rest.resources.ListEventsResource;
import apostolos.eventplanning.rest.resources.LoginResource;
import apostolos.eventplanning.rest.resources.RegisterUserResource;
import apostolos.eventplanning.rest.resources.RequestParticipationResource;
import apostolos.eventplanning.rest.resources.SetPassphraseResource;
import apostolos.eventplanning.rest.resources.SetPreferredProxyResource;
import apostolos.eventplanning.rest.resources.ValueByIdResource;

/**
 * EventPlanningRestlet prepares the restlet router of the event management system,
 * i.e., it registers all resources that shall be provided by the restlet API,
 * defining also the respective classes, i.e., the classes that contain the implementation of each of those resources.
 * 
 * The API follows an RPC-style approach rather than a strict RESTful approach,
 * i.e., an approach in which the restlet resources are "functions that the API user wants to perform"
 * (all implemented with POST or GET, as appropriate), rather than resources upon which the API user
 * can act using all HTTP verbs (GET, POST, PUT, DELETE) in a strict RESTful manner.
 * A strict RESTful approach could have been used here and there is a respective comments section inside the class below,
 * which indicates how the code (and the API) would look like in this case.
 * For most applications like this, a RESTful approach (or maybe an API that is 95% RESTful and 5% RPC-style)
 * would be probably more appropriate, due to the extensibility and uniformity of RESTful solutions.
 * Instead of repeating the "RESTful vs RPC-based" discussion, I provide some links for the interested readers:
 * - https://blog.jscrambler.com/rpc-style-vs-rest-web-apis/
 * - https://softwareengineering.stackexchange.com/questions/181176/when-are-rpc-ish-approaches-more-appropriate-than-rest
 * - https://stackoverflow.com/questions/22322468/is-bad-practice-to-mix-rest-and-rpc-together
 * - https://medium.com/@tlhunter/is-it-time-to-replace-rest-with-rpc-1304379456a2
 * The RPC-style approach has been used in this project for the following reasons:
 * i) This code serves debating and educational purposes and the implementation of RPC-style restlet resources is more diverse and less typified (which is, of course, usually a disadvantage when developing and maintaining big projects!)
 * ii) For average "code readers", RPC-style APIs are IMHO more understandable and more indicative of the functionality implemented in a published demo/application, since single API calls express what is done in the system.
 * iii) The RPC-style approach tends to include all the "processing" (querying, selection, checking, and filtering of resources) on the server side (which is desired here, again for educational purposes), while strict RESTful approaches tend to fetch entire resource entries (or lists of them) and perform more processing on the client-side application.
 * 
 * @author Apostolos Papageorgiou
 */
public class EventPlanningRestlet extends Application {
	
    /* (non-Javadoc)
     * @see org.restlet.Application#createInboundRoot()
     */
    @Override
    public synchronized Restlet createInboundRoot() {
    	
    	Configuration.readPropertiesFromConfigFile();

        Router router = new Router(getContext());

        router.attach("/insertDatastreamItem", InsertDatastreamItemResource.class);
        router.attach("/getLastId", LastIdResource.class);
        router.attach("/getValueById", ValueByIdResource.class);
        router.attach("/setPassphrase", SetPassphraseResource.class);
        router.attach("/registerUser", RegisterUserResource.class);
        router.attach("/listEvents", ListEventsResource.class);
        router.attach("/listEventRequests", ListEventRequestsResource.class);
        router.attach("/requestParticipation", RequestParticipationResource.class);
        router.attach("/registerEvent", RegisterEventResource.class);
        router.attach("/deleteEvent", DeleteEventResource.class);
        router.attach("/setPreferredProxy", SetPreferredProxyResource.class);
        router.attach("/login", LoginResource.class);
        
        // The following lines indicate how the API would look like in a strict RESTful solution,
        // but note that implementing certain actions, e.g. "setPassphrase", would require deeper URIs,
        // client-side logic while also revealing the fact that unconfirmed users are stored and managed
        // differently than confirmed users, or indeed combining REST with some RPC-style resources:
        //
        // router.attach("/datastreamItems", AllDatastreamItemsResource.class)
        // router.attach("/datastreamItems/{id}", DatastreamItemResource.class)
        // router.attach("/users}", AllUsersResource.class)
        // router.attach("/users/{id}}", UserResource.class)
        // router.attach("/events}", AllEventsResource.class)
        // router.attach("/events/{id}}", EventResource.class)
        // ... etc.
        
        return router;
    }
}
