/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Configuration is a static class used for maintaining system properties.
 * <p>
 * It reads the configuration from a java properties file, the path and name of which are also specified in this class.
 * It also provides Enumerations and useful methods for handling the properties.
 *
 * @author Apostolos Papageorgiou
 */
public class Configuration {

	private static Logger log = Logger.getLogger("Configuration");
	
	public static final String SUCCESS_MSG = "Success!";
	public static final String UNKNOWN_ERROR_MSG = "Unknown error";
	public static final String DUPLICATE_ERROR_MSG = "Duplicate entry error";
	public static final String AUTH_ERROR_MSG = "Not authorized";
	
	private static final String CONFIG_FILE_NS = "ep.";
	private static final String CONFIG_FILE_DIR = "apostolos" + File.separator + "eventplanning" + File.separator;
	private static final String CONFIG_FILE_NAME = "ep.properties";
	
	public static String DB_HOST;
	public static String DB_PORT;
	public static String DB_NAME;
	public static String DB_USER;
	public static String DB_PASS;
	public static String DATASTREAM_API_KEY;
	public static String DATASTREAM_ADMIN_USER;
	public static String MAIL_FROM_ADDRESS;
	public static String MAIL_PASS;
	public static String REGISTRATION_MAIL_SUBJECT;
	public static String REQUEST_MAIL_SUBJECT;
	public static int PASSWORD_LENGTH;
	public static int TOKEN_LENGTH;
	public static int SESSION_ID_LENGTH;
	public static String SESSION_COOKIE_NAME;
	public static String EMAIL_COOKIE_NAME;
	
	public static String REQUESTED_PROXY_DIR = "/dir/of/file/that/contains/requested/proxy";
	public static String REQUESTED_PROXY_FILENAME = "filename.extension";
	
	public static List<String> ALLOWED_ORIGINS = new ArrayList<String>();
	
	/**
	 * Sets the values of the Configuration public class variables by reading the properties file.
	 * Note that the path and the name of the file must have been set correctly in the code.
	 *
	 * @return execution status (true for success or false for failure)
	 */
	public static boolean readPropertiesFromConfigFile() {
		Properties properties = new Properties();
		try {
			
		    //properties.load(new FileInputStream(new File(CONFIG_FILE_DIR + CONFIG_FILE_NAME))); // for using directory instead of resource location
			properties.load(Configuration.class.getResourceAsStream("/" + CONFIG_FILE_DIR + CONFIG_FILE_NAME));
		    
		    if (properties.containsKey(CONFIG_FILE_NS + "db_host")) {
		    	DB_HOST = properties.getProperty(CONFIG_FILE_NS + "db_host");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "db_port")) {
		    	DB_PORT = properties.getProperty(CONFIG_FILE_NS + "db_port");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "db_name")) {
		    	DB_NAME = properties.getProperty(CONFIG_FILE_NS + "db_name");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "db_user")) {
		    	DB_USER = properties.getProperty(CONFIG_FILE_NS + "db_user");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "db_pass")) {
		    	DB_PASS = properties.getProperty(CONFIG_FILE_NS + "db_pass");
		    } else {
		    	DB_PASS = retrievePwd("tbdb");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "datastream_api_key")) {
		    	DATASTREAM_API_KEY = properties.getProperty(CONFIG_FILE_NS + "datastream_api_key");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "datastream_admin_user")) {
		    	DATASTREAM_ADMIN_USER = properties.getProperty(CONFIG_FILE_NS + "datastream_admin_user");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "allowed_origins")) {
		    	String allowedOrigins = properties.getProperty(CONFIG_FILE_NS + "allowed_origins");
		    	ALLOWED_ORIGINS = Arrays.asList(allowedOrigins.split(",")); 
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "mail_from_address")) {
		    	MAIL_FROM_ADDRESS = properties.getProperty(CONFIG_FILE_NS + "mail_from_address");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "mail_pass")) {
		    	MAIL_PASS = properties.getProperty(CONFIG_FILE_NS + "mail_pass");
		    } else {
		    	MAIL_PASS = retrievePwd("tbmailer");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "registration_mail_subject")) {
		    	REGISTRATION_MAIL_SUBJECT = properties.getProperty(CONFIG_FILE_NS + "registration_mail_subject");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "request_mail_subject")) {
		    	REQUEST_MAIL_SUBJECT = properties.getProperty(CONFIG_FILE_NS + "request_mail_subject");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "password_length")) {
		    	PASSWORD_LENGTH = Integer.parseInt(properties.getProperty(CONFIG_FILE_NS + "password_length"));
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "token_length")) {
		    	TOKEN_LENGTH = Integer.parseInt(properties.getProperty(CONFIG_FILE_NS + "token_length"));
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "session_id_length")) {
		    	SESSION_ID_LENGTH = Integer.parseInt(properties.getProperty(CONFIG_FILE_NS + "session_id_length"));
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "session_cookie_name")) {
		    	SESSION_COOKIE_NAME = properties.getProperty(CONFIG_FILE_NS + "session_cookie_name");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "email_cookie_name")) {
		    	EMAIL_COOKIE_NAME = properties.getProperty(CONFIG_FILE_NS + "email_cookie_name");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "requested_proxy_dir")) {
		    	REQUESTED_PROXY_DIR = properties.getProperty(CONFIG_FILE_NS + "requested_proxy_dir");
		    }
		    if (properties.containsKey(CONFIG_FILE_NS + "requested_proxy_filename")) {
		    	REQUESTED_PROXY_FILENAME = properties.getProperty(CONFIG_FILE_NS + "requested_proxy_filename");
		    }
		    
		    return true;
		    
		} catch (IOException ioe) {
			log.error("Error reading configuration: " + ioe.getMessage());
			return false;
		}
	}

	/**
	 * Retrieves a password for a service by calling some hidden implementation, e.g., a non-exposed database, web service, or class.
	 * That implementation provides passwords based on an input parameter and must be sufficiently protected.
	 * As also discussed at https://security.stackexchange.com/questions/52693/how-can-one-secure-a-password-key-in-source-code,
	 * this is the best that can be done to hide a plaintext password that has to be used within source code.
	 * 
	 * @param service a String that can be used by the hidden servlet to identify the service for which the password is being retrieved
	 * @return the password for the specified service
	 */
	private static String retrievePwd(String service) {
		return "********";
	}
	
}
