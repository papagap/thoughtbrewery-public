/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.datamanagement;

import java.util.ArrayList;

/**
 * DatasetDAO is a Data Access Object for all actions that fetch or manipulate data
 * from the event management data storage system.
 * <p>
 * It hides the details of the data storage implementation and it contains only methods
 * that are actually used by the REST resources of the system.
 * It is preferred to having a separate DAO for each entity, because it aspires to hide
 * the actual structure of the data storage system, some of the methods might involve different
 * entities, and it is targeted to a system with relatively few entities (such as the event management system).
 * Another alternative would be a complete CRUD-like interface for every possible entity (potentially with
 * an additional layer/interface in front of it for operations that involve multiple entities), but this
 * would again contain many unused methods and a level of complexity that might hit against the YAGNI
 * ("You Ain't Gonna Need It") principle.
 * However, either a complete "CRUD-like interface" or an "one-DAO-per-entity" approach could be considered
 * in case of a more complex system or in order to achieve a more generic nature of the data management interface.
 * Each of the approaches has advantages and disadvantages.
 * 
 * @author Apostolos Papageorgiou
 */
public interface DatasetDAO {
	
	
	//------------------ Candidate-related DAO methods ------------------
	
	/**
	 * Adds an unconfirmed user (i.e., "candidate") to the system.
	 * 
	 * @param candidate object containing all the details of the (unconfirmed) user that is being added to the system
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the candidate was successfully inserted, or an appropriate error message otherwise
	 */
	public abstract String insertCandidate(Candidate candidate);
	
	/**
	 * Deletes an entry of an unconfirmed user (i.e., "candidate").
	 * To be used either when the user gets confirmed or when the entry expires or becomes invalid for any other reason.
	 * 
	 * @param email the e-mail of the (unconfirmed) user to be deleted
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the candidate was successfully deleted, or an appropriate error message otherwise
	 */
	public abstract String deleteCandidate(String email);
	
	/**
	 * Adds a confirmed user (i.e., "person") to the system.
	 * 
	 * @param candidate object containing all the details of the (confirmed) user that is being added to the system
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the person was successfully inserted, or an appropriate error message otherwise
	 */
	public abstract String insertPerson(Candidate candidate);
	
	/**
	 * Finds a candidate with a specified e-mail address.
	 * 
	 * @param email the e-mail of the candidate whose information shall be retrieved
	 * @return the {@link apostolos.eventplanning.datamanagement.Candidate} object that corresponds with the candidate of the provided e-mail address, or null if no candidate with such an e-mail address is found
	 */
	public abstract Candidate getCandidateByEmail(String email);
	
	/**
	 * Finds a confirmed user with a specified e-mail address.
	 * 
	 * @param email the e-mail of the confirmed user whose information shall be retrieved
	 * @return the {@link apostolos.eventplanning.datamanagement.Candidate} object that corresponds with the copfirmed user of the provided e-mail address, or null if no confirmed user with such an e-mail address is found
	 */
	public abstract Candidate getPersonByEmail(String email);
	
	/**
	 * Checks if an e-mail is currently unused or if it is already in the database.  
	 * 
	 * @param email the e-mail to be checked
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the e-mail did not previously exist, null if it did exist, or an appropriate error message if the query fails
	 */
	public abstract String checkEmailAvailability(String email);
	
	/**
	 * Checks if an alias is valid and available for being assigned to a user with a specified e-mail address.
	 * 
	 * @param alias the alias to be checked for validity and availability
	 * @param email the e-mail of the user that would use this alias
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the alias is available for usage, null if not, or an appropriate error message if the query fails
	 */
	public abstract String checkAliasAvailability(String alias, String email);
	
	/**
	 * Checks if the provided passphrase belongs to the user with the provided e-mail address.
	 * 
	 * @param email the e-mail of the user whose passphrase is being checked
	 * @param pass the passphrase to check for validity
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the passphrase was correct for the specified user, or an appropriate error message otherwise
	 */
	public abstract String checkPassphrase(String email, String pass);
	
	/**
	 * Adds an active session for a user in the system.
	 * 
	 * @param session object containing all the details of the session that is being added to the system
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the session was successfully inserted, or an appropriate error message otherwise
	 */
	public abstract String insertSession(Session session);
	
	/**
	 * Checks if the provided session id is valid for the user with the provided e-mail address.
	 * 
	 * @param email the e-mail of the user whose session id is being checked
	 * @param session the session id to check for validity
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the session id was valid for the specified user, or an appropriate error message otherwise
	 */
	public abstract String checkSession(String email, String session);
	
	/**
	 * Checks if the provided (one-time) token is valid for the user with the provided e-mail address.
	 * 
	 * @param email the e-mail of the user whose token is being checked
	 * @param token the token to check for validity
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the token was correct for the specified user, or an appropriate error message otherwise
	 */
	public abstract String checkToken(String email, String token);
	
	/**
	 * Checks if the user with the specified e-mail address is the owner of the event with the specified event name.
	 * 
	 * @param eventName the name of the event the ownership of which is being checked
	 * @param email the e-mail address of the user who is checked as being the owner (or not) of the event with the specified name
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the user with the specified e-mail address was the owner of the event with the specified name, or an appropriate error message otherwise
	 */
	public abstract String checkEventOwnership(String eventName, String email);
	
	
	//------------------ Event-related DAO methods ------------------
	
	/**
	 * Retrieves all events that are registered in the event management system.
	 * 
	 * @return an ArrayList of Event objects containing all events registered in the system
	 */
	public abstract ArrayList<Event> getAllEvents();
	
	/**
	 * Retrieves an event with a specified event name
	 * 
	 * @param eventName the name of the event to be retrieved
	 * @return the {@link apostolos.eventplanning.datamanagement.Event} object for the event with the provided name, or null if no event with such an e-mail address is found
	 */
	public abstract Event getEventByName(String eventName);
	
	/**
	 * Adds an event to the system
	 * 
	 * @param event object containing all the details of the event that is being added to the system
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the event was successfully inserted, or an appropriate error message otherwise
	 */
	public abstract String insertEvent(Event event);
	
	/**
	 * Adds a request for participation to an event to the system.
	 * 
	 * @param eventRequest object containing all the details of the event request that is being added to the system
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the event request was successfully inserted, or an appropriate error message otherwise
	 */
	public abstract String insertEventRequest(EventRequest eventRequest);
	
	/**
	 * Deletes an event with a specified name from the system.
	 * 
	 * @param eventName the name of the event to be deleted
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the event was successfully deleted, or an appropriate error message otherwise
	 */
	public abstract String deleteEvent(String eventName);
	
	/**
	 * Retrieves all the event requests for the event with the specified name.
	 * 
	 * @param eventName the name of the event
	 * @return an ArrayList of EventRequest objects containing all the requests for the event with the specified name
	 */
	public abstract ArrayList<EventRequest> getRequestsOfEvent(String eventName);

	
	//------------------ Generic (cross-entity) DAO methods ------------------
	
	/**
	 * Retrieves the last (i.e., the biggest existing) id of the specified entity.
	 * 
	 * @param entityName the name of the entity the last id of which is being searched
	 * @return the last id of the specified entity if the query was successful, or an appropriate error message otherwise
	 */
	public abstract String getLastId(String entityName);
		
	/**
	 * Retrieves the value of the entry with the specified id for the specified entity.
	 * 
	 * @param entityName the name of entity for which the provided id and its value are being searched
	 * @param id the id for which the corresponding value shall be retrieved
	 * @return the searched value if the query was successful, or an appropriate error message otherwise
	 */
	public abstract String getValueById(String entityName, String id);
	
	/**
	 * Adds a data stream item of a specified type of entity with the provided values.
	 * 
	 * @param entityName the name of the entity a new item of which shall be added
	 * @param ds the DatastreamItem object that contains the values of the item to be added
	 * @return a {@link apostolos.eventplanning.Configuration#SUCCESS_MSG} if the item was successfully inserted, or an appropriate error message otherwise
	 */
	public abstract String insertDatastreamItem(String entityName, DatastreamItem ds);
}
