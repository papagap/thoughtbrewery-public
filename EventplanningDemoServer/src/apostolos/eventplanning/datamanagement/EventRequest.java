/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.datamanagement;

import java.sql.Timestamp;

/**
 * EventRequest is a POJO for holding all the information related to an event request in the event management system. 
 * 
 * @author Apostolos Papageorgiou
 */
public class EventRequest {

	private int id;
	private String email;
    private String event;
    private String message;
    private Timestamp time;
	
    public EventRequest(int id, String email, String event, String message, Timestamp time) {
		this.id = id;
		this.email = email;
		this.event = event;
		this.message = message;
		this.time = time;
	}
    
    public EventRequest() {}
    
    /*-------------- GETTERS and SETTERS -------------*/
    
	/**
	 * @return the id with which this event request is stored in a database
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id an id with which this event request shall be stored in a database
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the e-mail of the user that has issued this event request
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * @param email the e-mail to be set as the e-mail of the user that has issued this event request
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * @return the name of the event for which this request has been issued
	 */
	public String getEvent() {
		return event;
	}
	
	/**
	 * @param event the name to be set as the name of the event for which this request has been issued
	 */
	public void setEvent(String event) {
		this.event = event;
	}
	
	/**
	 * @return the message that the user attached to this event request
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * @param message text to be set as the message that the user attached to this event request
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * @return the date/time at which this event request was issued (or last updated)
	 */
	public Timestamp getTime() {
		return time;
	}
	
	/**
	 * @param time the date/time to be set as the date/time at which this event request was issued (or last updated)
	 */
	public void setTime(Timestamp time) {
		this.time = time;
	}
    
}
