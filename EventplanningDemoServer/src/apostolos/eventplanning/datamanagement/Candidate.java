/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.datamanagement;

import java.sql.Timestamp;

/**
 * Candidate is a POJO for holding all the information related to a user of the event management system.
 * This POJO is used both for unconfirmed users (i.e., "candidates") and for confirmed (i.e., "persons").
 * The field "passphrase" is used as a (one-time, temporary) token if the object is used for a candidate and as a regular password if the object is used for a person.
 * 
 * @author Apostolos Papageorgiou
 */
public class Candidate {

	private int id;
	private String alias;
	private String email;
	private String passphrase;
	private String name;
	private String surname;
	private String tel;
	private String address;
	private String sex;
	private int age;
	private Timestamp time;
	
	public Candidate(int id, String alias, String email, String passphrase, String name, String surname, String tel, String address, String sex, int age, Timestamp time) {
		this.id = id;
		this.alias = alias;
		this.email = email;
		this.passphrase = passphrase;
		this.name = name;
		this.surname = surname;
		this.tel = tel;
		this.address = address;
		this.sex = sex;
		this.age = age;
		this.time = time;
	}
	
	public Candidate() {}
	
	/*-------------- GETTERS and SETTERS -------------*/
	
	/**
	 * @return an id with which this user is stored in a database 
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id an id with which this user shall be stored in a database
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return an alias with which this user is stored in a database 
	 */
	public String getAlias() {
		return alias;
	}
	
	/**
	 * @param alias an alias with which this user shall be stored in a database
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	/**
	 * @return the e-mail of this user
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * @param email the e-mail to be registered with this user
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * @return the passphrase of this user
	 */
	public String getPassphrase() {
		return passphrase;
	}
	
	/**
	 * @param passphrase the passphrase to be set for this user
	 */
	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}
	
	/**
	 * @return the name of this user
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to be set for this user
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the  family name of this user
	 */
	public String getSurname() {
		return surname;
	}
	
	/**
	 * @param surname the family name to be set for this user
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	/**
	 * @return the telephone number of this user
	 */
	public String getTel() {
		return tel;
	}
	
	/**
	 * @param tel the telephone number to be set for this user
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}
	
	/**
	 * @return the postal address of this user
	 */
	public String getAddress() {
		return address;
	}
	
	/**
	 * @param address the postal address to be set for this user
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
	/**
	 * @return the sex of this user
	 */
	public String getSex() {
		return sex;
	}
	
	/**
	 * @param sex the sex to be set for this user
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	
	/**
	 * @return the age of this user
	 */
	public int getAge() {
		return age;
	}
	
	/**
	 * @param age the age to be set for this user
	 */
	public void setAge(int age) {
		this.age = age;
	}
	
	/**
	 * @return the time and date at which this user was registered (or last updated)
	 */
	public Timestamp getTime() {
		return time;
	}
	
	/**
	 * @param time the date/time to set as the date/time at which this user was registered (or last updated)
	 */
	public void setTime(Timestamp time) {
		this.time = time;
	}

}
