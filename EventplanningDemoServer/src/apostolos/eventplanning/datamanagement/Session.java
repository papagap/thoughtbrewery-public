/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.datamanagement;

import java.sql.Timestamp;

/**
 * 
 * Session is a POJO for holding all the information related to an active session in the event management system. 
 * 
 * @author Apostolos Papageorgiou
 */
public class Session {

	private String id;
	private String email;
    private Timestamp time;
    
    public Session(String id, String email, Timestamp time) {
		this.id = id;
		this.email = email;
		this.time = time;
	}
    
    public Session() {}
    
    /*-------------- GETTERS and SETTERS -------------*/

	/**
	 * @return the id with which this session is stored in a database
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * @param id an id with which this session shall be stored in a database
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the e-mail of the user to whom this session belongs
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * @param email the e-mail to be set as the e-mail of the user to whom this session belongs
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * @return the date/time at which this session was started
	 */
	public Timestamp getTime() {
		return time;
	}
	
	/**
	 * @param time the date/time to be set as the date/time at which this session was started
	 */
	public void setTime(Timestamp time) {
		this.time = time;
	}
}
