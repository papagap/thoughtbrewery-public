/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.datamanagement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

//import com.sun.rowset.CachedRowSetImpl;

import apostolos.eventplanning.Configuration;
import apostolos.eventplanning.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DatasetDBHandler is the class that implements the event management Data Access Object (DAO),
 * namely {@link apostolos.eventplanning.datamanagement.DatasetDAO} for the case of a jdbc/mysql storage implementation.
 * <p>
 * Database connections are opened and closed inside each method that actually fetches or manipulates data,
 * while those methods shall be called from locally instantiated DAO objects.
 * This approach is preferred compared to a "Singleton", because there is no "state" that we would want to keep across DB transactions and
 * there is practically no "shared" resource, given that each consumer gets a different connection to the database.
 * Further, with the current implementation, it is possible to have a "default" constructor (which takes the database info from the configuration file)
 * as well as an overloaded constructor, in case it is required to instantiate this handler for different databases/users somewhere in the code.
 * <p>
 * With regard to retrieving database connections, it may be costly to always get a new connection from the database itself.
 * Connection pooling would help by having a pool of open connections, one of which is retrieved by the "getConnection" method,
 * thus making the getConnection and closeConnection methods faster. Remember that having open connections (e.g. in this class)
 * and just giving them to calling Threads leads to resource leakage, because most databases "claim the connection back" after a certain time.
 * Contrary, the connection pooling software "refreshes", i.e. closes and re-opens, its connections periodically "in the background",
 * i.e., in a way that does not intervene with user transactions. It can also be argued that -for big number of users- connection pooling
 * can serve the same number of concurrent users with fewer "open connections", although this is practically impossible if the users are
 * literally concurrent (X concurrent users need X open connections one way or the other).
 * In the case of few users, connection pooling will have a few more open connections but in this case there is no performance bottleneck at stake anyway.
 * For larger deployments, the body of the getConnection method should be changed to use connection pooling (using a library).
 * 
 * @author Apostolos Papageorgiou
 */
public class DatasetDBHandler implements DatasetDAO {
	
	private Logger log = LoggerFactory.getLogger("Dataset DB Handler");
	
    private String dbHost;
	private String dbPort;
    private String dbName;
    private String user;
    private String pass;
    
    /**
     * DatasetDBHandler Constructor that uses values of java properties to initiate the connection details of the database-handling object.
     */
    public DatasetDBHandler() {
    	this.dbHost = Configuration.DB_HOST;
    	this.dbPort = Configuration.DB_PORT;
    	this.dbName = Configuration.DB_NAME;
    	this.user = Configuration.DB_USER;
    	this.pass = Configuration.DB_PASS;
    }
    
    /**
     * DatasetDBHandler Constructor that uses method arguments to initiate the connection details of the database-handling object.
     * 
     * @param dbHost the IP address of the server that hosts the database
     * @param dbPort the port on which the database is running
     * @param dbName the name of the database that stores the event management system data
     * @param user the username of the account with which the instantiated object shall log into the database
     * @param pass the password of the account with which the instantiated object shall log into the database
     */
    public DatasetDBHandler(String dbHost, String dbPort, String dbName, String user, String pass) {
    	this.dbHost = dbHost;
    	this.dbPort = dbPort;
    	this.dbName = dbName;
    	this.user = user;
    	this.pass = pass;
	}
    
    //------------------ Database connection-handling methods ------------------
    
    /**
     * Retrieves a connection to the database based on the info and credentials that are maintained as class variables. 
     * 
     * @return a connection to the database
     */
    private Connection getConnection() {
        Connection connection = null;
        // String baseUrl = "jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"; // This would be for implementations using the Oracle connector-j
        String baseUrl = "jdbc:mariadb://" + dbHost + ":" + dbPort + "/" + dbName + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    	try {
        	//DriverManager.registerDriver(new com.mysql.jdbc.Driver()); // This was an alternative to the Class.forName() call (for the old driver), because the latter was not working well with OSGi
        	//Class.forName("com.mysql.cj.jdbc.Driver").newInstance(); // This would be for implementations using the Oracle connector-j
    		Class.forName("org.mariadb.jdbc.Driver");
			connection = DriverManager.getConnection(baseUrl, user, pass); // In case of larger deployments, this should be updated to retrieve the connection from a connection pool (using a library).
		} catch (SQLException sqle) {
			log.error(sqle.getMessage());
		} catch (ClassNotFoundException cnfe) {
			log.error(cnfe.getMessage());
		}
    	return connection;
    }
    
    /**
     * Closes a connection in order to release database resources (e.g., cursors) that are held by this connection.
     * 
     * @param connection the database connection to be closed
     */
    private void closeConnection(Connection connection) {
    	if (connection != null) {
    		try {
				connection.close();
			} catch (SQLException sqle) {
				log.error(sqle.getMessage());
			}
    	}
    }
 
    //------------------ Helper SQL query methods ------------------
    
    /**
     * Closes a statement in order to release database resources (e.g. cursors) that are held by this statement.
     * Note that Result Sets are guaranteed to be closed when the corresponding Statement is closed,
     * so as long as Statements are always closed consistently, Result Sets do not need to be closed explicitly. 
     * 
     * @param pstmt the statement to be closed
     */
    private void closeStatement(PreparedStatement pstmt) {
    	if (pstmt != null) {
    		try {
				pstmt.close();
			} catch (SQLException sqle) {
				log.error(sqle.getMessage());
			}
    	}
    }
    
    /*
     * Executes an SQL query that is a static String (i.e., contains no variables) by creating a PreparedStatement based on that String,
     * and returns the rows of the result.
     * <p>
     * Note that this method does not return a ResultSet, because a ResultSet cannot be used after the connection is closed.
     * ResultSets do not actually store all result rows, but they buffer and fetch data from an open connection,
     * because sometimes too many rows might be returned. Contrary, a CachedRowSetImpl contains all rows
     * (if technically possible) and can be used after the connection is closed.
     * This method should be avoided whenever it is possible to just open a connection,
     * do the job, and close the connection within an individual method.
     * 
     * @param query the SQL query to be executed
     * @return the rows of the result of the database query as a CachedRowSetImpl object
     */
    /*private CachedRowSetImpl getQueryResult(String query) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
        try {
        	connection = getConnection();
        	pstmt = connection.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery(query);
            CachedRowSetImpl crs = new CachedRowSetImpl();
            crs.populate(rs);
            return crs;
        } catch (SQLException sqle) {
            log.error(sqle.getMessage());
            return null;
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    }*/
    
    //------------------ Candidate-related DAO methods ------------------
    
    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#insertCandidate(apostolos.eventplanning.datamanagement.Candidate)
     */
    @Override
    public String insertCandidate(Candidate candidate) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	try {
    		connection = getConnection();
    		String query = "insert into candidate (alias,email,token,name,surname,tel,address,sex,age,registration_time) values (?,?,?,?,?,?,?,?,?,?) on duplicate key update alias=values(alias), email=values(email), token=values(token), name=values(name), surname=values(surname), tel=values(tel), address=values(address), sex=values(sex), age=values(age), registration_time=values(registration_time)";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, candidate.getAlias());
            pstmt.setString(2, candidate.getEmail());
            pstmt.setString(3, candidate.getPassphrase());
            pstmt.setString(4, candidate.getName());
            pstmt.setString(5, candidate.getSurname());
            pstmt.setString(6, candidate.getTel());
            pstmt.setString(7, candidate.getAddress());
            pstmt.setString(8, candidate.getSex());
            pstmt.setInt(9, candidate.getAge());
            pstmt.setTimestamp(10, candidate.getTime());
            if (pstmt.executeUpdate() > 0) {
            	return Configuration.SUCCESS_MSG;
            } else {
            	return Configuration.UNKNOWN_ERROR_MSG;
            }
        } catch(SQLException sqle) {
          	log.error(sqle.getMessage());
          	return sqle.getMessage();
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    }
    
	/* (non-Javadoc)
	 * @see apostolos.eventplanning.datamanagement.DatasetDAO#delteCandidate(java.lang.String)
	 */
	@Override
	public String deleteCandidate(String email) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	try {
    		connection = getConnection();
    		log.info("Deleting candidate with email: " + email);
    		String query = "delete from candidate where email = ?";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, email);
            if (pstmt.executeUpdate() > 0) {
            	return Configuration.SUCCESS_MSG;
            } else {
            	return Configuration.UNKNOWN_ERROR_MSG;
            }
        } catch(SQLException sqle) {
          	log.error(sqle.getMessage());
          	return sqle.getMessage();
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
	}
    
    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#insertPerson(apostolos.eventplanning.datamanagement.Candidate)
     */
	@Override
    public String insertPerson(Candidate candidate) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	try {
    		connection = getConnection();
    		String query = "insert into person (alias,email,passphrase,name,surname,tel,address,sex,age,registration_time) values (?,?,?,?,?,?,?,?,?,?) on duplicate key update alias=values(alias), email=values(email), passphrase=values(passphrase), name=values(name), surname=values(surname), tel=values(tel), address=values(address), sex=values(sex), age=values(age), registration_time=values(registration_time)";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, candidate.getAlias());
            pstmt.setString(2, candidate.getEmail());
            pstmt.setString(3, candidate.getPassphrase());
            pstmt.setString(4, candidate.getName());
            pstmt.setString(5, candidate.getSurname());
            pstmt.setString(6, candidate.getTel());
            pstmt.setString(7, candidate.getAddress());
            pstmt.setString(8, candidate.getSex());
            pstmt.setInt(9, candidate.getAge());
            pstmt.setTimestamp(10, candidate.getTime());
            if (pstmt.executeUpdate() > 0) {
            	return Configuration.SUCCESS_MSG;
            } else {
            	return Configuration.UNKNOWN_ERROR_MSG;
            }
        } catch(SQLException sqle) {
          	log.error(sqle.getMessage());
          	return sqle.getMessage();
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    }
	
	@Override
    public Candidate getPersonByEmail(String email) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	ResultSet rs = null;
    	Candidate candidate = null;
    	
    	try {
    		connection = getConnection();
    		String query = "select * from person where email=?";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, email);
            rs = pstmt.executeQuery(); // Since executeQuery never returns null, no null-safety check is needed below.
            while (rs.next()) {
            	candidate = new Candidate();
            	candidate.setAlias(rs.getString("alias"));
            	candidate.setEmail(rs.getString("email"));
            	candidate.setPassphrase(rs.getString("passphrase"));
            	candidate.setName(rs.getString("name"));
            	candidate.setSurname(rs.getString("surname"));
            	candidate.setTel(rs.getString("tel"));
            	candidate.setAddress(rs.getString("address"));
            	candidate.setSex(rs.getString("sex"));
            	candidate.setAge(rs.getInt("age"));
            	candidate.setTime(rs.getTimestamp("registration_time"));
			}
        } catch(SQLException sqle) {
          	log.error(sqle.getMessage());
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    	
    	return candidate;
    }
    
	/* (non-Javadoc)
	 * @see apostolos.eventplanning.datamanagement.DatasetDAO#getCandidateByEmail(java.lang.String)
	 */
	@Override
    public Candidate getCandidateByEmail(String email) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	ResultSet rs = null;
    	Candidate candidate = null;
    	
    	try {
    		connection = getConnection();
    		String query = "select * from candidate where email=?";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, email);
            rs = pstmt.executeQuery(); // Since executeQuery never returns null, no null-safety check is needed below.
            while (rs.next()) {
            	candidate = new Candidate();
            	candidate.setAlias(rs.getString("alias"));
            	candidate.setEmail(rs.getString("email"));
            	candidate.setPassphrase(rs.getString("token"));
            	candidate.setName(rs.getString("name"));
            	candidate.setSurname(rs.getString("surname"));
            	candidate.setTel(rs.getString("tel"));
            	candidate.setAddress(rs.getString("address"));
            	candidate.setSex(rs.getString("sex"));
            	candidate.setAge(rs.getInt("age"));
            	candidate.setTime(rs.getTimestamp("registration_time"));
			}
        } catch(SQLException sqle) {
          	log.error(sqle.getMessage());
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    	
    	return candidate;
    }
    
    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#checkAliasAvailability(java.lang.String, java.lang.String)
     */
	@Override
    public String checkAliasAvailability(String alias, String email) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	ResultSet rs = null;
    	
        try { // checking in the table of confirmed users
        	connection = getConnection();
            String query = "select alias from person where alias=? and email!=?";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, alias);
            pstmt.setString(2, email);
            rs = pstmt.executeQuery(); // Since executeQuery never returns null, no null-safety check is needed below.
            if (rs.next()) {
            	// there is another entry with this alias
                return null;
            }
        } catch(SQLException sqle) {
        	// something went wrong with the query
        	log.error(sqle.getMessage());
        	return sqle.getMessage();
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
        
        try { // checking in the table of unconfirmed users
        	connection = getConnection();
            String query = "select alias from candidate where alias=? and email!=?";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, alias);
            pstmt.setString(2, email);
            rs = pstmt.executeQuery(); // Since executeQuery never returns null, no null-safety check is needed below.
            if (!rs.next()) {
            	// no data in the result set either
                return Configuration.SUCCESS_MSG;
            } else {
            	// there is another entry with this alias
            	return null;
            }
        } catch(SQLException sqle) {
        	// something went wrong with the query
        	log.error(sqle.getMessage());
        	return sqle.getMessage();
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    }
    
    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#checkEmailAvailability(java.lang.String)
     */
	@Override
    public String checkEmailAvailability(String email) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	ResultSet rs = null;
    	
        try { // checking in the table of confirmed users 
        	connection = getConnection();
            String query = "select email from person where email=?";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, email);
            rs = pstmt.executeQuery(); // Since executeQuery never returns null, no null-safety check is needed below.
            if (rs.next()) {
            	// e-mail was found in the database
            	return null;
            }
        } catch(SQLException sqle) {
        	// something went wrong with the database query, so there is no certainty about existence of the e-mail in the database
        	log.error(sqle.getMessage());
        	return sqle.getMessage();
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
        
        try { // checking in the table of unconfirmed users
        	connection = getConnection();
            String query = "select email from candidate where email=?";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, email);
            rs = pstmt.executeQuery(); // Since executeQuery never returns null, no null-safety check is needed below.
            if (!rs.next()) {
            	// no data in the result set
                return Configuration.SUCCESS_MSG;
            } else {
            	// e-mail was found in the database
            	return null;
            }
        } catch(SQLException sqle) {
        	// something went wrong with the database query, so there is no certainty about existence of the e-mail in the database
        	log.error(sqle.getMessage());
        	return sqle.getMessage();
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    }
    
    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#checkPassphrase(java.lang.String, java.lang.String)
     */
	@Override
    public String checkPassphrase(String email, String pass) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	ResultSet rs = null;
    	String val = " ";
    	
        try {
        	connection = getConnection();
            String query = "select passphrase from person where email=?";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, email);
            rs = pstmt.executeQuery(); // Since executeQuery never returns null, no null-safety check is needed below.
            while (rs.next()) {
            	val = rs.getString("passphrase");
			}
            if (Util.checkPwd(pass, val)) {
        		return Configuration.SUCCESS_MSG;
        	}
        } catch(SQLException sqle) {
        	log.error(sqle.getMessage());
        	return sqle.getMessage();
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
        
        return Configuration.AUTH_ERROR_MSG;
    }
	
	@Override
    public String insertSession(Session session) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	try {
    		connection = getConnection();
    		String query = "insert into session (id,email,start_time) values (?,?,?) on duplicate key update id=values(id), email=values(email), start_time=values(start_time)";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, session.getId());
            pstmt.setString(2, session.getEmail());
            pstmt.setTimestamp(3, session.getTime());
            if (pstmt.executeUpdate() > 0) {
            	return Configuration.SUCCESS_MSG;
            } else {
            	return Configuration.UNKNOWN_ERROR_MSG;
            }
        } catch(SQLException sqle) {
          	log.error(sqle.getMessage());
          	if (sqle.getMessage().contains("Duplicate entry")) {
          		return Configuration.DUPLICATE_ERROR_MSG;
          	} else {
          		return sqle.getMessage();
          	}
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    }
	
	/* (non-Javadoc)
	 * @see apostolos.eventplanning.datamanagement.DatasetDAO#checkSession(java.lang.String, java.lang.String)
	 */
	@Override
    public String checkSession(String email, String session) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	ResultSet rs = null;
    	String val = " ";
    	
        try {
        	connection = getConnection();
            String query = "select id from session where email=?";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, email);
            rs = pstmt.executeQuery(); // Since executeQuery never returns null, no null-safety check is needed below.
            while (rs.next()) {
            	val = rs.getString("id");
            	if (Util.checkPwd(session, val)) {
            		return Configuration.SUCCESS_MSG;
            	}
			}
        } catch(SQLException sqle) {
        	log.error(sqle.getMessage());
        	return sqle.getMessage();
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
        
        return Configuration.AUTH_ERROR_MSG;
    }
    
	/* (non-Javadoc)
	 * @see apostolos.eventplanning.datamanagement.DatasetDAO#checkToken(java.lang.String, java.lang.String)
	 */
	@Override
    public String checkToken(String email, String token) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	ResultSet rs = null;
    	String val = " ";
    	
        try {
        	connection = getConnection();
            String query = "select token from candidate where email=?";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, email);
            rs = pstmt.executeQuery(); // Since executeQuery never returns null, no null-safety check is needed below.
            while (rs.next()) {
            	val = rs.getString("token");
			}
            if (val.equals(token)) {
        		return Configuration.SUCCESS_MSG;
        	}
        } catch(SQLException sqle) {
        	log.error(sqle.getMessage());
        	return sqle.getMessage();
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
        
        return Configuration.AUTH_ERROR_MSG;
    }
    
    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#checkEventOwnership(java.lang.String, java.lang.String)
     */
	@Override
    public String checkEventOwnership(String eventName, String email) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	ResultSet rs = null;
    	String val = " ";
    	
        try {
        	connection = getConnection();
            String query = "select owner_email from event where name=?";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, eventName);
            rs = pstmt.executeQuery(); // Since executeQuery never returns null, no null-safety check is needed below.
            while (rs.next()) {
            	val = rs.getString("owner_email");
			}
            if (val.equals(email)) {
        		return Configuration.SUCCESS_MSG;
        	}
        } catch(SQLException sqle) {
        	log.error(sqle.getMessage());
        	return sqle.getMessage();
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
        
        return Configuration.AUTH_ERROR_MSG;
    }
    
    //------------------ Event-related methods ------------------

    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#getAllEvents()
     */
	@Override
    public ArrayList<Event> getAllEvents() {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	ResultSet rs = null;
    	Event currEvent = null;
    	ArrayList<Event> eventsList = new ArrayList<Event>();
    	
    	try {
    		connection = getConnection();
    		String query = "select * from event";
            pstmt = connection.prepareStatement(query);
            rs = pstmt.executeQuery(); // Since executeQuery never returns null, no null-safety check is needed below.
            while (rs.next()) {
            	currEvent = new Event();
            	currEvent.setId(rs.getInt("id"));
            	currEvent.setName(rs.getString("name"));
            	currEvent.setEmail(rs.getString("owner_email"));
            	currEvent.setInfo(rs.getString("info"));
            	currEvent.setPlace(rs.getString("place"));
            	currEvent.setAddress(rs.getString("address"));
            	currEvent.setType(rs.getString("type"));
            	currEvent.setLat(rs.getFloat("lat"));
            	currEvent.setLon(rs.getFloat("lon"));
            	currEvent.setTime(rs.getTimestamp("event_time"));
            	currEvent.setRegTime(rs.getTimestamp("registration_time"));
            	eventsList.add(currEvent);
			}
        } catch(SQLException sqle) {
          	log.error(sqle.getMessage());
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    	
    	return eventsList;
    }
	
    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#getEventByName(java.lang.String)
     */
	@Override
    public Event getEventByName(String eventName) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	ResultSet rs = null;
    	Event event = null;
    	
    	try {
    		connection = getConnection();
    		String query = "select * from event where name=?";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, eventName);
            rs = pstmt.executeQuery(); // Since executeQuery never returns null, no null-safety check is needed below.
            while (rs.next()) {
            	event = new Event();
            	event.setName(rs.getString("name"));
            	event.setEmail(rs.getString("owner_email"));
            	event.setInfo(rs.getString("info"));
            	event.setPlace(rs.getString("place"));
            	event.setAddress(rs.getString("address"));
            	event.setLat(rs.getFloat("lat"));
            	event.setLon(rs.getFloat("lon"));
            	event.setType(rs.getString("type"));
            	event.setTime(rs.getTimestamp("event_time"));
            	event.setRegTime(rs.getTimestamp("registration_time"));
			}
        } catch(SQLException sqle) {
          	log.error(sqle.getMessage());
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    	
    	return event;
    }

    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#insertEventRequest(apostolos.eventplanning.datamanagement.EventRequest)
     */
	@Override
    public String insertEventRequest(EventRequest eventRequest) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	try {
    		connection = getConnection();
    		String query = "insert into request (email,event_name,message,timestamp) values (?,?,?,?) on duplicate key update email=values(email), event_name=values(event_name), message=values(message), timestamp=values(timestamp)";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, eventRequest.getEmail());
            pstmt.setString(2, eventRequest.getEvent());
            pstmt.setString(3, eventRequest.getMessage());
            pstmt.setTimestamp(4, eventRequest.getTime());
            if (pstmt.executeUpdate() > 0) {
            	return Configuration.SUCCESS_MSG;
            } else {
            	return Configuration.UNKNOWN_ERROR_MSG;
            }
        } catch(SQLException sqle) {
          	log.error(sqle.getMessage());
          	if (sqle.getMessage().contains("Duplicate entry")) {
          		return Configuration.DUPLICATE_ERROR_MSG;
          	} else {
          		return sqle.getMessage();
          	}
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    }
    
    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#insertEvent(apostolos.eventplanning.datamanagement.Event)
     */
	@Override
    public String insertEvent(Event event) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	try {
    		connection = getConnection();
    		log.info("Inserting event with type: " + event.getType());
    		String query = "insert into event (name,owner_email,info,place,address,lat,lon,type,registration_time,event_time) values (?,?,?,?,?,?,?,?,?,?)";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, event.getName());
            pstmt.setString(2, event.getEmail());
            pstmt.setString(3, event.getInfo());
            pstmt.setString(4, event.getPlace());
            pstmt.setString(5, event.getAddress());
            pstmt.setFloat(6, event.getLat());
            pstmt.setFloat(7, event.getLon());
            pstmt.setString(8, event.getType());
            pstmt.setTimestamp(9, event.getRegTime());
            pstmt.setTimestamp(10, event.getTime());
            if (pstmt.executeUpdate() > 0) {
            	return Configuration.SUCCESS_MSG;
            } else {
            	return Configuration.UNKNOWN_ERROR_MSG;
            }
        } catch(SQLException sqle) {
        	log.error(sqle.getMessage());
          	if (sqle.getMessage().contains("Duplicate entry")) {
          		return Configuration.DUPLICATE_ERROR_MSG;
          	} else {
          		return sqle.getMessage();
          	}
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    }
    
    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#deleteEvent(java.lang.String)
     */
	@Override
    public String deleteEvent(String eventName) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	try {
    		connection = getConnection();
    		log.info("Deleting event with name: " + eventName);
    		String query = "delete from event where name = ?";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, eventName);
            if (pstmt.executeUpdate() > 0) {
            	return Configuration.SUCCESS_MSG;
            } else {
            	return Configuration.UNKNOWN_ERROR_MSG;
            }
        } catch(SQLException sqle) {
          	log.error(sqle.getMessage());
          	return sqle.getMessage();
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    }

    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#getRequestsOfEvent(java.lang.String)
     */
	@Override
    public ArrayList<EventRequest> getRequestsOfEvent(String eventName) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	ResultSet rs = null;
    	EventRequest currRequest = null;
    	ArrayList<EventRequest> requestsList = new ArrayList<EventRequest>();
    	
    	try {
    		connection = getConnection();
    		String query = "select * from request where event_name=?";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, eventName);
            rs = pstmt.executeQuery(); // Since executeQuery never returns null, no null-safety check is needed below.
            while (rs.next()) {
            	currRequest = new EventRequest();
            	currRequest.setId(rs.getInt("id"));
            	currRequest.setEmail(rs.getString("email"));
            	currRequest.setEvent(rs.getString("event_name"));
            	currRequest.setMessage(rs.getString("message"));
            	currRequest.setTime(rs.getTimestamp("timestamp"));
            	requestsList.add(currRequest);
			}
        } catch(SQLException sqle) {
          	log.error(sqle.getMessage());
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    	
    	return requestsList;
    }
    
    //------------------ Generic (cross-entity) methods ------------------
    
    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#getLastId(java.lang.String)
     */
	@Override
    public String getLastId(String tableName) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	ResultSet rs = null;
    	String res = " ";
    	
    	try {
    		connection = getConnection();
    		String query = "select max(id) from " + tableName;
            pstmt = connection.prepareStatement(query);
            rs = pstmt.executeQuery(); // Since executeQuery never returns null, no null-safety check is needed below.
           	rs.next();
           	res = String.valueOf(rs.getInt("max(id)"));
        } catch(SQLException sqle) {
          	log.error(sqle.getMessage());
          	res = sqle.getMessage();
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    	
    	return res;
    }
    
    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#getValueById(java.lang.String, java.lang.String)
     */
	@Override
    public String getValueById(String tableName, String id) {
		
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	String query = "select value from " + tableName + " where id=" + id;
    	String value = null;
    	
        try {
        	connection = getConnection();
        	pstmt = connection.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery(query);
            if (rs != null) {
    	    	try {
    	    		while (rs.next()) {
    					value = rs.getString("value");
    				}
    	    	} catch (SQLException sqle) {
    				log.error(sqle.getMessage());
    			}
        	}
            return value;
        } catch (SQLException sqle) {
            log.error(sqle.getMessage());
            return null;
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
		
        // Below is an alternative implementation of this method
        // that would use the getQueryResult method of this class,
        // which is now also commented out because it uses the
        // non-standard Java library sun.com.rowset.CachedRowSetImpl
    	/*ResultSet rs = getQueryResult("select value from " + tableName + " where id=" + Integer.parseInt(id));
    	String val = null;
    	
    	if (rs != null) {
	    	try {
	    		while (rs.next()) {
					val = rs.getString("value");
				}
	    	} catch (SQLException sqle) {
				log.error(sqle.getMessage());
			}
    	}
    	
    	return val;*/
    }
    
    /* (non-Javadoc)
     * @see apostolos.eventplanning.datamanagement.DatasetDAO#insertEntryByNameValue(java.lang.String, java.lang.String, java.lang.String)
     */
	@Override
    public String insertDatastreamItem(String tableName, DatastreamItem ds) {
    	Connection connection = null;
    	PreparedStatement pstmt = null;
    	
    	try {
    		connection = getConnection();
    		String query = "insert into " + tableName + " (name,value,timestamp) values (?,?,?)";
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, ds.getName());
            pstmt.setString(2, ds.getValue());
            pstmt.setTimestamp(3, ds.getTimestamp());
            if (pstmt.executeUpdate() > 0){
            	return Configuration.SUCCESS_MSG;
            } else {
            	return Configuration.UNKNOWN_ERROR_MSG;
            }
        } catch(SQLException sqle) {
          	log.error(sqle.getMessage());
          	return sqle.getMessage();
        } finally {
        	closeConnection(connection);
        	closeStatement(pstmt);
        }
    }

}
