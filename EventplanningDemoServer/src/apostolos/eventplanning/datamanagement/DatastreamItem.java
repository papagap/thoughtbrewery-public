/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.datamanagement;

import java.sql.Timestamp;

/**
 * DatastreamItem is a POJO for holding all the information related to a single data stream item.
 * 
 * @author Apostolos Papageorgiou
 */
public class DatastreamItem {

	private int id;
	private String name;
	private String value;
	private Timestamp timestamp;
	
	public DatastreamItem(int id, String name, String val, Timestamp time) {
		this.id = id;
		this.name = name;
		this.value = val;
		this.timestamp = time;
	}
	
	/*-------------- GETTERS and SETTERS -------------*/
	
	/**
	 * @return an id with which this item is stored in a database 
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id an id with which this item is stored in a database
	 */
	public void setEntryId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the name of the object that has been received
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name of the object that has been received
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the value of the object that has been received
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * @param value the value of the object that has been received
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * @return the timestamp of the object that has been received
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}
	
	/**
	 * @param timestamp the timestamp of the object that has been received
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	
}
