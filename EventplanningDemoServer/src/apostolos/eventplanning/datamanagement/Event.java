/*******************************************************************************
 * Copyright 2019 Apostolos Papageorgiou
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package apostolos.eventplanning.datamanagement;

import java.sql.Timestamp;

/**
 * Event is a POJO for holding all the information related to an event of the event management system. 
 * 
 * @author Apostolos Papageorgiou
 */
public class Event {
	
	private int id;
	private String name;
	private String email;
	private String info;
	private String place;
	private String address;
	private float lat;
	private float lon;
	private String type;
	private Timestamp regTime;
	private Timestamp time;
	
	public Event(int id, String name, String email, String info, String place, String address, float lat, float lon, String type, Timestamp regTime, Timestamp time) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.info = info;
		this.place = place;
		this.address = address;
		this.lat = lat;
		this.lon = lon;
		this.type = type;
		this.regTime = regTime;
		this.time = time;
	}
	
	public Event() {}

	/*-------------- GETTERS and SETTERS -------------*/
	
	/**
	 * @return the id with which this event is stored in a database 
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id an id with which this event shall be stored in a database
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the name of this event
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to be used for this event
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the e-mail of the user that has registered this event 
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * @param email the e-mail to be set as the e-mail of the user that has registered this event
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * @return the (owner-provided) information about this event 
	 */
	public String getInfo() {
		return info;
	}
	
	/**
	 * @param info the text to be used as the (owner-provided) information about this event
	 */
	public void setInfo(String info) {
		this.info = info;
	}
	
	/**
	 * @return a description of the place at which this event shall take place
	 */
	public String getPlace() {
		return place;
	}
	
	/**
	 * @param place the text to be used as the description of the place at which this event shall take place
	 */
	public void setPlace(String place) {
		this.place = place;
	}
	
	/**
	 * @return the postal address at which this event shall take place
	 */
	public String getAddress() {
		return address;
	}
	
	/**
	 * @param address the postal address to be set as the address at which this event shall take place
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
	/**
	 * @return the geographical latitude of the address of this event
	 */
	public float getLat() {
		return lat;
	}
	
	/**
	 * @param lat a number to be used as the geographical latitude of the address of this event
	 */
	public void setLat(float lat) {
		this.lat = lat;
	}
	
	/**
	 * @return the geographical longitude of the address of this event
	 */
	public float getLon() {
		return lon;
	}
	
	/**
	 * @param lon a number to be used as the geographical longitude of the address of this event
	 */
	public void setLon(float lon) {
		this.lon = lon;
	}
	
	/**
	 * @return the type of this event, e.g., "invitation" or "foodsharing" (refer to the database schema for the valid values)
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * @param type the type to be set for this event
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * @return the date/time at which this event takes place
	 */
	public Timestamp getTime() {
		return time;
	}
	
	/**
	 * @param time the date/time to be set as the date/time at which this event takes place
	 */
	public void setTime(Timestamp time) {
		this.time = time;
	}

	/**
	 * @return the date/time at which this event was registered (or last updated)
	 */
	public Timestamp getRegTime() {
		return regTime;
	}

	/**
	 * @param regTime the date/time to be set as the date/time at which this event was registered (or last updated)
	 */
	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}

}
