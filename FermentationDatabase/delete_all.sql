SET SQL_SAFE_UPDATES = 0;

delete from session;
delete from request;
delete from event;
delete from person;
delete from candidate;
delete from tsentry;