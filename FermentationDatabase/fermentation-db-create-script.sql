DROP TABLE IF EXISTS session;
DROP TABLE IF EXISTS request;
DROP TABLE IF EXISTS event;
DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS candidate;
DROP TABLE IF EXISTS tsentry;


CREATE TABLE candidate (
  id int unsigned NOT NULL AUTO_INCREMENT,
  alias varchar(20) NOT NULL,
  email varchar(255) NOT NULL,
  token char(100),
  name varchar(50),
  surname varchar(50),
  tel varchar(40),
  address varchar(255),
  sex enum('male', 'female', 'unknown'),
  age int,
  registration_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE KEY (alias),
  UNIQUE KEY (email)
);


CREATE TABLE person (
  id int unsigned NOT NULL AUTO_INCREMENT,
  alias varchar(20) NOT NULL,
  email varchar(255) NOT NULL,
  passphrase CHAR(60) CHARACTER SET latin1 COLLATE latin1_bin,
  name varchar(50),
  surname varchar(50),
  tel varchar(40),
  address varchar(255),
  sex enum('male', 'female', 'unknown'),
  age int,
  registration_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE KEY (alias),
  UNIQUE KEY (email)
);


CREATE TABLE event (
  id int unsigned NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  owner_email varchar(255) NOT NULL,
  info text,
  place varchar(50),
  address varchar(255),
  lat float(10,6) NOT NULL,
  lon float(10,6) NOT NULL,
  type enum('invitation', 'foodsharing', 'unknown'),
  registration_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  event_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE KEY (name),
  CONSTRAINT event_owner_fk
        FOREIGN KEY (owner_email) 
        REFERENCES person(email) ON DELETE CASCADE
);


CREATE TABLE request (
  id int unsigned NOT NULL AUTO_INCREMENT,
  email varchar(255) NOT NULL,
  event_name varchar(100) NOT NULL,
  message text,
  timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE KEY (email, event_name),
  CONSTRAINT request_event_fk
        FOREIGN KEY (event_name) 
        REFERENCES event(name) ON DELETE CASCADE,
  CONSTRAINT request_email_fk
        FOREIGN KEY (email) 
        REFERENCES person(email) ON DELETE CASCADE
);

CREATE TABLE session (
  id CHAR(60) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  email varchar(255) NOT NULL,
  start_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  CONSTRAINT session_email_fk
        FOREIGN KEY (email) 
        REFERENCES person(email) ON DELETE CASCADE
);

CREATE TABLE tsentry (
  id int unsigned NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  value varchar(100) NOT NULL,
  timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);