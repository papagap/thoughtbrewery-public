CREATE EVENT AutoDeleteOldDatastreamItems
ON SCHEDULE AT CURRENT_TIMESTAMP + INTERVAL 1 DAY 
ON COMPLETION PRESERVE
DO 
DELETE LOW_PRIORITY FROM thoughtb_fermentation.tsentry WHERE timestamp < DATE_SUB(NOW(), INTERVAL 1 DAY)