#-------------------------------------------------------------------------------
# Copyright 2019 Apostolos Papageorgiou
# 
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License.  You may obtain a copy
# of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
# License for the specific language governing permissions and limitations under
# the License.
#-------------------------------------------------------------------------------
# FermentationDatabase

This project includes scripts for creating a database that can be used for event planning applications.
In the context of the thoughtbrewery domain, this database is used by the event planning demo server.

## Usage

Install a mysql server and run the scripts in it. Currently tested with MySQL Community Server 8.0.

## Authors

* **Apostolos Papageorgiou** - [thoughtbrewery](https://thoughtbrewery.net)

## License

This project is licensed under the Apache 2.0 license. Please see the LICENSE file for more details.
